/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

dryrun_info dryrun;	
unsigned int cur_range, auto_mode_delay,start_motor_delay;//, fav_volt
static unsigned char tmr2_time_check_flg, tmr_mode, start[5], tmr_motor_status;
unsigned char motor_on,cyclic_timer=0,twice_timer=0,start_sec[5];
unsigned char byps;

extern volatile unsigned char dtmf_buf[BUF_LEN];
extern volatile unsigned int tmr1_count,timer1_delay;
extern ivrs_info ivrs, st;			
extern unsigned int ivrs_buf[],ivrs_pos;
//extern char current_range[5];
extern unsigned char dtmf_wait_cnt;
unsigned char admin_ph[AD_NPHONES][NDIGIT_PH_NUM + NULL_CHARACTER] = {{0}};
unsigned char debug_ph[NDIGIT_PH_NUM + NULL_CHARACTER] = {0};
unsigned char b_ph[NDIGIT_PH_NUM + NULL_CHARACTER] = {"9980010810"};



void convert_int_to_str(unsigned int val, unsigned char *buf)
{
	unsigned char j = 0, i = 0, temp;

	do		// do while is used to run the loop even when 0 is passed to val
	{    
		buf[j] = (val % 10) + '0'; 
		val /= 10;
		j++; 
	}while(val);   

	buf[j] = '\0';
	--j; 
	while(i < j) 
	{    
		temp = buf[i], buf[i] = buf[j], buf[j] = temp;
		i++, --j; 
	}    
}


/**************************************************************************************** 
 * This function is used to calibrate KISANRAJA with the connected motor. 
 * A variable 'ftb_flg' (First time boot flag) is used to identify if the 
 * device is booting for the first time. During calibration, 
 * after running the motor for 5 minutes, all the three phase voltages and two phase 
 * currents will be stored in eeprom. These values will be used in calculations 
 * to decide on favourable or unfavourable conditions to start motor.   
 ****************************************************************************************/
	void
motor_calibration(void)
{
	unsigned char ftb_flg=0, calib_time=0, manual_stop=0, calib_status=0;
	unsigned int lcur=0;
#if AUTOMATE
	char i=0;
#endif
	wdt_reset();
	ftb_flg = my_eeprom_read_byte((unsigned char*)EEFTB);

	while(ftb_flg == 'F')
	{
		stop_timer3(); 	// this is added so that when device is 
		//kept in motor calibration if periodic reboot happens then entered time will be lost.
		if(calib_time == 0)
		{
			if(calib_flag == 1) 	// if *99# is activated then set calibration time default 
				//as one minute else ask for calibration time.
			{
				calib_time=1;
				calib_flag=0;
				my_eeprom_write_byte((unsigned char)calib_flag, (unsigned char*)EECALIB_FLAG);
				strcpy((char *)caller_number,(const char *)ph[0]);
			}
			else
			{
				calib_time = get_calibration_time();
			}
		}
		display_flag =1;
		/**	wait here if power is not available **/
		while(check_favourable() == C_UnFavourable)	
		{

#if AUTOMATE
			Delay_sec(1);
			send_serial0('\r');
			for(i=0;i<=40;i++)
			{
				send_serial0(' ');
			}
#endif
			/** LED indication for a device in calibration. Power LED will be blinking **/
			SET_PIN(LED_CTRL_PORT, FAV_LED);                
			Delay_sec(1);
			CLEAR_PIN(LED_CTRL_PORT, FAV_LED);      
#if AUTOMATE
			if((automation_code ==1) && (password_set ==1))
			{

				print_ssd_delay("\rCALIB",0,500);
				check_automated_favr();
				check_sig_reg();
				get_ser_gsm();
				if(display_flag == 1)
				{
					display_device_vals();
					display_flag =0;
				}

			}
#else
			print_ssd_delay(NULL,adc_conv(SMPS_OUTPUT_PIN),SHORT_PRINT_DELAY);
			print_ssd_delay("CALB",0,SHORT_PRINT_DELAY);
			Delay_sec(1);
			if(answer_call())
			{
				audio_out(M_Calibration);
				audio_out(M_FailSwitchOn);
				disconnect_call();
			}

#endif
		}
#if AUTOMATE
		if((automation_code ==1) && (password_set ==1))
		{
			check_automated_favr();
			print_ssd_delay("\n\rPOWR AVAIL",0,500);
			display_device_vals();
			Delay_sec(2);
		}
#else
		print_ssd_delay(NULL,adc_conv(SMPS_OUTPUT_PIN),PRINT_DELAY);
#endif

		if((motor_status() == M_Stopped) && (!manual_stop))
			start_motor(); 

		Delay_sec(1);
#if AUTOMATE
		if((motor_status() == M_Started) && ((automation_code ==1) && (password_set ==1)))
		{
			print_ssd_delay("\n\rMOTR ON",0,500);
			display_device_vals();
		}
#endif
		/*** The following condition is to handle the case where i 
		 * triggered start_motor, but motor has not started ****/
		while(motor_status() == M_Stopped)
		{

			/** LED indication for a device in calibration. Power LED will be blinking **/
			SET_PIN(LED_CTRL_PORT, FAV_LED);                
			Delay_sec(1);
			CLEAR_PIN(LED_CTRL_PORT, FAV_LED);  
#if AUTOMATE
			if((automation_code ==1) && (password_set ==1))
			{
				stop_motor_ct_err();
				r_phase_current = adc_conv(CURRENT_PHASE_R);
				y_phase_current = adc_conv(CURRENT_PHASE_Y);
				b_phase_current = adc_conv(CURRENT_PHASE_B);
				if((r_phase_current == 0) && (y_phase_current == 0) && (b_phase_current ==0))
				{
					stop_motor_ct_err();
					display_device_vals();
					while(1)
					{
						while_delay();
						print_ssd_delay("\rCT-CON-PROB",0,500);
					}
				}	
				check_ct_values();
				display_device_vals();
				print_ssd_delay("REL_ERR\n",0,550);
				while(1)
				{
					while_delay();
					print_ssd_delay("\rFAIL TO ON MOTR",0,500);
				}
				send_serial0('\n');

			}
#else

			print_ssd_delay("STRT",0,SHORT_PRINT_DELAY);
			Delay_sec(1);
			if(answer_call())
			{
				audio_out(M_Calibration);
				audio_out(M_FailSwitchOn);
				disconnect_call();
			}

#endif
		}


		/** when motor has started, start the timer for motor calibration **/
		start_timer1(calib_time);
		while(!timer1_expired)
		{
			wdt_reset();
#if AUTOMATE
			if((automation_code == 1) && (password_set == 1))
			{

				check_ct_values();
				if((check_automated_favr()) ==1)
					break;

				send_serial0('\r');
				for(i=0;i<=40;i++)
				{
					send_serial0(' ');
				}
				wdt_reset();
				_delay_ms(100);
				wdt_reset();
				print_ssd_delay("\rCALIB MOTR RUNING WAIT..",0,500);
				wdt_reset();
				_delay_ms(100);
				wdt_reset();

			}
#else
			/** LED indication for a device in calibration and motor is running. 
			 * Power LED and Auto LED will be blinking **/
			CLEAR_PIN(LED_CTRL_PORT, FAV_LED);  
			SET_PIN(LED_CTRL_PORT, AUTO_LED);                
			Delay_sec(1);
			SET_PIN(LED_CTRL_PORT, FAV_LED);                
			CLEAR_PIN(LED_CTRL_PORT, AUTO_LED);  
			Delay_sec(1);

			if(answer_call()){
				audio_out(M_Calibration);
				audio_out(M_MotorOn);
				disconnect_call();
			}


			print_ssd_delay("CABT",0,SHORT_PRINT_DELAY);
			/**	stop motor and break from here incase of power cut while motor is running **/
			if((adc_conv(SMPS_OUTPUT_PIN) < MIN_SMPS_VOLTAGE))
			{
				stop_motor();
				break;	
			}
			if((adc_conv(SMPS_OUTPUT_PIN) > MIN_SMPS_VOLTAGE) && (motor_status() == M_Stopped))
			{
				manual_stop = 1;
				break;
			}
#endif		
		}
		stop_timer1();

		/** once the motor calibration timer expires, 
		 * write all the voltage and current values to eeprom. These values will be used as 
		 reference values to decide on power availability, wirecut and dry-run **/
		if(timer1_expired==1)
		{	
			wdt_reset();
#if AUTOMATE
			print_ssd_delay("TMR_EXPR",0,500);
			if((automation_code ==1) && (password_set ==1))
				display_device_vals();
#endif
			r_phase_current = adc_conv(CURRENT_PHASE_R);
			//print_ssd_delay(" C1 ",0,PRINT_DELAY);
			//print_ssd_delay(NULL, r_phase_current,PRINT_DELAY);

			y_phase_current = adc_conv(CURRENT_PHASE_Y);
			//	print_ssd_delay(" C2 ",0,PRINT_DELAY);
			//	print_ssd_delay(NULL, y_phase_current,PRINT_DELAY);

			// newly added for 3 CT's.
			b_phase_current = adc_conv(CURRENT_PHASE_B);
			//	print_ssd_delay(" C3 ",0,PRINT_DELAY);
			//	print_ssd_delay(NULL, b_phase_current,PRINT_DELAY);

			stop_motor();
			Delay_sec(2);
			if((r_phase_current > MIN_CUR_VAL) && (y_phase_current > MIN_CUR_VAL) \
					&& (b_phase_current > MIN_CUR_VAL))
			{	

				if(adc_conv(VOLTAGE_PHASE_Y) < MIN_PHASE_VOLTAGE) /** two phase calibration **/
				{
					my_eeprom_write_int((unsigned int)r_phase_current\
							, (unsigned int *)TWPH_R_PHASE_CURRENT);
					my_eeprom_write_int((unsigned int)y_phase_current\
							, (unsigned int *)TWPH_Y_PHASE_CURRENT);
					my_eeprom_write_int((unsigned int)b_phase_current\
							, (unsigned int *)TWPH_B_PHASE_CURRENT);

					lcur = y_phase_current + r_phase_current + b_phase_current;
					my_eeprom_write_int((unsigned int)lcur, (unsigned int *)TWPH_EELCUR);
					// print_ssd_delay("C LD",0,PRINT_DELAY);
					// print_ssd_delay(NULL,lcur,PRINT_DELAY);
					cur_range = 0.7 * lcur;
					my_eeprom_write_int((unsigned int)cur_range,(unsigned int *)TWPH_EECRNGE);
				}
				else /** three phase calibration **/
				{
					my_eeprom_write_int((unsigned int)r_phase_current\
							, (unsigned int *)R_PHASE_CURRENT);
					my_eeprom_write_int((unsigned int)y_phase_current\
							, (unsigned int *)Y_PHASE_CURRENT);
					my_eeprom_write_int((unsigned int)b_phase_current\
							, (unsigned int *)B_PHASE_CURRENT);

					lcur = y_phase_current + r_phase_current + b_phase_current;
					my_eeprom_write_int((unsigned int)lcur, (unsigned int *)EELCUR);
					//  print_ssd_delay("C LD",0,PRINT_DELAY);
					//   print_ssd_delay(NULL,lcur,PRINT_DELAY);
					cur_range = 0.7 * lcur;
					my_eeprom_write_int((unsigned int)cur_range,(unsigned int *)EECRNGE);
				}
				stop_motor();
#if AUTOMATE
				if((motor_status() == M_Started) && ((automation_code ==1) && (password_set==1)))
				{
					print_ssd_delay("\n\rMOTR_NOT_STOPD\n\r",0,500);
					display_device_vals();
					while(1)
					{
						while_delay();
						print_ssd_delay("\rREL_PROB",0,550);
					}
				}
#endif
				ftb_flg = 'T';
				my_eeprom_write_byte((unsigned char)ftb_flg, (unsigned char *)EEFTB);
#if AUTOMATE
				if((automation_code ==1) && (password_set ==1))
				{

					print_ssd_delay("\n\rCALIB SUCESS",0,500);
					display_device_vals();

				}	
#else
				send_device_parameters_1();
				my_eeprom_read((void *)ph[0],(const void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);  
				/* Read Farmer phone number from EEPROM */
				ph[0][10] = '\0';
#endif
			}
			else
			{
				stop_motor();
				calib_status = 1;
				my_eeprom_write_byte((unsigned char)calib_status, (unsigned char *)CALIB_STATUS);
				_delay_ms(5000);	
				/** calibration failed, so reboot the device and set it for calibration again **/
			}
		}	
	}
}

/************************************************************************************
 * This function is to answer the call, when waiting in loops for motor calibration.
 ************************************************************************************/ 
unsigned char answer_call(void)
{
	unsigned char ret_res = 0;
#if AUTOMATE
	static char i=0;
#endif
	wdt_reset();

	if((ri == 1) )
	{
		do{    
			ret_res = recv_response(WAIT_GSM_RESPONSE);
			print_ssd_delay((char*)response,0,PRINT_DELAY);
		}while(strcmp((const char*)response,"RING") && (!ret_res));  
		// loop here untill the response is rin

		if(strcmp((const char*)response,"RING")){
			ri = 0;
			return 0;
		}    
		take_call();
		if(authorize())
		{
			return 1;
		}
		else
		{
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
		}	
	}
	else
	{
#if AUTOMATE
		if((automation_code ==1) && (i==10))
		{

			check_sig_reg(); /*switch off the GSM module if signal is less than 5 or 
					   simcard is not registered */
			switchon_gsm();
			i=0;

		}
		i++;
#else
		check_sig_reg(); /*switch off the GSM module if signal is less than 
				   5 or simcard is not registered */
		switchon_gsm();

#endif
	}
	return 0;
}


/** This function reads two phase currents and determine the running status of the motor. 
 * Status is updated in the variable (hw.motor) **/

Motor motor_status(void)
{
	unsigned char i;
	wdt_reset();
	hw.motor=M_Stopped;
/*
	if(single_phs_enable == 1)
	{
#undef MIN_CUR_VAL
#define MIN_CUR_VAL	5
	}
*/
	for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
	{		
		if(adc_conv(i) > MIN_CUR_VAL)
		{
			hw.motor=M_Started;
			break;
		}
	}
	return hw.motor;
}


/************************************************
 * This function is to trigger the start relay.
 ***********************************************/ 

void start_motor(void)
{

#if THREE_BUTTON

	wdt_reset();
	if(motor_status() == M_Stopped)
	{
		if((run_in_which_phase == THREE_PHASE_ANDHRA) || (run_in_which_phase == THREE_PHASE_KARNATAKA))
		{
			SET_PIN(DDRE, PE4);
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(5);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(1);
			SET_PIN(PORTE, PE4); 
			Delay_sec(2);
			CLEAR_PIN(PORTE, PE4);
			Delay_sec(1);
			CLEAR_PIN(DDRE, PE4); // make as input


			prv_status.motor = hw.motor;
			hw.motor = motor_status();
			if(automation_code==0)
				print_ssd_delay("STRD",0,PRINT_DELAY);
		}
		else if(run_in_which_phase == TWO_PHASE)
		{    
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(3);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			start_timer(TWO_TO_THREE_PHASE_DELAY);
			while(!timer_expired)
			{    
				if(motor_status()==M_Started)
				{    
					Delay_sec(8);// wait for star to delta conversion time
					break;
				}    
			}    
			stop_timer();
			Delay_sec(2);
			prv_status.motor = hw.motor;
			hw.motor = motor_status();
			if(automation_code==0)
				print_ssd_delay("STRD",0,500);
		}
	}
	if(hw.motor == M_Started)
	{
		motor_on = 1;
		call_motor_off=0; // added newly
	} 
#else
	wdt_reset();
	if(motor_status() == M_Stopped)
	{
		if((run_in_which_phase == THREE_PHASE_ANDHRA) || (run_in_which_phase == THREE_PHASE_KARNATAKA))
		{
			Delay_sec(1);
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(5);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(2);

			prv_status.motor = hw.motor;
			hw.motor = motor_status();
			if(automation_code==0)
				print_ssd_delay("STRD",0,PRINT_DELAY);
		}
		else if(run_in_which_phase == TWO_PHASE)
		{    
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(3);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			start_timer(TWO_TO_THREE_PHASE_DELAY);
			while(!timer_expired)
			{    
				if(motor_status()==M_Started)
				{    
					Delay_sec(8);// wait for star to delta conversion time
					break;
				}    
			}    
			stop_timer();
			Delay_sec(2);
			prv_status.motor = hw.motor;
			hw.motor = motor_status();
			if(automation_code==0)
				print_ssd_delay("STRD",0,500);
		}
	}
	if(hw.motor == M_Started)
	{
		motor_on = 1;
		call_motor_off=0; // added newly
	} 

#endif

}


/************************************************
 * This function is to trigger the stop relay.
 ***********************************************/ 

void stop_motor(void)
{
	wdt_reset();
	if(motor_status() == M_Started)
	{

		SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
		Delay_sec(2);
		CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
		Delay_sec(1);
		CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
		Delay_sec(1);

		start_timer(5);			/** this delay should be configured based on horse power of motor **/
		while((!timer_expired) && (motor_status() != M_Stopped))
			;    
		stop_timer();

		hw.motor = motor_status();
		if(automation_code==0)
			print_ssd_delay("STOP",0,PRINT_DELAY);
	}
	if(hw.motor == M_Stopped)
	{
		motor_on = 0;
	} 

}
	
void stop_motor_ct_err(void)
{
	wdt_reset();
	SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
	Delay_sec(2);
	CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
	Delay_sec(1);
	CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
	Delay_sec(1);
	hw.motor = motor_status();
	if(hw.motor == M_Stopped)
	{
		motor_on = 0;
	}
}

/****************************************************************************************** 
 * This function reads all operational parameters from the EEPROM  during device start up. 
 ******************************************************************************************/
	
void read_eeprom(void)
{
	char char_temp = 0;
       	int int_temp = 0;	
	r_phase_current = my_eeprom_read_int((unsigned int *)R_PHASE_CURRENT);
	y_phase_current = my_eeprom_read_int((unsigned int *)Y_PHASE_CURRENT);
	b_phase_current = my_eeprom_read_int((unsigned int *)B_PHASE_CURRENT);
	my_eeprom_read((void *)debug_ph, (const void *)EEDEBUG_PH_NUM, NDIGIT_PH_NUM); 
	/* Read admin phone number from EEPROM */
	phase_rev_pro_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_PHASE_REV_PRO_ENABLE);
	//newly added for phase rev
	debug_ph[10] = '\0';

	my_eeprom_read((void *)ph[0],(const void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);     
	/* Read Farmer phone number from EEPROM */
	ph[0][10] = '\0';
	cur_range = my_eeprom_read_int((unsigned int *)EECRNGE);
	dryrun.water_wait = my_eeprom_read_int((unsigned int *)EEDELAY);

	theft_enable = my_eeprom_read_byte((unsigned char *)ETHEFT);
	theft_detected = my_eeprom_read_byte((unsigned char *)EETHFT_OPR);

	dryrun.enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_DRYRUN_PROTECTION);
	nonpeaktime_alerts_disable = my_eeprom_read_byte((unsigned char *)CONFIGURE_NONPEAKTIME_ALERTS);
	if(nonpeaktime_alerts_disable==1)
	{
		my_eeprom_read((void *)nonpeak_start, (const void *)NONPEAK_START, 4);
		my_eeprom_read((void *)nonpeak_end, (const void *)NONPEAK_END, 4);
		nonpeak_start[4] = '\0';
		nonpeak_end[4] = '\0';
	}
	my_eeprom_read((void *)hw.passwd, (const void *)EEPROM_PASSWD_OFFSET, 4);

	byps = my_eeprom_read_byte((unsigned char *)EEBYPASS);

	my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
	serial_number[NDIGIT_SER_NUM] = '\0';

	run_in_which_phase = my_eeprom_read_byte((unsigned char *)EE_PHASE);
	g_sub_phase = my_eeprom_read_byte((unsigned char *)SUB_PHASE);

	lower_volt_range = my_eeprom_read_int((unsigned int *)LOWER_VOLT_RANGE);
	higher_volt_range = my_eeprom_read_int((unsigned int *)HIGHER_VOLT_RANGE);
	//over_current_limit = my_eeprom_read_int((unsigned int *)OVER_CURRENT_LIMIT);
	//water_flow_sensor = my_eeprom_read_byte((unsigned char *)WATER_FLOW_SENSOR);

	auto_mode_delay = my_eeprom_read_int((unsigned int *)AUTO_MODE_DELAY);
	start_motor_delay = my_eeprom_read_int((unsigned int *)START_MOTOR_DELAY);
	debugging_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_DEBUGGING_FEATURE);
	debugging_flag = my_eeprom_read_byte((unsigned char *)CONFIGURE_DEBUGGING_FLAG);
	calib_flag = my_eeprom_read_byte((unsigned char *)EECALIB_FLAG);


	my_eeprom_read((void *)b_ph,(const void *)EEB_PH_NUM, NDIGIT_PH_NUM); 	/* Read admin phone number from EEPROM */
	b_ph[10] = '\0';

	boot_ver = my_eeprom_read_int((unsigned int *)EEBOOT_VERSION);
	/*
	signal_val = my_eeprom_read_int((unsigned int *)EE_SIGNAL_VAL);
	if(signal_val > 99)
	{
		signal_val = 3;
	}
	*/

	call_motor_off = my_eeprom_read_byte((unsigned char *)EE_CALL_MOTOR_OFF); // call motor off
	prev_call_motor_off=call_motor_off;

/*
	higher_hp_delay = my_eeprom_read_int((unsigned int *)HIGHER_HP_DELAY);
	if(higher_hp_delay > 99)
	{
		higher_hp_delay = 2;
	}
*/

	per_reboot = my_eeprom_read_byte((unsigned char *)EE_PER_REBOOT); // read the data of 8 hour reboot
	reboot_counter = my_eeprom_read_int((unsigned int *)EE_REBOOT_CNTR);
	if(per_reboot == 1)
	{
		reboot_counter++;
		my_eeprom_write_int((unsigned int)reboot_counter,(unsigned int *)EE_REBOOT_CNTR);
	}
	notify_sms_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_NOTIFY_SMS);
	admin_activation = my_eeprom_read_byte((unsigned char *)ADMIN_ACTIVATION); 
	// read the admin activation details
	if(!((admin_activation==0) || ((admin_activation ==1))))
	{
		admin_activation=1;
		my_eeprom_write_byte((unsigned char)admin_activation, (unsigned char *)ADMIN_ACTIVATION); 
	}
	sms_ver = my_eeprom_read_byte((unsigned char *)CONFIGURE_SMS_VER);
	enable_auto_monitor = my_eeprom_read_byte((unsigned char *)CONFIGURE_AUTO_MONITOR);
	//automation_done = my_eeprom_read_byte((unsigned char *)AUTOMATION_DONE);
	automation_done = 0;
	single_phs_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_SINGLE_PHS);
	wire_cut_v_level =  my_eeprom_read_int((unsigned int *)WIRECUT_LVL); // read the vale of wirecut voltage level.
	max_ct_val = my_eeprom_read_int((unsigned int *)MAX_CT_VAL_AUTOMATION);

	hyster_value = my_eeprom_read_int((unsigned int *)HYSTER_VALUE);

	all_3_phases_loaded = my_eeprom_read_byte((unsigned char *)ALL_3_PHASES_LOADED);
	
	/* if bootloaded is not loaded then this eeprom location should have Zero*/
	boot_prog_info = my_eeprom_read_byte((unsigned char *)EEBOOT_PROG_INFO);
	char_temp = 0;
	my_eeprom_write_byte((unsigned char)char_temp, (unsigned char *)EEBOOT_PROG_INFO);

	/* if bootloaded is not loaded then this eeprom location should have Zero*/
	boot_ver = my_eeprom_read_int((unsigned int *)EEBOOT_VERSION);
	int_temp = 0;
	my_eeprom_write_int((unsigned int)int_temp,(unsigned int *)EEBOOT_VERSION);
}


void check_bypass_and_theft(void)
{
	static unsigned char theft_informed=0;
	unsigned int a;
	unsigned char i=0, val=0;
#if AUTOMATE
	static char j=0;
#endif

	wdt_reset();
	dtmf_buf[0]='1';

	if(spike_theft==1)
	{
		spike_theft=0;
		Delay_sec(2);
		if(!THEFT_DETECT) 
		{
			// if the jumper is present.
			if(automation_code ==0)
				print_ssd_delay("SPK1",0,500);
			theft_detected = 0;   
			EIMSK |= (1<<INT6);
			val=1;
		}
		else
		{
			// this is added to check the theft 4 times and confirm that theft happened .
			for (i=0;i<=50;i++)
			{
#if AUTOMATE
				if((j ==0) && (automation_code ==1))
				{
					print_ssd_delay("\n\rCHECKI THFT WAIT\n\r",0,500);
					j=1;
				}
#endif
				wdt_reset();
				_delay_ms(100);
				wdt_reset();
				if(!THEFT_DETECT)
				{
					// if jumper is present.
					print_ssd_delay("SPK2",0,500);
					theft_detected = 0;   
					EIMSK |= (1<<INT6);
					val =1;
					break;
				}
				wdt_reset();
			}
			if(val == 0) // once 4 times checking is done then confirm that theft has happened
			{
				if(automation_code ==0)
					print_ssd_delay("THFT",0,500);
				theft_detected = 1;      
				my_eeprom_write_byte((unsigned char)theft_detected, (unsigned char *)EETHFT_OPR);
			}
		}


	}

	if(theft_detected==1)
	{
#if AUTOMATE
		if((automation_code ==1) && (password_set ==1))
		{

			thft__byps_press =2;
			send_serial0('\r');
			for(j=0;j<50;j++)
			{
				send_serial0(' ');
			}
			wdt_reset();
			_delay_ms(400);
			wdt_reset();
			print_ssd_delay("\rTHFT DETECTD PUT JUMPR",0,500);
			wdt_reset();
			_delay_ms(400);
			wdt_reset();

			send_serial0('.');
			if(!THEFT_DETECT)
			{
				Delay_sec(2);
				if(!THEFT_DETECT)
				{
					print_ssd_delay("\n\rTHFT DISB\n\r",0,500);
					theft_detected=0;
					my_eeprom_write_byte((unsigned char)theft_detected, (unsigned char *)EETHFT_OPR);
					EIMSK |= (1 << INT6);
					end_automation();
				}
			}
		}
#else
		print_ssd_delay("THFT",0,500);
		if(!THEFT_DETECT) // if jumper is placed it goes in this condition.
		{
			print_ssd_delay("CCCC",0,500);
			Delay_sec(1);
			if(deactivate_theft())
			{
				theft_informed=0;	
				return;
			}
		}
		else
		{
			check_call();
			strcpy((char *)msg_info,SMS_THEFT);
		}
		if(theft_informed==0)
		{
			theft_informed=1;
			send_sms(ph[0],msg_info);
			Delay_sec(3);
			if(0 != notify())
			{
				audio_out(M_Theft);
				disconnect_call();
			}
		}
#endif
	}   
	else
	{
		if(BYPASS) 
		{    
			_delay_ms(100);
			if(BYPASS)
			{    
				if(!byps)  //if previously byps=0 make it as 1
				{    
					byps=1;
					my_eeprom_write_byte((unsigned char)byps, (unsigned char *)EEBYPASS);
					send_serial0('\n');
					while(BYPASS) 	// wait till button is released
					{
#if AUTOMATE
						if(automation_code ==1)
						{

							CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
							print_ssd_delay("\rBYPS PRESD",0,500);
							SET_PIN(LED_CTRL_PORT,AUTO_LED);
							print_ssd_delay("\rBYPS PRESD",0,500);

						}
#else
						CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
						print_ssd_delay("BYPS",0,100);
						SET_PIN(LED_CTRL_PORT,AUTO_LED);
						print_ssd_delay("BYPS",0,100);
#endif
						wdt_reset();		
					}
				}    
				else     
				{    
					byps=0;
					my_eeprom_write_byte((unsigned char)byps, (unsigned char *)EEBYPASS);
					while(BYPASS) 	// wait till bypass button is released
					{
						CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
						print_ssd_delay("MANU",0,100);
						SET_PIN(LED_CTRL_PORT,AUTO_LED);
						print_ssd_delay("MANU",0,100);
						wdt_reset();				
					}
				}    
			}
		}

		if(byps)
		{
#if AUTOMATE
			if((automation_code ==1) && (password_set==1))
			{

				print_ssd_delay("\n\rBYPS DISBLD\n\r",0,500);
				byps=0;
				my_eeprom_write_byte((unsigned char)byps, (unsigned char *)EEBYPASS);
				hw.mode = M_Manual;
				my_eeprom_write_int((unsigned int)(hw.mode),(unsigned int *)EEPROM_START_MODE);
				SET_PIN(LED_CTRL_PORT,AUTO_LED);
				prv_status.cond = 0;
				prv_status.motor = 0;
				thft__byps_press =1;

			}
#else
			CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
			print_ssd_delay("BYPS",0,100);
			SET_PIN(LED_CTRL_PORT,AUTO_LED);
			print_ssd_delay("BYPS",0,100);
			if(ri)
			{    
				dtmf_recvd=0;
				dtmf_wait_cnt=0;
				dtmf_buf[0]='0';
				do   
				{    
					a = recv_response(100);
					print_ssd_delay((char*)response,0,500);
				}  while (strcmp((const char*)response,"RING") && !a); 

				if(strcmp((const char*)response,"RING")){
					ri =0;
					return;
				}    

				take_call();
				ivrs_buf[0]=ONE;
				ivrs_buf[10]=STAR;

				audio_out(M_Welcome);
				if(authorize())
				{
					while(check_call_status())
					{
						dtmf_recvd = pos = 0;
						while(!dtmf_recvd)
						{
							audio_out(M_BypassMode);
							if(!ongoing_call || pos > 0)
								break;	
							audio_out(M_Enabled);
							if(!ongoing_call || pos > 0)
								break;	
							audio_out(M_ToChangeToManual);
							if(!ongoing_call || pos > 0)
								break;	
							audio_out(M_Press1);
							if(!ongoing_call || pos > 0)
								break;	
							audio_out(M_StarExit);
							if(!ongoing_call || pos > 0)
								break;	
							dtmf_wait();
							if(!ongoing_call)
								return;
						}
						if(!ongoing_call)
							return;

						on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
						if(ONE == ivrs_pos)
						{
							byps=0;
							my_eeprom_write_byte((unsigned char)byps\
									, (unsigned char *)EEBYPASS);
							audio_out(M_Manualmode);
							audio_out(M_ThankUKisanRaja);
							hw.mode = M_Manual;
							my_eeprom_write_int((unsigned int)(hw.mode)\
									,(unsigned int *)EEPROM_START_MODE);
							SET_PIN(LED_CTRL_PORT,AUTO_LED);
							prv_status.cond = 0;
							prv_status.motor = 0;
							break;
						}
						else if(STAR == ivrs_pos)
						{
							audio_out(M_ThankUKisanRaja);
							break;
						}else
						{
							audio_out(M_Incorrect);
							audio_out(M_Reenter);		
						}	
					}
				}
				disconnect_call();

			}

			else if(automation_code ==0) 
			{
				check_sig_reg();
			}

#endif
		}
	}
}


/*
 * when theft is detected, if anyone other than the farmer calls the device, 
 * then answer the call and then sms the caller number as thief number to the 
 * farmer and as well as to the vinfinet admin number.
 */

void check_call(void)
{
	unsigned char a;

	call_status=0;

	if(ri)
	{    
		do   
		{    
			a = recv_response(100);
			print_ssd_delay((char*)response,0,500);
		}while (strcmp((const char*)response,"RING") && !a); 

		take_call();
		audio_out(M_Theft);
		disconnect_call();

		if(0 != strcmp((const char*)ph, (const char*)caller_number))	
		{	
			get_number(0);
			Delay_sec(7);
			send_sms(ph[0],msg_info);
			Delay_sec(7);
			get_number(1);
			send_sms(admin_ph[0],msg_info);
			Delay_sec(7);
		}
	}    
}


/*
 * To restore the device from theft detected state
 */

unsigned char deactivate_theft(void)
{
	int chance, i, admin_theft=0;

	if(ri) 
	{    
		do   
		{    
			i = recv_response(100);
			print_ssd_delay((char*)response,0,500);
		}while(strcmp((const char*)response,"RING") && !i); 

		if(strcmp((const char*)response,"RING")){
			ri=0;
			return 0;
		}    

		take_call();
		chance=3;
		if(authorize())		
		{    
			pos=0;
			audio_out(M_Theft);
			audio_out(M_EnterSerialNumber);

			while(chance)
			{    
				start_timer(LONG_WAIT_FOR_DTMF_INPUT);
				while(pos<6)
				{    
					if(timer_expired==1){
						audio_out(M_NoResponse);
						break;
					}
					print_ssd_delay(NULL,pos,250);

					if((dtmf_buf[0] == ASCII_STAR) && (dtmf_buf[5] == ASCII_HASH))
						admin_theft = 1;
				}
				stop_timer();
				if(0 == check_call_status())
					return 0;

				if(timer_expired==1)
				{
					if(--chance){
						audio_out(M_EnterSerialNumber);
						pos = admin_theft = 0;
					}
				}
				else if(admin_theft==1)		/* call from any number and enter *12121212#    */
				{
					//print_ssd_delay("AMIN",0,1000);
					dtmf_buf[6]='\0';
					if(!strcmp((const char*)dtmf_buf,"*1212#"))
					{
						theft_detected=0;
						my_eeprom_write_byte((unsigned char)theft_detected\
								, (unsigned char *)EETHFT_OPR);
						audio_out(M_TheftControl);
						audio_out(M_Enabled);
						audio_out(M_ThankUKisanRaja);
						disconnect_call();
						Delay_sec(5);
						strcpy((char *)msg_info, SMS_THEFT_RESTORED);
						send_sms(admin_ph[0], msg_info);
						EIMSK |= (1 << INT6);
						return 1;
					}
					else
					{
						audio_out(M_Serial);
						audio_out(M_Incorrect);
						if(--chance){
							audio_out(M_Reenter);
							pos = admin_theft = 0;
						}
					}
				}
				else 	/* call from any number and enter the serial number of the device */
				{
					dtmf_buf[10]='\0';
					if(!strcmp((const char *)serial_number,(const char *) dtmf_buf))
					{
						theft_detected=0;
						my_eeprom_write_byte((unsigned char)theft_detected\
								, (unsigned char *)EETHFT_OPR);
						audio_out(M_TheftControl);
						audio_out(M_Enabled);
						audio_out(M_ThankUKisanRaja);
						disconnect_call();
						Delay_sec(5);
						strcpy((char *)msg_info, SMS_THEFT_RESTORED);
						send_sms(admin_ph[0], msg_info);
						EIMSK |= (1 << INT6);
						return 1;
					}
					else
					{
						audio_out(M_Serial);
						audio_out(M_Incorrect);
						if(--chance){
							audio_out(M_Reenter);
							pos = admin_theft = 0;
						}	
					}
				}
			}
			if(chance ==0)
				disconnect_call();
		}

		if(0 != strcmp((const char *)ph, (const char *)caller_number))	
		{
			get_number(0);
			disconnect_call();
			Delay_sec(7);
			send_sms(ph[0],msg_info);
			Delay_sec(10);
			get_number(1);
			send_sms(admin_ph[0],msg_info);
			Delay_sec(5);
		}
	}
	else 
	{
		check_sig_reg();
	}
	return 0;
}


/* This function accepts start time and duration for timer mode-2 from user and stores them to eeprom */
	
unsigned char get_duration(void)
{
	unsigned int dur2;

	audio_out(M_EnterStartTime);
	if(get_time() == 0)
		return 0;

	strcpy((char *)start, (const char*)dtmf_buf);

	audio_out(M_EnterDuration);
	dur2 = get_time();
	if(dur2==0)
		return 0;

	if(twice_timer == 1)
	{
		audio_out(M_Two);
		audio_out(M_EnterStartTime);
		get_time();
		check_call_status();
		if(!ongoing_call)
			return 0;
		strcpy((char*)start_sec,(const char*)dtmf_buf);
		my_eeprom_write((const void *)start_sec,(void *)EESTART_SECOND_TIME,5);
	}

	my_eeprom_write((const void *)start,(void *)EESTART_TIME,5);
	my_eeprom_write_int((unsigned int)dur2, (unsigned int *)EEDURSET);

	tmr_motor_status = 1;
	tmr2_time_check_flg = 0;
	tmr_mode=2;
	my_eeprom_write_byte((unsigned char)tmr2_time_check_flg, (unsigned char *)TMR2_TIME_CHECK_FLG);
	my_eeprom_write_byte((unsigned char)(tmr_mode),(unsigned char *)EETM_MODE);
	my_eeprom_write_byte((unsigned char)(tmr_motor_status),(unsigned char *)EEPROM_TIMER_FLAG);

	return 1;
}


/********************************************************************
 * This function is responsible for operating the motor in AUTO mode. 
 ********************************************************************/
	void
auto_mode(void)
{
	unsigned char start_call=0;

	hw.cond=check_favourable();
	wirecut_detection(); // added newly
	print_ssd_delay("AUTO",0,SHORT_PRINT_DELAY);
	/*if call_motor_off variable is updated then store its value in EEPROM */
	if(call_motor_off != prev_call_motor_off)
	{
		my_eeprom_write_byte((unsigned char)call_motor_off,(unsigned char *)EE_CALL_MOTOR_OFF);
		// periodic reboot make it 0
		prev_call_motor_off=call_motor_off;
	}

	/***** if previous status of power is not equal to present status of power, then inform farmer *****/
	if(prv_status.cond != hw.cond) 
	{
		/*****  case 1: power available *****/
		if(hw.cond == C_Favourable) 		
		{
			Delay_sec(auto_mode_delay);		
			/** Configurable delay before starting the motor in auto mode **/
			hw.cond = check_favourable();
			if(hw.cond == C_Favourable) 		
			{
				start_call = 1;
				start_motor();

				/***** case 2: check for wire cut in 'R' and 'B' phases.
				 * Motor will not be started if R or B phase wires are cut *****/
				if(motor_status() == M_Stopped)	
				{
					stop_motor_ct_err();

					hw.err = E_WireCut;
					call_motor_off = 1;// do not start the motor again,
					//start the motor only when power goes and comes
					strcat((char *)msg_info, SMS_WIRE_CUT);
				}
				else	
				{
					/***** case 3: motor started, so check for wire cut and dry run *****/
					if(dryrun.enable==1)
						dryrun.startup_flg = 1;

					prv_status.err = hw.err = 0;
					hw.cond = check_favourable();
					dryrun.startup_flg = 0;
					if(motor_status() == M_Stopped)
					{
						call_motor_off=1;
						if((hw.err != E_WireCut) && (hw.err != E_NoWater))
						{
							return;	/** return from here if power cut or manual 
								  stop has happened while execution was 
								  in water wait delay **/
						}
					}
					else	
						strcat((char *)msg_info,"\nMOTR ON");
				}
			}
			else
				return;
		}
		else			
		{
			/** this statement is for avoiding calls in single phase **/
			if((motor_status() == M_Started) && (hw.err == E_LowVoltage))	
				stop_motor();
			else if(hw.err == E_LowVoltage)	
				return;

			start_call=1;
			/***** case 4: Earlier motor was running and now power is OFF *****/
			if((prv_status.motor==M_Started) && (hw.err==E_NoPower))
			{
				if(run_in_which_phase == TWO_PHASE)	
					CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
				stop_motor();
				strcat((char *)msg_info,"\nMOTR STOPD");
			}
		}
	}
	/***** if previous status of motor is not equal to present status of motor, then inform farmer *****/
	else if(prv_status.motor != hw.motor) 
	{
		/****  Motor stopped manually from starter, ****  Motor started manually from starter 
		 ****  Motor stopped due to wirecut, ****  Motor stopped due to no water	*****/
		start_call=1;
	}
	/* added newly,in field if power goes and comes within 1 or 2 minutes then 
	 * kisan raja is not starting the motor
	 to avoid this ,even if the motor is stopped manually for starter let it 
	 inform that motor has stopped manually and 
	 let it start the motor again.Only if the motor is stopped from mobile, 
	 dryrun happens , high current and wirecut             
	 then only motor will be stopped ,else motor will start again*/
	//else if((hw.cond == C_Favourable) && (call_motor_off == 0) && (hw.motor == M_Stopped))
	if(enable_auto_monitor == 1)
	{
		if((hw.cond == C_Favourable) && (call_motor_off == 0) && ( M_Stopped == motor_status()))
		{
			call_motor_off=0; 	// added newly 
			prv_status.cond =0; 	// next time when it comes to auto mode function it 
			//will start the motor again.
			return;	
		}
	}
	if(start_call == 1) 
	{
		strcat((char *)msg_info, "\nAUTO MODE");
		hw.motor = motor_status();
		if(!call_status)
		{
			if(notify() == 0)
				return;
		}

		if(hw.cond == C_Favourable)
		{
			/*****	for favourable conditions like "motor started", 
			 * "manual start" and "manual stop" cases, jump to handle_level1() function	*****/
			if((hw.motor == M_Started)  || (hw.err == E_ManualStart) || (hw.err == E_ManualStop))
			{
				st.level=L1;
				return;
			}
			/***** motor stopped because of wire cut or no water *****/
			else if((hw.err == E_WireCut) || (hw.err == E_NoWater) || (E_OverCurrent == hw.err))		
			{
				play_unfavourable_msg(hw.err);
				prv_status.err = hw.err;
			}
		}
		else if(hw.cond == C_UnFavourable)
		{
			/*****	motor is switched off due to non-availability of power	*******/
			if((prv_status.motor == M_Started) && (hw.err == E_NoPower)) 	
			{
				audio_out(M_OffNoPower);		
			}
			/***** input power supply fuses have been removed *****/
			else if(hw.err == E_WireCut)		
			{
				audio_out(M_NoStartWireCut);
				if(motor_status() == M_Started)
					audio_out(M_MotorStartedManually);
				prv_status.err = hw.err;
			}
			else if((hw.err == E_HighVoltage)||(hw.err == E_LowVoltage))
			{
				audio_out(M_OffNoOptVoltage);
				prv_status.err = hw.err;
			}
			else if(hw.err == E_Ph_reverse) // phase reversal
			{
				audio_out(M_Three);
				audio_out(M_Incorrect);
				audio_out(M_NoStartNoOptVoltage);
				/*if(hw.motor == M_Stopped)
				  {
				  audio_out(M_MotorStopSuccess);

				  }*/

				prv_status.err = hw.err;

			}
			/***** Earlier power was ON and now power is OFF *****/
			else if(hw.err == E_NoPower)
			{
				st.level = L1;	
				return;	
			}		
		}
		audio_out(M_ThankUKisanRaja);
		disconnect_call();
		prv_status.cond = hw.cond;
		prv_status.motor = hw.motor;
		hw.err=0;
	}
}


/**************************************************************************** 
 * This function facilitates the user to choose timer mode-1 or timer mode-2 
 ****************************************************************************/

void get_timer_mode_details(void)
{
	int dur;
	unsigned char tmr_first_start = 0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;
	ivrs_buf[2]=THREE;

	while(L1 == ivrs.level)
	{
		dtmf_recvd = dtmf_wait_cnt = pos = 0;
		while(!dtmf_recvd)
		{
			if(dtmf_wait_cnt == DTMF_WAIT_CNT){
				ivrs.level = LInvalid;
				return;
			}
			audio_out(M_InTimerMode);
			dtmf_wait();
			if(!ongoing_call)
				return;
		}
		on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
		switch(ivrs_pos)
		{
			case ONE:
				tmr_mode=1;
				audio_out(M_EnterDuration);
				dur = get_time();
				if(dur==0)
				{
					ivrs.level=LInvalid;
					return;
				}
				my_eeprom_write_int((unsigned int)dur, (unsigned int *)EEDURSET);
				hw.mode=M_Timer;
				my_eeprom_write_int((unsigned int)(hw.mode),(unsigned int *)EEPROM_START_MODE);
				my_eeprom_write_byte((unsigned char)tmr_mode,(unsigned char *)EETM_MODE);
				tmr_motor_status = 1;	
				timer1_expired = 0;
				tmr_first_start = 1;
				my_eeprom_write_byte((unsigned char)tmr_first_start, (unsigned char *)TMR_FIRST_START);
				ivrs.level = LInvalid;
				break;
			case TWO:
				if(!get_duration())
				{
					ivrs.level=LInvalid;
					return;
				}
				hw.mode=M_Timer;
				my_eeprom_write_int((unsigned int)(hw.mode),(unsigned int *)EEPROM_START_MODE);
				timer1_expired = 0;
				tmr_first_start = 1;
				my_eeprom_write_byte((unsigned char)tmr_first_start, (unsigned char *)TMR_FIRST_START);
				ivrs.level = LInvalid;
				break;
			case THREE:
				return;
			case STAR:
				ivrs.level = LInvalid;
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);	
				break;
		}
	}
}


/*********************************************************************** 
 * This function is responsible for operating the motor in timer mode 
 * 'tmr_motor_status' variable indicates motor status.
 * tmr_motor_status = 0 --> motor is ON
 * tmr_motor_status = 1 --> motor is OFF
 ***********************************************************************/

void timer_mode(void)
{
	unsigned int dur = 0;
	unsigned char clk[5], start_call = 0, tmr_first_start = 0;

	/***** Timer expires after running the motor for stipulated time. 
	 * Move the device to manual mode on timer expiry. *****/
	if(timer1_expired==1)
	{
		stop_motor();
		stop_timer1();
		timer1_expired=0;

		/*	if(tmr_mode == 1)
			strcpy((char *)msg_info, "MOTOR STOPPED TIMER-1");
			else 
			strcpy((char *)msg_info, "MOTOR STOPPED TIMER-2");
			*/
		notify();
		audio_out(M_RanToCompletion);

		if(1 == cyclic_timer)
		{
			audio_out(M_ThankUKisanRaja);
			disconnect_call();

			tmr_motor_status = 1;
			tmr2_time_check_flg = 0;
			tmr_mode=2;
			my_eeprom_write_byte((unsigned char)tmr2_time_check_flg\
					, (unsigned char *)TMR2_TIME_CHECK_FLG);
			my_eeprom_write_byte((unsigned char)(tmr_mode),(unsigned char *)EETM_MODE);
			my_eeprom_write_byte((unsigned char)(tmr_motor_status)\
					,(unsigned char *)EEPROM_TIMER_FLAG);
			return;	
		}

		hw.mode = M_Manual;
		my_eeprom_write_int((unsigned int)(hw.mode),(unsigned int *)EEPROM_START_MODE);
		audio_out(M_Manualmode);			/* Device has been changed to Manual Mode */
		audio_out(M_ThankUKisanRaja);
		disconnect_call();
		return;
	}

	hw.cond = check_favourable();

	if((tmr_mode == TIMER_MODE_2) && (tmr2_time_check_flg == 0))
	{
		get_clock((unsigned char *)clk);
		Delay_sec(GSM_DELAY);
		clk[4] = '\0';
		print_ssd_delay((char *)clk,0,PRINT_DELAY);
		if((strcmp((const char*)clk, (const char*)start) == 0 )|| (twice_timer \
					&& (strcmp((const char*)clk,(const char*)start_sec)==0)))
		{
			tmr2_time_check_flg = 1;
			my_eeprom_write_byte((unsigned char)tmr2_time_check_flg, (unsigned char *)TMR2_TIME_CHECK_FLG);
			start_call = 1;
			tmr_motor_status = 1;
			if(hw.cond == C_UnFavourable)	
				strcat((char *)msg_info, "MOTR STOPD");
		}
		else	
		{
			return;		/***** Current time is not equal to set time for timer-2. 
					  So return from here *****/
		}
	}

	/***** For Wire Cut, No Water and Manual Stop cases, inform farmer, 
	 * stop the timer and move the device to manual mode *****/ 
	if((hw.err == E_WireCut) || (hw.err == E_NoWater) || (hw.err == E_ManualStop) \
			|| (E_OverCurrent == hw.err)||((motor_status() == M_Started)\
				&&((E_HighVoltage == hw.err)||(hw.err == E_LowVoltage))))
	{
		stop_motor();
		strcat((char *)msg_info, "\nMANL MODE");
		start_call = 1;
		tmr_motor_status = 1;
		hw.cond = C_UnFavourable;
	}

	/** Before completion of the set duration. If motor stopped due to no power, 
	 * then stop the timer and calculate the remaining time duration **/
	if(!tmr_motor_status)
	{
		if(hw.err == E_NoPower)
		{
			if(run_in_which_phase == TWO_PHASE)	
				CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			stop_motor();
			dur = (timer1_delay - tmr1_count)/10 ;
			stop_timer1();
			my_eeprom_write_int((unsigned int)dur, (unsigned int *)EEDURSET);
			tmr_motor_status=1;
			//print_ssd_delay("OFF ",0,PRINT_DELAY);
			strcat((char *)msg_info, "MOTR OFF");
		}
	}
	else if(hw.cond == C_Favourable)
	{
		tmr_first_start = my_eeprom_read_byte((unsigned char *)TMR_FIRST_START);
		if(tmr_first_start)
		{	
			start_call = 1;
			tmr_first_start = 0;
			my_eeprom_write_byte((unsigned char)tmr_first_start, (unsigned char *)TMR_FIRST_START);
		}
		tmr_motor_status = 0;
		if(run_in_which_phase == TWO_PHASE)
			Delay_sec(5);	
		start_motor();

		/***** case 2: check for wire cut in 'R' and 'B' phases. 
		 * Motor will not be started if R or B phase wires are cut *****/
		if(motor_status() == M_Stopped)	
		{
			hw.err = E_WireCut;
			strcat((char *)msg_info, SMS_WIRE_CUT);
		}
		else	
		{
			/***** case 3: motor started, so check for wire cut and dry run *****/
			if(dryrun.enable)
				dryrun.startup_flg = 1;

			if(run_in_which_phase == TWO_PHASE)
				Delay_sec(3);	

			hw.cond = check_favourable();
			dryrun.startup_flg = 0;
			if(motor_status() == M_Stopped)
			{
				if((hw.err != E_WireCut) && (hw.err != E_NoWater)){
					tmr_motor_status = 1;
					return;	/** return from here if power cut or manual stop has
						  happened while execution was in water wait delay **/
				}
			}
			else
			{	
				strcat((char *)msg_info, "\nMOTR ON");
				dur = my_eeprom_read_int((unsigned int *)EEDURSET);
				start_timer1(dur);
				//print_ssd_delay(" ON ",0, SHORT_PRINT_DELAY);
			}
		}
	}

	if(start_call) 
	{
		if(((hw.err != E_WireCut) && (hw.err != E_NoWater) && (hw.err != E_ManualStop) \
					&& (E_OverCurrent != hw.err))||((hw.err != E_HighVoltage)))
		{
			/*if(tmr_mode == TIMER_MODE_1)
			  strcat((char *)msg_info, "\nDEVICE IN TIMER MODE-1");
			  else
			  strcat((char *)msg_info, "\nDEVICE IN TIMER MODE-2");*/
		}

		hw.motor = motor_status();
		if(!call_status)
		{
			if(notify() == 0)
			{
				if((hw.err == E_WireCut) || (hw.err == E_NoWater) \
						|| (hw.err == E_ManualStop) || (E_OverCurrent == hw.err)\
						||(E_HighVoltage == hw.err))
				{
					stop_timer1();
					hw.mode=M_Manual;
					my_eeprom_write_int((unsigned int)(hw.mode), (unsigned int *)EEPROM_START_MODE);
				}
				hw.err = 0;
				return;
			}
		}

		audio_out(M_MotorInTimer);	/** audio to play that currently motor is in timer mode **/

		/***** For Wire Cut, No Water and Manual Stop cases, 
		 * stop the timer and move the device to manual mode *****/ 
		if((hw.err == E_WireCut) || (hw.err == E_NoWater) || (hw.err == E_ManualStop) \
				|| (E_OverCurrent == hw.err)||(E_HighVoltage == hw.err)||(hw.err == E_LowVoltage))
		{
			stop_timer1();
			prv_status.err = hw.err;
			play_unfavourable_msg(hw.err);
			audio_out(M_Manualmode);			
			hw.mode=M_Manual;
			my_eeprom_write_int((unsigned int)(hw.mode), (unsigned int *)EEPROM_START_MODE);
		}
		/*****	motor is switched off due to non-availability of power	*******/
		if((prv_status.motor == M_Started) && (hw.err == E_NoPower)) 	
		{
			audio_out(M_OffNoPower);		
		}
		/*****	motor started or earlier power was ON and now power is OFF, 
		 * jump to handle_level1() function and announce audio *****/
		else if((hw.motor == M_Started) || (hw.err == E_NoPower))
		{
			st.level=L1;
			return;
		}
		audio_out(M_ThankUKisanRaja);
		disconnect_call();
		prv_status.cond = hw.cond;
		prv_status.motor = hw.motor;
		hw.err=0;
	}

	/*	if(tmr_mode == TIMER_MODE_1)
		print_ssd_delay("TMR1",0, SHORT_PRINT_DELAY);
		else
		print_ssd_delay("TMR2",0, SHORT_PRINT_DELAY);*/
}


/**********************************************************************
 * This function is responsible for operating the motor in manual mode. 
 **********************************************************************/
	
void manual_mode(void)
{
	unsigned char start_call = 0;

	hw.cond = check_favourable();
	wirecut_detection(); // added newly
	print_ssd_delay("MANU",0,PRINT_DELAY);

	/***** If previous condition is not equal to present condition, then inform farmer *****/
	if(hw.cond != prv_status.cond)
	{
		/* code to handle voltage fluctuation*/	


		if(hw.cond == C_Favourable)
		{
			Delay_sec(auto_mode_delay);		
			/** Configurable delay before starting the motor in auto mode **/
			hw.cond = check_favourable();

			if(hw.cond == C_UnFavourable)
			{
				return;
			}
		}
		else
		{
			Delay_sec(auto_mode_delay);		
			/** Configurable delay before starting the motor in auto mode **/

			hw.cond = check_favourable();

			if(hw.cond == C_Favourable)
			{
				return;
			}

		}

		/** this statement is for avoiding calls in single phase **/
		if((motor_status() == M_Started) && (hw.err == E_LowVoltage))	
			stop_motor();
		else if(hw.err == E_LowVoltage)	
			return;

		if((prv_status.err == E_WireCut) && (hw.cond == C_Favourable))
		{
			hw.err = prv_status.err = 0;
			strcpy((char *)msg_info, SMS_FAV);
		}
		start_call = 1;
		/******* case 1: Earlier motor was running and now power is OFF *********/
		if((prv_status.motor == M_Started) && (hw.err == E_NoPower)) 
		{
			if(run_in_which_phase == TWO_PHASE)	
				CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			stop_motor();
			strcat((char *)msg_info, "\nMOTR STOPD");
		}
	}
	/***** If previous status of motor is not equal to present status of motor, then inform farmer *****/
	else if(hw.motor != prv_status.motor) 
	{
		/**** case 2: Motor stopped manually from starter 
		 **** case 3: Motor started manually from starter 
		 **** case 4: Motor stopped due to wirecut 
		 **** case 5: Motor stopped due to no water	*****/
		start_call = 1;
	}

	if(start_call)
	{
		strcat((char *)msg_info, "\nMANUAL MODE");
		if(!call_status)
		{
			if(notify() == 0)
				return;
		}

		//	audio_out(M_ManualMode);		
		//	/** audio to play that currently motor is in manual mode **/

		if(hw.cond == C_UnFavourable) 
		{
			/*****	motor is switched off due to non-availability of power	*******/
			if((prv_status.motor == M_Started) && (hw.err == E_NoPower)) 	
			{
				audio_out(M_OffNoPower);
			}
			/** input power supply fuses have been removed **/
			else if(hw.err == E_WireCut)
			{
				audio_out(M_NoStartWireCut);		
				if(motor_status() == M_Started)
					audio_out(M_MotorStartedManually);
				prv_status.err = hw.err;
			}
			else if((hw.err == E_HighVoltage) || (hw.err == E_LowVoltage))
			{
				audio_out(M_OffNoOptVoltage);
				prv_status.err = hw.err;
			}
			else if(hw.err == E_Ph_reverse) // phase reversal
			{
				audio_out(M_Three);
				audio_out(M_Incorrect);	
				audio_out(M_NoStartNoOptVoltage);
				/*if(hw.motor == M_Stopped)
				  {
				  audio_out(M_MotorStopSuccess);

				  }*/

				prv_status.err = hw.err;

			}
			/***** Earlier power was ON and now power is OFF *****/
			else if(hw.err == E_NoPower)
			{
				st.level = L1;	
				return;	
			}		
		}
		else if(hw.cond == C_Favourable)
		{
			if((hw.err == E_WireCut) || (hw.err == E_NoWater) || (E_OverCurrent == hw.err))
			{
				play_unfavourable_msg(hw.err);
				prv_status.err = hw.err;
			}
			/*****	for favourable conditions like "Power On", 
			 * "manual start" and "manual stop" cases, jump to handle_level1() function	*****/
			else	
			{
				st.level=L1;
				return;
			}
		}
		audio_out(M_ThankUKisanRaja);
		disconnect_call();
		prv_status.cond = hw.cond;
		prv_status.motor = hw.motor;
		hw.err=0;
	}
}


/* This function checks the KISANRAJA's mode of operation, and invokes appropriate course of actions */
	
void operate(void)
{
	/***** store present mode of the device in eeprom *****/
	if(hw.mode != mode_change)
	{
		my_eeprom_write_int((unsigned int)(hw.mode), (unsigned int *)EEPROM_START_MODE);
		mode_change = hw.mode;

		/***** Turn ON Auto LED for auto mode and timer mode and turn it OFF for manual mode *****/
		if(hw.mode == M_Manual){ 
			SET_PIN(LED_CTRL_PORT, AUTO_LED);
		}else{
			CLEAR_PIN(LED_CTRL_PORT, AUTO_LED);
		}
		/** the following statement is required to inform power and motor status after mode change **/ 
		prv_status.cond = 0;
	}

	switch(hw.mode)
	{
		case M_Auto:
			auto_mode();
			break;
		case M_Timer:
			timer_mode();
			break;
		case M_Manual:
			manual_mode();
			break;
		default:
			hw.mode = M_Manual;
			break;
	}
}


/* initialize the mode of operation of device. If mode is invalid, set to Manual */
	
void init_operations(void)
{
	hw.mode = my_eeprom_read_int((unsigned int*)EEPROM_START_MODE);            
	/*Read the KISANRAJA mode from EEPROM */
	if(hw.mode == M_Invalid)
	{
		hw.mode = M_Manual;
		SET_PIN(LED_CTRL_PORT,AUTO_LED);
		my_eeprom_write_int((unsigned int)(hw.mode),(unsigned int *)EEPROM_START_MODE);
	}
	else if(hw.mode == M_Timer)
	{
		CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
		tmr_mode = my_eeprom_read_byte((unsigned char *)EETM_MODE);
		if(tmr_mode == TIMER_MODE_2)
		{
			tmr2_time_check_flg = my_eeprom_read_byte((unsigned char *)TMR2_TIME_CHECK_FLG);
			my_eeprom_read((void *)start,(const void *)EESTART_TIME,5);

			(tmr_mode) = my_eeprom_read_byte((unsigned char *)EETM_MODE);
			(tmr_motor_status) = my_eeprom_read_byte((unsigned char *)EEPROM_TIMER_FLAG); 

			cyclic_timer = my_eeprom_read_byte((unsigned char *)EEPROM_CYCLIC_TIMER);	
			twice_timer = my_eeprom_read_byte((unsigned char *)EEPROM_TWICE_TIMER);	
			if(1 == twice_timer)
				my_eeprom_read((void *)start_sec,(const void *)EESTART_SECOND_TIME,5);
		}
	}
	else if(hw.mode == M_Auto)
		CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
	else
		SET_PIN(LED_CTRL_PORT,AUTO_LED);

	SET_PIN(DDRE, MOTOR_CTRL_STRT);                 /*Switch OFF the Motor START Relay*/
	SET_PIN(DDRE, MOTOR_CTRL_STP);                  /*Switch OFF the Motor STOP Relay*/
}

//*30#
int init_set_date(void)
{
	int i=0;
	//print_ssd_delay("PASS",0,1000);
	audio_out(M_EnterStartTime);

	pos=0;
	start_timer(30);

	while(pos < 6)
	{
		print_ssd_delay(NULL,pos,500);
		if(timer_expired)
		{
			audio_out(M_NoResponse);
			break;
		}
	}
	stop_timer();
	if(timer_expired)
		return 0;
	dtmf_buf[6] = '\0';

	//	for(i=0; i<6; i+=4)
	//		print_ssd_delay((char *)dtmf_buf+i,0,500);

	audio_out(M_EnteredTime);
	play_time((unsigned char *)dtmf_buf);
	play_time((unsigned char *)dtmf_buf+2);
	play_time((unsigned char *)dtmf_buf+4);

	i = atoi((const char *)dtmf_buf+4);
	my_eeprom_write_int((unsigned int)i,(unsigned int *) EEPROM_PREV_DAY);	//2 byte int
	i = 0;
	i = my_eeprom_read_int((unsigned int *) EEPROM_PREV_DAY);		//2 byte int
	print_ssd_delay(NULL,i,500);

	set_date((char*)dtmf_buf);

	Delay_sec(1);
	return 1;
}



unsigned char set_date(char *buff)
{
	unsigned char i = 0;


	send_serial_string("AT+CCLK=\"");

	while(i < 6)
	{
		send_serial(buff[i]);
		i++;
		if(((i%2) == 0) && (i < 6))
			send_serial('/');
	}

	send_to_gsm_string(",00:00:00+00\"");
	recv_response(100);
	get_date((unsigned char*)buff);
	return 0;
}



//*04#
/* This function accepts the time entered by the farmer at start up and sets it as GSM time */
unsigned char init_set_time(void)
{
	unsigned int chance;
	unsigned char n;

	chance=3;
	wdt_reset();
	while(chance!=0)
	{
		dtmf_buf[0]='0';
		dtmf_buf[1]='0';
		dtmf_buf[2]='0';
		dtmf_buf[3]='0';
		dtmf_buf[4]='\0';

		audio_out(M_EnterStartTime);
		pos=0;
		start_timer(LONG_WAIT_FOR_DTMF_INPUT);
		n=0;
		while(pos < 2) {
			print_ssd_delay((char *)dtmf_buf,0,500);
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				--chance;
				n=1;
				break;
			}
		}
		stop_timer();
		if(n==0)
		{
			audio_out(M_EnterMinutes);
			start_timer(LONG_WAIT_FOR_DTMF_INPUT);
			while(pos < 4) {
				print_ssd_delay((char *)dtmf_buf,0,500);
				if(timer_expired==1)
				{
					audio_out(M_NoResponse);
					--chance;
					n=1;
					break;
				}
			}
			if(n==0)
			{
				stop_timer();
				print_ssd_delay((char *)dtmf_buf,0,500);
				audio_out(M_EnteredTime);               /* The Entered Time is */

				if ((strcmp("59", (const char*)dtmf_buf+2) < 0) \
						|| (strcmp("2359",(const char*) dtmf_buf) < 0))
				{
					print_ssd_delay("ERR ",0,1000);
					--chance;
					audio_out(M_Incorrect);
				}
				else
				{
					play_time((unsigned char *)dtmf_buf);
					audio_out(M_Hours);
					play_time((unsigned char *)dtmf_buf+2);
					audio_out(M_Minutes);

					set_clock((char*)dtmf_buf);
					Delay_sec(1);
					return 0;
				}
			}
		}
	}
	return 1;
}




/* After calibration process, this function will read 'current range' value from the user. 
 * Current range value will be used to detect dry run */
unsigned char init_cur_range(unsigned char phase)
{
	char buff[10];
	announce_Cload(phase);
	audio_out(M_Welcome);

	if(phase == TWO_PHASE)
	{
		cur_range = my_eeprom_read_int((unsigned int *)TWPH_EECRNGE);
		itoa(cur_range, buff, 10);	
		play_number((char*)buff);
	}
	else
	{
		cur_range = my_eeprom_read_int((unsigned int *)EECRNGE);
		itoa(cur_range, buff, 10);	
		play_number((char*)buff);
	}

	audio_out(M_Enter4DigitPasswd);
	dtmf_buf[0] = '0';
	dtmf_buf[1] = '0';
	dtmf_buf[2] = '0';
	dtmf_buf[3] = '0';

	//print_ssd_delay("CRAN",0,500);
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	pos=0;
	while(pos < 4) {
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	if(timer_expired==1)
		return 1;
	dtmf_buf[4] = '\0';

	cur_range = atoi((const char*)dtmf_buf);
	if(phase == TWO_PHASE)
		my_eeprom_write_int((unsigned int)cur_range,(unsigned int *)TWPH_EECRNGE);
	else
		my_eeprom_write_int((unsigned int)cur_range,(unsigned int *)EECRNGE);

	itoa(cur_range, buff, 10);	
	play_number((char*)buff);

	return 0;
}


/********************************************************************************************************************** 
 * During calibration, this function will wait for the user to enter the time for which the calibration has to be done. 
 **********************************************************************************************************************/

unsigned char get_calibration_time(void)
{
	unsigned char ftb_flg=0, calb_min=0, loop=0, calib_status=0;

	calib_status = my_eeprom_read_byte((unsigned char *)CALIB_STATUS);

	while(!loop)
	{
		//battery_low_sensing(); // delete this.

		/** if the motor has started, then take the default calibration time as five minutes and return **/
		if(motor_status()==M_Started)
		{
			calb_min=5;
			return calb_min;
		}
		else if(answer_call())
		{
			audio_out(M_Welcome);
			pos=0;
			audio_out(M_Calibration);
			if(calib_status)
				audio_out(M_Incorrect);
			audio_out(M_EnterMinutes);

			start_timer(LONG_WAIT_FOR_DTMF_INPUT);
			while((pos < 2) && (!timer_expired)) 
				print_ssd_delay(NULL,pos,BLINK_DELAY);

			stop_timer();
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				loop=0;
			}
			else
			{
				calb_min = (dtmf_buf[0] - '0')*10 + (dtmf_buf[1] - '0');
				if(calb_min >= 5)
					calb_min=5;
				print_ssd_delay(NULL,calb_min,PRINT_DELAY);
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				return calb_min;
			}
		}
		/** to skip calibration, just enter into debug board menu and exit from it **/
		calib_val_complete=0;
		if(calib_val_complete==1)
		{
			reg_sim_num();
			ftb_flg='T';
			wdt_reset();
			eeprom_busy_wait();
			my_eeprom_write_byte((unsigned char)ftb_flg, (unsigned char *)EEFTB);
			eeprom_busy_wait();
			_delay_ms(3000);
		}
		/** LED indication for a device in calibration. Power LED will be blinking **/
		wdt_reset();
		_delay_ms(300);
		SET_PIN(LED_CTRL_PORT, FAV_LED);                
		_delay_ms(300);
		CLEAR_PIN(LED_CTRL_PORT, FAV_LED);                
		wdt_reset();
	}

	return 0;
}

void announce_Cload(unsigned char phase)
{
	unsigned char cur_val[5];
	int i=0, load_cur;

	//print_ssd_delay("LCUR",0,1000);
	if(phase == TWO_PHASE)
		load_cur = my_eeprom_read_int((unsigned int *)TWPH_EELCUR);
	else
		load_cur = my_eeprom_read_int((unsigned int *)EELCUR);

	convert_int_to_str(load_cur, cur_val);

	for(i=0; cur_val[i] != '\0'; i++)	
	{
		switch(cur_val[i]-'0')
		{
			case 0:
				audio_out(M_Zero);
				break;
			case 1:
				audio_out(M_One);
				break;
			case 2:
				audio_out(M_Two);
				break;
			case 3:
				audio_out(M_Three);
				break;
			case 4:
				audio_out(M_Four);
				break;
			case 5:
				audio_out(M_Five);
				break;
			case 6:
				audio_out(M_Six);
				break;
			case 7:
				audio_out(M_Seven);
				break;
			case 8:
				audio_out(M_Eight);
				break;
			case 9:
				audio_out(M_Nine);
				break;
			default:
				audio_out(M_Invalid);
				break;
		}
	}
}

//*06#
unsigned char init_water_check_delay(void)
{
	unsigned int delay;

	audio_out(M_MotorOnWaterCheck);
	dtmf_buf[0] = '0';
	dtmf_buf[1] = '0';
	dtmf_buf[2] = '0';
	dtmf_buf[3] = '0';

	//print_ssd_delay("TIME",0,PRINT_DELAY);
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	pos=0;
	while(pos < 4) {
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	if(timer_expired || pos > 4)
		return 1;
	dtmf_buf[4] = '\0';

	audio_out(M_ThankUKisanRaja);
	disconnect_call();
	delay = atoi((const char*)dtmf_buf);
	my_eeprom_write_int((unsigned int)delay ,(unsigned int *)EEDELAY);
	return 0;
}

void init_wdt(void)
{
	WDT_ENABLE;
}

#if 0
void calculating_motor_run_duration(void)
{                      
	unsigned char motor_running = 0;
	unsigned char date[7]={0},time[5]={0}, buff[5]={0}, org_ph_no[15] = {0};
	unsigned int daily_counter=0,days=0,weekly_counter=0,stop_min=0,hr=0,min=0;
	uint cur_day=0,prev_day=0, start_min=0;

	my_eeprom_read((void*)&motor_running, (const void*)EEPROM_MOTOR_RUNNING, 1);
	if((!motor_running) && (motor_status() == M_Started))
	{            
		Delay_sec(1);
		get_clock(time);
		Delay_sec(GSM_DELAY);
		start_min = ((time[0]-'0')*10 + (time[1]-'0'))* 60 + (time[2] - '0') * 10 + time[3] - '0';

		my_eeprom_write((const void*)&start_min, (void*)EEPROM_START_MIN, 2);
		motor_running = 1;
		my_eeprom_write((const void*)&motor_running,(void*) EEPROM_MOTOR_RUNNING, 1);                
	}
	if(motor_running && (motor_status() == M_Started))
	{
		motor_running = 0;
		Delay_sec(1);
		get_clock(time);
		Delay_sec(GSM_DELAY);
		time[4]='\0';
		if(0 == strcmp((const char*)time, "2359"))
		{
			stop_min = ((time[0]-'0')*10 + (time[1]-'0'))* 60 + (time[2] - '0') * 10 + time[3] - '0';

			my_eeprom_write((const void*)&motor_running,(void*) EEPROM_MOTOR_RUNNING, 1);                
			my_eeprom_read((void*)&start_min, (const void*)EEPROM_START_MIN, 2);
			my_eeprom_read((void*)&daily_counter, (const void*)EEPROM_DAILY_COUNTER, 2);

			daily_counter += (stop_min - start_min);
			my_eeprom_write((const void *)&(daily_counter),(void *)EEPROM_DAILY_COUNTER, 2);
			Delay_sec(60);
		}
	}
	else if(motor_running && (motor_status() == M_Stopped))
	{               
		motor_running = 0;
		Delay_sec(1);
		get_clock(time);
		Delay_sec(GSM_DELAY);
		stop_min = ((time[0]-'0')*10 + (time[1]-'0'))* 60 + (time[2] - '0') * 10 + time[3] - '0';

		my_eeprom_write((const void*)&motor_running,(void *) EEPROM_MOTOR_RUNNING, 1);                
		my_eeprom_read((void *)&start_min, (const void*)EEPROM_START_MIN, 2);
		my_eeprom_read((void *)&daily_counter, (const void*)EEPROM_DAILY_COUNTER, 2);

		daily_counter += (stop_min - start_min);
		my_eeprom_write((const void *)&(daily_counter),(void *)EEPROM_DAILY_COUNTER, 2);

	}              
	my_eeprom_read((void *)&days, (const void *)EEPROM_DAYS, sizeof(int));

	my_eeprom_read((void*)&prev_day, (const void*)EEPROM_PREV_DAY, 2);

	Delay_sec(GSM_DELAY);
	if((get_date((unsigned char *) date)))
	{
		return;
	}
	date[6] = '\0';
	cur_day = atoi((const char*)date+4);

	if(cur_day == prev_day)
	{
		return;
	}
	else
	{	
		my_eeprom_read((void *)&daily_counter, (const void *) EEPROM_DAILY_COUNTER, 2);	
		my_eeprom_read((void *)&weekly_counter, (const void *) EEPROM_WEEKLY_COUNTER, 2);

		weekly_counter += daily_counter;
		my_eeprom_write((const void *)&(weekly_counter),(void *)EEPROM_WEEKLY_COUNTER,2);
		daily_counter = 0;
		my_eeprom_write((const void *)&(daily_counter),(void *)EEPROM_DAILY_COUNTER,2);
		prev_day = cur_day;
		my_eeprom_write((const void *)&prev_day,(void *) EEPROM_PREV_DAY, 2);

		days++;
		my_eeprom_write((const void *)&days,(void *) EEPROM_DAYS, 2);
	}

	if(days >=7)
	{
		hr = weekly_counter/60;
		min = weekly_counter%60;

		itoa(hr,(char*) buff, 10);
		strcpy((char*)msg_info,(const char*)buff);
		strcat((char*)msg_info," HOURS ");

		itoa(min, (char*)buff, 10);
		strcat((char*)msg_info,(const char*)buff);
		strcat((char*)msg_info," MINUTES");

		my_eeprom_read((void *)org_ph_no,(const void *)EEPROM_ORG_PHONE_NUM, NDIGIT_PH_NUM);
		Delay_sec(LONG_GSM_DELAY);
		send_sms(org_ph_no, msg_info);
		Delay_sec(LONG_GSM_DELAY);
		recv_response(100);

		weekly_counter = 0;
		days = 0;
		my_eeprom_write((const void *)&(days),(void *)EEPROM_DAYS,2);
		my_eeprom_write((const void *)&(weekly_counter),(void *)EEPROM_WEEKLY_COUNTER,2);

	}
	return;
}

#endif
#if 1
void send_debugging_msg(void)
{
	wdt_reset();
	debugging_flag = 0;
	my_eeprom_write_byte((unsigned char)debugging_flag,(unsigned char *)CONFIGURE_DEBUGGING_FLAG);

	if(kr_call==1)
	{
		kr_call=0;
		wdt_reset();
		strcat((char *)msg_info, "\nKR CAL ");
	}
	else
	{
		wdt_reset();
		if((motor_status() == M_Started))
			strcpy((char *)msg_info, "M-ON ");
		else if((motor_status() == M_Stopped))
			strcpy((char *)msg_info, "M-OF ");

		//strcat((char *)msg_info, "\nMode ");
		if(hw.mode == M_Manual)
			strcat((char *)msg_info, "MANL");
		else if(hw.mode == M_Auto)
			strcat((char *)msg_info, "AUTO");
		else
			strcat((char *)msg_info, "TMR");

		//strcat((char *)msg_info, "\nFAR CAL");
	}

	Delay_sec(GSM_DELAY);
	send_sms((unsigned char *)debug_ph, msg_info);
	Delay_sec(GSM_DELAY);

	return;
}

#endif

void periodic_reboot_update(void)
{
	mode_change=0;
	if((debugging_enable==1) && (per_reboot == 0)) // if it is 8 hour reboot then dont send message
	{
		strcpy((char*)msg_info,"KR Rebot");
		Delay_sec(GSM_DELAY);
		send_sms((unsigned char *)debug_ph, msg_info);
		Delay_sec(GSM_DELAY);
	}


	/* to take care the periodic reboot of the controller and GSM module */	
	if(per_reboot == 1) // to avoid calls when controller reboots every 1 hour.
	{
#if PER_REBOOT_DEBUG
		strcpy((char*)msg_info,"PER_REBOOT");
		Delay_sec(GSM_DELAY);
		send_sms((unsigned char *)PER_REB_TEST_PH_NO, msg_info);
		Delay_sec(GSM_DELAY);
#endif
		per_reboot=0;
		my_eeprom_write_byte((unsigned char)per_reboot,(unsigned char *)EE_PER_REBOOT);
		hw.cond = check_favourable();
		hw.motor = motor_status();
		prv_status.cond = hw.cond;
		prv_status.motor = hw.motor;
		hw.err=0;
		mode_change=hw.mode ;
	}
	send_serial0('\n');
	send_serial0('\r');
}


void firm_upgrade_info(void)
{
	unsigned char buff[15];
	unsigned char firmware_upgrade = 0,firmware_upgrade_2=0,firm_error=0;

	firmware_upgrade = my_eeprom_read_byte((unsigned char *)FIRMWARE_UPGRADE);
	firmware_upgrade_2 = my_eeprom_read_byte((unsigned char *)FIRMWARE_UPGRADE_2);

	if((!firmware_upgrade) && (!firmware_upgrade_2))
	{
		firm_error = my_eeprom_read_byte((unsigned char *)EEBOOT_FIRM_ERR);
		my_eeprom_read((void *)b_ph,(const void *)EEB_PH_NUM, NDIGIT_PH_NUM); 	
		/* Read admin phone number from EEPROM */
		boot_ver = my_eeprom_read_int((unsigned int *)EEBOOT_VERSION);
		b_ph[10] = '\0';
		if(firm_error == 1)
		{
			firm_error = 0;
			my_eeprom_write_byte((unsigned char)firm_error, (unsigned char*)EEBOOT_FIRM_ERR);
			//print_ssd_delay("FIRM_FAIL",0,500);
			strcpy((char*)msg_info, "FIRM FAIL");
			strcpy((char*)ph,(const char*) b_ph);
			if(notify() != 0)
			{
				audio_out(M_Welcome);
				audio_out(M_Disabled);
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
			}			
		}
		else
		{	
			strcpy((char*)msg_info, "FIRM SUCES");
			strcpy((char*)ph,(const char*) b_ph);
			if(notify() != 0)
			{
				audio_out(M_Welcome);
				audio_out(M_Enabled);
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
			}			
		}
		strcat((char *)msg_info, "\nAPPS VER ");
		strcat((char*)msg_info,VERSION);

		strcat((char *)msg_info, "\nBOOT VER ");
		itoa(boot_ver, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);

		Delay_sec(GSM_DELAY);
		send_sms(ph[0],msg_info); 
		Delay_sec(GSM_DELAY);

		my_eeprom_read((void *)ph[0],(const void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);     
		/* Read Farmer phone number from EEPROM */
		ph[0][10] = '\0';
		firmware_upgrade = 1;
		my_eeprom_write_byte((unsigned char)firmware_upgrade, (unsigned char*)FIRMWARE_UPGRADE);
		firmware_upgrade_2 = 1;
		my_eeprom_write_byte((unsigned char)firmware_upgrade_2, (unsigned char*)FIRMWARE_UPGRADE_2);
	}
}

void check_var_values(void)
{
	/**********
	 * NOTE : UNCOMMENT WATER FLOW SENSOR ERROR CHECKING IF REQUIRED 
	 * ********/
	unsigned char c=0;
	unsigned int i=0;
	unsigned char firmware_upgrade = 0,firmware_upgrade_2=0;


	/** TO HANDLE CURRENTS **/
	if((r_phase_current < MIN_DEF_CUR) || (r_phase_current > MAX_DEF_CUR))
	{
		r_phase_current = my_eeprom_read_int((unsigned int *)R_PHASE_CURRENT);
		if((r_phase_current < MIN_DEF_CUR) || (r_phase_current > MAX_DEF_CUR))
		{
			r_phase_current = DEFAULT_CUR_VALUES;
			my_eeprom_write_int((unsigned int) r_phase_current, (unsigned int *)R_PHASE_CURRENT);
		}
	}
	if((y_phase_current < MIN_DEF_CUR) || (y_phase_current > MAX_DEF_CUR))
	{
		y_phase_current = my_eeprom_read_int((unsigned int *)Y_PHASE_CURRENT);
		if((y_phase_current < MIN_DEF_CUR) || (y_phase_current > MAX_DEF_CUR))
		{
			y_phase_current = DEFAULT_CUR_VALUES;
			my_eeprom_write_int((unsigned int) y_phase_current, (unsigned int *)Y_PHASE_CURRENT);
		}
	}
	if((b_phase_current < MIN_DEF_CUR) || (b_phase_current > MAX_DEF_CUR))
	{
		b_phase_current = my_eeprom_read_int((unsigned int *)B_PHASE_CURRENT);
		if((b_phase_current < MIN_DEF_CUR) || (b_phase_current > MAX_DEF_CUR))
		{
			b_phase_current = DEFAULT_CUR_VALUES;
			my_eeprom_write_int((unsigned int) b_phase_current, (unsigned int *)B_PHASE_CURRENT);
		}
	}
	if((cur_range < MIN_DEF_CUR) || (cur_range > MAX_DEF_CUR))
	{
		cur_range = my_eeprom_read_int((unsigned int *)EECRNGE);
		if((cur_range < MIN_DEF_CUR) || (cur_range > MAX_DEF_CUR))
		{
			cur_range=DEFAULT_CUR_VALUES;
			my_eeprom_write_int((unsigned int) cur_range, (unsigned int *)EECRNGE);
		}
	}
	/** TO HANDLE DELAY'S **/
	if((auto_mode_delay < MIN_DEF_DELAY) || (auto_mode_delay > MAX_DEF_DELAY))
	{
		auto_mode_delay = my_eeprom_read_int((unsigned int *)AUTO_MODE_DELAY);
		if((auto_mode_delay < MIN_DEF_DELAY) || (auto_mode_delay > MAX_DEF_DELAY))
		{
			auto_mode_delay = DEFAULT_DELAY;
			my_eeprom_write_int((unsigned int) auto_mode_delay, (unsigned int *)AUTO_MODE_DELAY);	
		}
	}
	if((start_motor_delay < MIN_DEF_DELAY) || (start_motor_delay > 20))
	{
		start_motor_delay = my_eeprom_read_int((unsigned int *)START_MOTOR_DELAY);
		if((start_motor_delay < MIN_DEF_DELAY) || (start_motor_delay > 20))
		{
			start_motor_delay = DEFAULT_DELAY;
			my_eeprom_write_int((unsigned int)start_motor_delay, (unsigned int *)START_MOTOR_DELAY);
		}
	}
	/*
	if((higher_hp_delay < MIN_DEF_DELAY) || (higher_hp_delay > MAX_DEF_DELAY))
	{
		higher_hp_delay = my_eeprom_read_int((unsigned int *)HIGHER_HP_DELAY);
		if((higher_hp_delay < MIN_DEF_DELAY) || (higher_hp_delay > MAX_DEF_DELAY))
		{
			higher_hp_delay= DEFAULT_HP_DELAY; //2; 	
			//default delay for motor start in start motor for higher hp 
			my_eeprom_write_int((unsigned int)higher_hp_delay, (unsigned int *)HIGHER_HP_DELAY);
		}
	}
	*/

	/** TO HANDLE SIGNAL ANND REBOOT VALUES **/
/*	if((signal_val < MIN_DEF_VAL) || (signal_val > MAX_DEF_VAL))
	{
		signal_val = my_eeprom_read_int((unsigned int *)EE_SIGNAL_VAL);
		if((signal_val < MIN_DEF_VAL) || (signal_val > MAX_DEF_VAL))
		{
			signal_val=DEFAULT_SIG_VAL; //3; // default signal value 3. 
			my_eeprom_write_int((unsigned int)signal_val, (unsigned int *)EE_SIGNAL_VAL);
		}
	}*/
	if((reboot_counter < MIN_DEF_VAL) || (reboot_counter > MAX_DEF_VAL))
	{
		reboot_counter = my_eeprom_read_int((unsigned int *)EE_REBOOT_CNTR);
		if((reboot_counter < MIN_DEF_VAL) || (reboot_counter > MAX_DEF_VAL))
		{
			reboot_counter = DEFAULT_ZERO; //0; 	/** default keep default reboot counter value to zero **/
			my_eeprom_write_int((unsigned int) reboot_counter, (unsigned int *)EE_REBOOT_CNTR);
		}
	}

	if((lower_volt_range < MIN_RANGE) || (lower_volt_range > MAX_RANGE))
	{
		lower_volt_range = my_eeprom_read_int((unsigned int *)LOWER_VOLT_RANGE);
		if((lower_volt_range < MIN_RANGE) || (lower_volt_range > MAX_RANGE))
		{
			lower_volt_range= DEFAULT_LOWR_VTG; //200; 
			my_eeprom_write_int((unsigned int)lower_volt_range, (unsigned int *)LOWER_VOLT_RANGE);
		}
	}
	if((higher_volt_range < MIN_RANGE) || (higher_volt_range > MAX_HIGH_V_RNG))
	{
		higher_volt_range = my_eeprom_read_int((unsigned int *)HIGHER_VOLT_RANGE);
		if((higher_volt_range < MIN_RANGE) || (higher_volt_range > MAX_HIGH_V_RNG))
		{
			higher_volt_range= DEFAULT_HIGHR_VTG;  
			my_eeprom_write_int((unsigned int)higher_volt_range, (unsigned int *)HIGHER_VOLT_RANGE);
		} 
	}
	if((dryrun.water_wait < MIN_DRY_DELAY) || (dryrun.water_wait > MAX_DRY_DELAY))
	{
		dryrun.water_wait = my_eeprom_read_int((unsigned int *)EEDELAY);
		if((dryrun.water_wait < MIN_DRY_DELAY) || (dryrun.water_wait > MAX_DRY_DELAY))
		{
			dryrun.water_wait = DEFAULT_DELAY; //5;
			my_eeprom_write_int((unsigned int)dryrun.water_wait, (unsigned int *)EEDELAY);
		}
	}
#if 0
	if((percentage_current_raise  < MIN_PERCEN_RAISE ) || (percentage_current_raise > MAX_PERCEN_RAISE))
	{
		percentage_current_raise = my_eeprom_read_int((unsigned int *)PERCENTAGE_CURRENT_RAISE);
		if((percentage_current_raise  < MIN_PERCEN_RAISE ) || (percentage_current_raise > MAX_PERCEN_RAISE))
		{
			percentage_current_raise=DEFAULT_PER_RAISE; //35; 
			my_eeprom_write_int((unsigned int)percentage_current_raise, (unsigned int *)PERCENTAGE_CURRENT_RAISE);
		}
	}
	if((over_current_limit < MIN_RANGE) || (over_current_limit > MAX_OVR_CUR_RANGE))
	{
		over_current_limit = my_eeprom_read_int((unsigned int *)OVER_CURRENT_LIMIT);
		if((over_current_limit < MIN_RANGE) || (over_current_limit > MAX_OVR_CUR_RANGE))
		{
			over_current_limit=DEFAULT_OVR_CUR; //300;// changed from 140 to 300 
			my_eeprom_write_int((unsigned int)over_current_limit, (unsigned int *)OVER_CURRENT_LIMIT);
		}
	}
#endif
	if((run_in_which_phase < MIN_RUN_PHS) || (run_in_which_phase > MAX_RUN_PHS))
	{
		run_in_which_phase = my_eeprom_read_byte((unsigned char *)EE_PHASE);
		if((run_in_which_phase < MIN_RUN_PHS) || (run_in_which_phase > MAX_RUN_PHS))
		{
			run_in_which_phase=THREE_PHASE_ANDHRA;
			my_eeprom_write_byte((unsigned char)run_in_which_phase, (unsigned char  *)EE_PHASE);
		}
	}
	if((g_sub_phase < MIN_SUB_PHS) || (g_sub_phase >MAX_SUB_PHS))
	{
		g_sub_phase = my_eeprom_read_byte((unsigned char *)SUB_PHASE);
		if((g_sub_phase < MIN_SUB_PHS) || (g_sub_phase >MAX_SUB_PHS))
		{
			g_sub_phase= DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char)g_sub_phase, (unsigned char  *)SUB_PHASE);
		}

	}
	if((notify_sms_enable < DEFAULT_ZERO) || (notify_sms_enable > DEFAULT_ENABLE))
	{
		notify_sms_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_NOTIFY_SMS);
		if((notify_sms_enable < DEFAULT_ZERO) || (notify_sms_enable > DEFAULT_ENABLE))
		{
			notify_sms_enable = DEFAULT_ENABLE; //1;
			my_eeprom_write_byte((unsigned char) notify_sms_enable,(unsigned char  *)CONFIGURE_NOTIFY_SMS);
		}
	}
	if((admin_activation < DEFAULT_ZERO) || (admin_activation > DEFAULT_ENABLE))
	{
		admin_activation = my_eeprom_read_byte((unsigned char *)ADMIN_ACTIVATION); 
		// read the admin activation details
		if((admin_activation < DEFAULT_ZERO) || (admin_activation > DEFAULT_ENABLE))
		{
			admin_activation=DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char) admin_activation, (unsigned char  *)ADMIN_ACTIVATION); 
			// deactivate the device
		}
	}
	if((sms_ver < DEFAULT_ZERO) || (sms_ver > DEFAULT_ENABLE))
	{
		sms_ver = my_eeprom_read_byte((unsigned char *)CONFIGURE_SMS_VER);
		if((sms_ver < DEFAULT_ZERO) || (sms_ver > DEFAULT_ENABLE))
		{
			sms_ver =  DEFAULT_ENABLE; //1;
			my_eeprom_write_byte((unsigned char) sms_ver,(unsigned char  *)CONFIGURE_SMS_VER);
		}
	}
	if((theft_detected< DEFAULT_ZERO) || (theft_detected > DEFAULT_ENABLE))
	{
		theft_detected = my_eeprom_read_byte((unsigned char *)EETHFT_OPR);
		if((theft_detected< DEFAULT_ZERO) || (theft_detected > DEFAULT_ENABLE))
		{
			theft_detected = DEFAULT_ZERO; //0;
			my_eeprom_write_byte((unsigned char)theft_detected, (unsigned char  *)EETHFT_OPR); 
			/** initially make theft detected as zero **/	
		}
	}

	if((dryrun.enable  < DEFAULT_ZERO) || (dryrun.enable > DEFAULT_ENABLE))
	{
		dryrun.enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_DRYRUN_PROTECTION);
		if((dryrun.enable  < DEFAULT_ZERO) || (dryrun.enable > DEFAULT_ENABLE))
		{
			dryrun.enable = DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char)dryrun.enable, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       /* dry-run protection feature will be disabled by default */
		}
	}

	if((nonpeaktime_alerts_disable  < DEFAULT_ZERO) || (nonpeaktime_alerts_disable  > DEFAULT_ENABLE))
	{
		nonpeaktime_alerts_disable = my_eeprom_read_byte((unsigned char *)CONFIGURE_NONPEAKTIME_ALERTS);
		if((nonpeaktime_alerts_disable  < DEFAULT_ZERO) || (nonpeaktime_alerts_disable  > DEFAULT_ENABLE))
		{
			nonpeaktime_alerts_disable = DEFAULT_ZERO;
			my_eeprom_write_byte((unsigned char) nonpeaktime_alerts_disable\
					, (unsigned char  *)CONFIGURE_NONPEAKTIME_ALERTS);      /* non-peak time alerts feature will be disabled by default */

		}
	}

	if((byps < DEFAULT_ZERO) || (byps > DEFAULT_ENABLE))
	{
		byps = my_eeprom_read_byte((unsigned char *)EEBYPASS);
		if((byps < DEFAULT_ZERO) || (byps > DEFAULT_ENABLE))
		{
			byps = DEFAULT_ZERO;
			my_eeprom_write_byte((unsigned char) i, (unsigned char  *)EEBYPASS);
		}
	}

	if((debugging_enable < DEFAULT_ZERO) || (debugging_enable > DEFAULT_ENABLE))
	{
		debugging_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_DEBUGGING_FEATURE);
		if((debugging_enable < DEFAULT_ZERO) || (debugging_enable > DEFAULT_ENABLE))
		{
			debugging_enable = DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char) debugging_enable\
					,(unsigned char  *)CONFIGURE_DEBUGGING_FEATURE);
		}
	}
	if((debugging_flag < DEFAULT_ZERO) || (debugging_flag > DEFAULT_ENABLE))
	{
		debugging_flag = my_eeprom_read_byte((unsigned char *)CONFIGURE_DEBUGGING_FLAG);
		if((debugging_flag < DEFAULT_ZERO) || (debugging_flag > DEFAULT_ENABLE))
		{
			debugging_flag = DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char) debugging_flag,(unsigned char  *)CONFIGURE_DEBUGGING_FLAG);
		}
	}
	if((call_motor_off < DEFAULT_ZERO) || (call_motor_off > DEFAULT_ENABLE))
	{
		call_motor_off = my_eeprom_read_byte((unsigned char *)EE_CALL_MOTOR_OFF); // call motor off
		if((call_motor_off < DEFAULT_ZERO) || (call_motor_off > DEFAULT_ENABLE))
		{
			call_motor_off = DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char)call_motor_off,(unsigned char  *)EE_CALL_MOTOR_OFF);
			// switch off the motor from call 0.

		}
	}
	if((per_reboot < DEFAULT_ZERO) || (per_reboot > DEFAULT_ENABLE))
	{
		per_reboot = my_eeprom_read_byte((unsigned char *)EE_PER_REBOOT); // read the data of 8 hour reboot
		if((per_reboot < DEFAULT_ZERO) || (per_reboot > DEFAULT_ENABLE))
		{

			per_reboot=DEFAULT_ZERO; // 0;
			my_eeprom_write_byte((unsigned char)per_reboot,(unsigned char  *)EE_PER_REBOOT);
			// periodic reboot make it 0.
		}
	}
	if((theft_enable < DEFAULT_ZERO) || (theft_enable > DEFAULT_ENABLE))
	{
		theft_enable = my_eeprom_read_byte((unsigned char *)ETHEFT);
		if((theft_enable < DEFAULT_ZERO) || (theft_enable > DEFAULT_ENABLE))
		{

			theft_enable = DEFAULT_ENABLE; //1;
			my_eeprom_write_byte((unsigned char) theft_enable, (unsigned char  *)ETHEFT);	
			/**  enable/disable theft feature **/
		}

	}
	/*if((water_flow_sensor < DEFAULT_ZERO) || (water_flow_sensor > DEFAULT_ENABLE))
	  {
	  water_flow_sensor = my_eeprom_read_byte((unsigned char *)WATER_FLOW_SENSOR);
	  if((water_flow_sensor < DEFAULT_ZERO) || (water_flow_sensor > DEFAULT_ENABLE))
	  {
	  water_flow_sensor = DEFAULT_ZERO; // 0;
	  my_eeprom_write_byte((unsigned char) water_flow_sensor, (unsigned char  *)WATER_FLOW_SENSOR);
	  }
	  }*/
	if((wire_cut_v_level < MIN_RANGE) || (wire_cut_v_level > MAX_RANGE))
	{
		wire_cut_v_level =  my_eeprom_read_int((unsigned int *)WIRECUT_LVL); 
		// read the vale of wirecut voltage level.
		if((wire_cut_v_level < MIN_RANGE) || (wire_cut_v_level > MAX_RANGE))
		{
			wire_cut_v_level = DEFAULT_WIRE_V_VAL; 	/** default delay for motor start in auto mode **/
			my_eeprom_write_int((unsigned int) wire_cut_v_level, (unsigned int *)WIRECUT_LVL);	
		}
	}
	if((enable_auto_monitor < DEFAULT_ZERO) || (enable_auto_monitor > DEFAULT_ENABLE))
	{
		enable_auto_monitor = my_eeprom_read_byte((unsigned char *)CONFIGURE_AUTO_MONITOR);
		if((enable_auto_monitor < DEFAULT_ZERO) || (enable_auto_monitor > DEFAULT_ENABLE))
		{
			enable_auto_monitor = DEFAULT_ZERO;
			my_eeprom_write_byte((unsigned char) enable_auto_monitor, (unsigned char  *)CONFIGURE_AUTO_MONITOR);
		}
	}

	if((single_phs_enable < DEFAULT_ZERO) || (single_phs_enable > DEFAULT_ENABLE))
	{
		single_phs_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_SINGLE_PHS);
		if((single_phs_enable < DEFAULT_ZERO) || (single_phs_enable > DEFAULT_ENABLE))
		{

			single_phs_enable =  DEFAULT_ZERO; //1;
			my_eeprom_write_byte((unsigned char)single_phs_enable,(unsigned char  *)CONFIGURE_SINGLE_PHS);
		}
	}
	if((timer3_hours < DEFAULT_ZERO) || (timer3_hours > MAX_TIMER_3_DEL))
	{
		timer3_hours = my_eeprom_read_byte((unsigned char *)START_TIMR3_DELAY);
		if((timer3_hours < DEFAULT_ZERO) || (timer3_hours > MAX_TIMER_3_DEL))
		{
			timer3_hours = DEFAULT_TMR3; //6; 	/** default delay for start timer3 is 6 hours**/
			my_eeprom_write_int((unsigned int) timer3_hours, (unsigned int *)START_TIMR3_DELAY);
		}
	}
	if(!((hw.mode == M_Manual) || (hw.mode == M_Auto) || (hw.mode == M_Timer)))
	{
		hw.mode = my_eeprom_read_int((unsigned int*)EEPROM_START_MODE);            
		/*Read the KISANRAJA mode from EEPROM */

		if(!((hw.mode == M_Manual) || (hw.mode == M_Auto) || (hw.mode == M_Timer)))
		{
			hw.mode = M_Manual;
			my_eeprom_write_int((unsigned int) (hw.mode), (unsigned int*)EEPROM_START_MODE);
		}
	}
	if((comm_level_flag < DEFAULT_ZERO) || (comm_level_flag > DEFAULT_ENABLE) \
			|| (msg_flag < DEFAULT_ZERO) || (msg_flag > DEFAULT_ENABLE))
	{
		c = my_eeprom_read_byte((unsigned char *)ECMLVL);		

		if(c==0) 	/* if communication flag is 0, normal mode */
		{
			msg_flag=0;
			comm_level_flag=0;
		}
		else if(c == 1)             /*if communication flag is 1, RESTRICTED mode*/
		{
			msg_flag=0;
			comm_level_flag=1;
		}
		else		             /*if communication flag is 2, MESSAGE only mode*/
		{
			msg_flag=1;
			comm_level_flag=0;

		}
	}

	if((firmware_upgrade < DEFAULT_ZERO) || (firmware_upgrade > DEFAULT_ENABLE) \
			|| (firmware_upgrade_2 < DEFAULT_ZERO) || (firmware_upgrade_2 > DEFAULT_ENABLE))
	{
		firmware_upgrade = my_eeprom_read_byte((unsigned char *)FIRMWARE_UPGRADE);
		firmware_upgrade_2 = my_eeprom_read_byte((unsigned char *)FIRMWARE_UPGRADE_2);
		if((firmware_upgrade < DEFAULT_ZERO) || (firmware_upgrade > DEFAULT_ENABLE) \
				|| (firmware_upgrade_2 < DEFAULT_ZERO) || (firmware_upgrade_2 > DEFAULT_ENABLE))
		{
			firmware_upgrade =  DEFAULT_ENABLE; //1;
			firmware_upgrade_2 =  DEFAULT_ENABLE; //1;
			my_eeprom_write_byte((unsigned char)firmware_upgrade, (unsigned char *)FIRMWARE_UPGRADE);
			my_eeprom_write_byte((unsigned char)firmware_upgrade_2, (unsigned char *)FIRMWARE_UPGRADE_2);
		}
	}

	if((hyster_value < MIN_HYS_RANGE) || (hyster_value > MAX_HYS_RANGE))
	{
		hyster_value = my_eeprom_read_int((unsigned int *)HYSTER_VALUE);
		if((hyster_value < MIN_HYS_RANGE) || (hyster_value > MAX_HYS_RANGE))
		{
			hyster_value= DEF_HYSTER_VALUE; 
			my_eeprom_write_int((unsigned int)hyster_value, (unsigned int *)HYSTER_VALUE);
		}
	}
	if((all_3_phases_loaded < DEFAULT_ZERO) || (all_3_phases_loaded > DEFAULT_ENABLE))
	{
		all_3_phases_loaded = my_eeprom_read_byte((unsigned char *)ALL_3_PHASES_LOADED);
		if((all_3_phases_loaded < DEFAULT_ZERO) || (all_3_phases_loaded > DEFAULT_ENABLE))
		{
			all_3_phases_loaded =  DEFAULT_ZERO; //1;
			my_eeprom_write_byte((unsigned char)all_3_phases_loaded,(unsigned char  *)ALL_3_PHASES_LOADED);
		}
	}

#if AUTOMATE
	if((automation_code < DEFAULT_ZERO) || (automation_code > DEFAULT_ENABLE))
	{
		automation_code = my_eeprom_read_byte((unsigned char *)AUTOMATION_CODE);
		if((automation_code < DEFAULT_ZERO) || (automation_code > DEFAULT_ENABLE))
		{
			automation_code = DEFAULT_ZERO;
			my_eeprom_write_byte((unsigned char)automation_code, (unsigned char *)AUTOMATION_CODE);
		}
	}
	if(!((password_set==0) || (password_set==1)))
	{
		password_set= 0;
	}
	if((automation_done < DEFAULT_ZERO) || (automation_done > DEFAULT_ENABLE))
	{
		automation_done = my_eeprom_read_byte((unsigned char *)AUTOMATION_DONE);
		if((automation_done < DEFAULT_ZERO) || (automation_done > DEFAULT_ENABLE))
		{

			automation_done = DEFAULT_ZERO;
			my_eeprom_write_byte((unsigned char)automation_done, (unsigned char *)AUTOMATION_DONE);
		}
	}
#endif
}

#if AUTOMATE
void display_device_vals(void)
{
	unsigned char buff[15];
	float temp_float_val=0;
	wdt_reset();
	print_ssd_delay("\n\rSMPS",0,500);
	temp_float_val = ((float)adc_conv(SMPS_OUTPUT_PIN) / MAX_ADC_OUTPUT) * ADC_VREF * SMPS_ADC_MULTIPLY_FACTOR;
	itoa((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT),(char*) buff, 10);

	if((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT) < 100)
	{
		buff[4] = '\0';
		buff[3] = buff[2];
		buff[2] = buff[0];
		buff[0] = '0';
	}
	else
	{
		buff[4] = '\0';
		buff[3] = buff[2];
		buff[2] = buff[1];
	}
	buff[1] = '.';
	print_ssd_delay((char*)buff,0,PRINT_DELAY);
	send_serial0(' ');
	send_serial0(' ');

	print_ssd_delay("\n\rPH-R",0,500);
	print_ssd_delay(NULL,adc_conv(VOLTAGE_PHASE_R),PRINT_DELAY);

	print_ssd_delay("\n\rPH-Y",0,500);
	print_ssd_delay(NULL,adc_conv(VOLTAGE_PHASE_Y),PRINT_DELAY);

	print_ssd_delay("\n\rPH-B",0,500);
	print_ssd_delay(NULL,adc_conv(VOLTAGE_PHASE_B),PRINT_DELAY);

	print_ssd_delay("\n\rC1",0,500);
	print_ssd_delay(NULL,adc_conv(CURRENT_PHASE_R),PRINT_DELAY);

	print_ssd_delay("\n\rC2",0,500);
	print_ssd_delay(NULL,adc_conv(CURRENT_PHASE_Y),PRINT_DELAY);

	print_ssd_delay("\n\rC3",0,500);
	print_ssd_delay(NULL,adc_conv(CURRENT_PHASE_B),PRINT_DELAY);
	send_serial0('\n');
	send_serial0('\r');
	wdt_reset();
}
#endif

void first_time_factory_reset(void)
{
	unsigned char fac_reset=0;
	/** Logic to do factory reset at the first time of device start up **/
	fac_reset = my_eeprom_read_byte((unsigned char *)EE_FAC_RESET);
	if(fac_reset == 0xFF)
	{
		remote_frst();
		fac_reset=1;
		my_eeprom_write_byte((unsigned char)fac_reset,(unsigned char *)EE_FAC_RESET);
#if AUTOMATE
		automation_done =0;
		my_eeprom_write_byte((unsigned char)automation_done, (unsigned char *)AUTOMATION_DONE);
		print_ssd_delay("\n\rFRST_OK\n\r",0,500);
#endif
		print_ssd_delay("FRST",0,500);
		init_wdt();
		_delay_ms(5000); 
	}
}

#if AUTOMATE
char check_automated_favr(void)
{
	unsigned int smps_val =0;
	wdt_reset();
	if((automation_code ==1) && (password_set==1))
	{
		hw.cond = check_favourable();

		if((hw.cond == C_Favourable) && (motor_status() == M_Started))
			check_ct_values();
		else if(hw.cond == C_UnFavourable)
		{
			smps_val = adc_conv(SMPS_OUTPUT_PIN);
			if((hw.err == E_NoPower) && (smps_val > 400))
			{
				display_device_vals();
				while(1)
				{
					while_delay();
					print_ssd_delay("\rRES-CON-PROB",0,500);
				}
			}
			else if((hw.err == E_NoPower) && (smps_val < 400))
			{
				return 1;
			}
			else if((hw.err == E_WireCut) && (smps_val < 400))
			{
				display_device_vals();
				while(1)
				{
					while_delay();
					print_ssd_delay("\r5V-PROB",0,500);
				}	
			}
			else if((hw.err == E_ManualStop))
			{
				display_device_vals();
				while(1)
				{
					while_delay();
					print_ssd_delay("\rCT-CON-PROB",0,500);
				}	
			}	
			else if(hw.err == E_WireCut)
			{
				if((adc_conv(VOLTAGE_PHASE_Y) < MIN_PHASE_VOLTAGE))
				{
					display_device_vals();
					while(1)
					{
						while_delay();
						print_ssd_delay("\rY-PHS-CUT",0,500);
					}
				}
				else
				{
					display_device_vals();
					while(1)
					{
						while_delay();
						print_ssd_delay("\rR/B-CUT",0,500);
					}
				}
			}
		}
		else
		{
			smps_val = adc_conv(SMPS_OUTPUT_PIN);
			if(smps_val < 400)
			{
				display_device_vals();
				while(1)
				{
					while_delay();
					print_ssd_delay("\rSMPS_LOW",0,500);
				}
			}
		}
	}
	wdt_reset();
	return 0;
}
#endif

#if AUTOMATE
void check_ct_values(void)
{
	wdt_reset();
	r_phase_current = adc_conv(CURRENT_PHASE_R);
	y_phase_current = adc_conv(CURRENT_PHASE_Y);
	b_phase_current = adc_conv(CURRENT_PHASE_B);

	if(((r_phase_current == 0) || (r_phase_current > max_ct_val)))
	{
		display_device_vals();
		stop_motor_ct_err();
		while(1)
		{
			while_delay();
			print_ssd_delay("\rC1-ERR",0,500);
		}
	}
	else if(((y_phase_current == 0) || (y_phase_current > max_ct_val)))
	{
		display_device_vals();
		stop_motor_ct_err();
		while(1)
		{
			while_delay();
			print_ssd_delay("\rC2-ERR",0,500);
		}
	}
	else if(((b_phase_current ==0) || (b_phase_current > max_ct_val)))
	{
		display_device_vals();
		stop_motor_ct_err();
		while(1)
		{
			while_delay();
			print_ssd_delay("\rC3-ERR",0,500);
		}
	}
	wdt_reset();
}
#endif 

#if AUTOMATE
void end_automation(void)
{
	unsigned char admin_audio_num[]=ADMIN_AUDIO_NUMBR;
	unsigned char buf[15];
	float temp_float_val=0;

	print_ssd_delay("\n\rPREV_BAT_VTG  ",0,500);
	print_ssd_delay((char*)batry1,0,500);

	print_ssd_delay("\n\rPRESENT_BAT_VTG",0,500);
	temp_float_val = ((float)adc_conv(BATTERY_VOLTAGE_PIN) / MAX_ADC_OUTPUT) * ADC_VREF * BATTERY_ADC_MULTIPLY_FACTOR;
	convert_int_to_str((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT), buf);
	buf[4] = buf[3];
	buf[3] = buf[2];
	buf[2] = buf[1];
	buf[1] = '.';
	print_ssd_delay((char*)buf,0,500);
	send_serial0('\n');
	send_serial0('\r');

	strcpy((char*)ph,(const char*)admin_audio_num);	
	notify_sms_enable =0;
	if(notify() != 0)
	{
		audio_out(M_InvalidHrs);
		audio_out(M_ThankUKisanRaja);
		disconnect_call();
	}		
	else
	{
		while(1)
		{
			while_delay();
			print_ssd_delay("\rCAL_ERR",0,500);
		}
		return;
	}
	notify_sms_enable =1;
	remote_frst();
	print_ssd_delay("FRST OK",0,500);
	print_ssd_delay("\n\rAUTOMTION_OK\n\r",0,500);
	password_set=0;
	automation_done =0;
}
#endif 
