/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

unsigned char enable_auto_monitor;//enable_battery_monitor,
unsigned char nonpeak_start[5], nonpeak_end[5], nonpeaktime_alerts_disable;
unsigned char msg_flag=0,comm_level_flag=0;
unsigned char dtmf_wait_cnt, theft_enable;
unsigned char caller_number[NDIGIT_PH_NUM + NULL_CHARACTER];
unsigned char num_bufr[11]={0};
unsigned int ivrs_pos=M_Welcome, ivrs_buf[MAX_DTMF_TONES];
extern char cyclic_timer, twice_timer;
extern volatile unsigned char dtmf_buf[BUF_LEN];
extern volatile unsigned char ri;			                       /* Ring Indicator */
extern ivrs_info ivrs, st;			                           
extern char motor_on;     	/*response is the UART buffer;audio_done
				  indicates the completion of audio played by the Audio IC*/
char sms_admin = 0;


void init_state(void)
{
	ivrs.level = LInvalid;
	ivrs.state = S_Welcome;
	ri = 0;
	//	ivrs.incall = 0;
	st.level = L1;
}

/* This function updates the ivrs_buf array based on the dtmf received */

void on_dtmf_receipt(unsigned int *dtmf_opt, unsigned char max)
{
	unsigned char dtmf;

	dtmf_recvd = 0;
	dtmf = dtmf_buf[pos - 1] - '0';
	print_ssd_delay((char *)0,dtmf,500);

	/* Don't take any action if 0 is pressed */
	if(dtmf == 0)
		return;

	if(dtmf_buf[pos-1] == ASCII_STAR)   /* condition to handle '*' key press */
	{
		ivrs_pos = STAR;
	}
	else if(dtmf_buf[pos-1] == ASCII_HASH)   /* condition to handle '*' key press */
	{
		ivrs_pos = HASH;
	}
	else if(dtmf <= max)
	{
		ivrs_pos = dtmf_opt[dtmf - 1];
	}
	pos = 0;
}


unsigned char dtmf_wait(void)
{
	start_timer(KEY_PRESS_WAIT_TIME);
	while(!dtmf_recvd && !timer_expired)
	{
		if(!check_call_status())
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}
	stop_timer();
	if(timer_expired==1) 
	{
		audio_out(M_NoResponse);
		dtmf_wait_cnt++;
	}
	return 0;
}


void play_time(unsigned char time[])
{
	/*This function plays out the time entered by the farmer in the Timer mode */

	unsigned char i=0,t=0;
	switch(time[i])
	{
		case '0':
			if(time[i+1] == '0')
				audio_out(M_Zero);
			break;
		case '1':
			switch(time[i+1])
			{
				case '0':
					audio_out(M_Ten);
					break;

				case '1':
					//audio_out(M_Eleven);
					audio_out(M_One);
					audio_out(M_One);
					break;

				case '2':
					 audio_out(M_One);
					 audio_out(M_Two);
					 //audio_out(M_Twelve);
					 break;

				case '3':
					 audio_out(M_One);
					 audio_out(M_Three);
					 //audio_out(M_Thirteen);
					 break;

				case '4':
					 audio_out(M_One);
					 audio_out(M_Four);
					 //audio_out(M_Fourteen);
					 break;

				case '5':
					 audio_out(M_One);
					 audio_out(M_Five);
					 //audio_out(M_Fifteen);
					 break;

				case '6':
					 audio_out(M_One);
					 audio_out(M_Six);
					 //audio_out(M_Sixteen);
					 break;

				case '7':
					 audio_out(M_One);
					 audio_out(M_Seven);
					 //audio_out(M_Seventeen);
					 break;

				case '8':
					 audio_out(M_One);
					 audio_out(M_Eight);
					 //audio_out(M_Eighteen);
					 break;

				case '9':
					 audio_out(M_One);
					 audio_out(M_Nine);
					 //audio_out(M_Nineteen);
					 break;
			}
			t=1;
			break;

		case '2':
			audio_out(M_Twenty);
			break;

		case '3':
			audio_out(M_Thirty);
			break;

		case '4':
			audio_out(M_Forty);
			break;

		case '5':
			audio_out(M_Fifty);
			break;
	}

	if(!t)
	{
		switch(time[i+1])
		{

			case '1':
				audio_out(M_One);
				break;

			case '2':
				audio_out(M_Two);
				break;

			case '3':
				audio_out(M_Three);
				break;

			case '4':
				audio_out(M_Four);
				break;

			case '5':
				audio_out(M_Five);
				break;

			case '6':
				audio_out(M_Six);
				break;

			case '7':
				audio_out(M_Seven);
				break;

			case '8':
				audio_out(M_Eight);
				break;

			case '9':
				audio_out(M_Nine);
				break;
		}
	}
}

/* This function will read the time entered by the farmer in Timer mode, validates it and converts it into minutes. */
	
unsigned int get_time(void)
{
	unsigned char chance=3, n=0;
	Time t;
	unsigned int dur;
	wdt_reset();

	while(chance)
	{
		dtmf_buf[0] = dtmf_buf[1] = dtmf_buf[2] = dtmf_buf[3] = '0';
		dtmf_buf[4]='\0';

		pos=0;
		start_timer(WAIT_FOR_DTMF_INPUT);
		while(pos < 2) 
		{
			print_ssd_delay((char *)dtmf_buf,0,PRINT_DELAY);
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				(--chance > 0) ? audio_out(M_EnterStartTime) : 0;
				n=1;
				break;
			}
		}
		stop_timer();

		if(!check_call_status())
		{
			ivrs.level=LInvalid;
			return 0;
		}

		if(n == 0)
		{
			audio_out(M_EnterMinutes);

			start_timer(WAIT_FOR_DTMF_INPUT);
			pos = 2;
			while(pos < 4) 
			{
				print_ssd_delay((char *)dtmf_buf,0,500);
				if(timer_expired==1)
				{
					audio_out(M_NoResponse);
					audio_out(M_EnterStartTime);
					(--chance > 0) ? audio_out(M_EnterStartTime) : 0;
					n=1;
					break;
				}
			}
			stop_timer();
			if(!check_call_status())
			{
				ivrs.level=LInvalid;
				return 0;
			}

			if(n == 0)
			{
				dtmf_buf[4] = '\0';
				audio_out(M_EnteredTime);		/* The Entered Time is */

				if ((strcmp("59", (const char*)dtmf_buf+2) < 0) \
						|| (strcmp("2359", (const char*)dtmf_buf) < 0))
				{
					//print_ssd_delay("ERR ",0,1000);
					audio_out(M_Incorrect);
					(--chance > 0) ? audio_out(M_EnterStartTime) : 0;
				}
				else
				{
					play_time((unsigned char *)dtmf_buf);	
					audio_out(M_Hours);
					play_time((unsigned char *)dtmf_buf+2);
					audio_out(M_Minutes);

					t.hr=(dtmf_buf[0] - '0')*10 + (dtmf_buf[1] - '0');
					t.min=(dtmf_buf[2] - '0')*10 + (dtmf_buf[3] - '0');

					dur=t.hr * 60 + t.min;
					return (dur);
				}
			}
		}
	}
	return 1;
}

	
void init_ivrs(void)
{
	unsigned char i;
	init_state();
	theft_enable = my_eeprom_read_byte((unsigned char *)ETHEFT);
	i = my_eeprom_read_byte((unsigned char *)ECMLVL);		

	if(i==0) 	/* if communication flag is 0, normal mode */
	{
		msg_flag=0;
		comm_level_flag=0;
	}
	else if(i == 1)             /*if communication flag is 1, RESTRICTED mode*/
	{
		msg_flag=0;
		comm_level_flag=1;
	}
	else		             /*if communication flag is 2, MESSAGE only mode*/
	{
		msg_flag=1;
		comm_level_flag=0;
	}
}


/*********************************************************************************
 * This function facilitates the user to choose the operating mode of the device.  
 *********************************************************************************/
	
void change_mode(void)
{
	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;
	ivrs_buf[2]=THREE;

	while(1)
	{
		dtmf_recvd = dtmf_wait_cnt = pos = 0;
		if(0 == check_call_status())
		{
			ivrs.level = LInvalid;
			return;
		}
		switch(hw.mode)
		{ 
			case M_Auto:
				while(!dtmf_recvd)
				{
					if(dtmf_wait_cnt == DTMF_WAIT_CNT){
						ivrs.level = LInvalid; 
						return;
					}
					audio_out(M_InAutoMode);        /*Device is in Auto mode*/
					dtmf_wait();
					if(!ongoing_call)
						return;
				}

				on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
				switch(ivrs_pos)
				{
					case ONE:               /*Press One to change the mode to MANUAL*/
						hw.mode=M_Manual;
						SET_PIN(LED_CTRL_PORT,AUTO_LED);
						my_eeprom_write_int((unsigned int)(hw.mode),\
								(unsigned int*)EEPROM_START_MODE);
						while(1)
						{
							if(0 == check_call_status())
							{
								ivrs.level = LInvalid;
								return;
							}
							dtmf_recvd = dtmf_wait_cnt = 0;
							while(!dtmf_recvd)
							{
								if(dtmf_wait_cnt == DTMF_WAIT_CNT){
									ivrs.level = LInvalid; 
									return;
								}
								audio_out(M_Manualmode);    	
								/* Device changed to Manual Mode */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_BackToMainMenu);	
								/* to go back to main menu */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_Press1);			/* press 1 */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_StarExit);
								if(!ongoing_call || pos > 0)
									break;	
								dtmf_wait();
								if(!ongoing_call)
									return;
							}
							if(!ongoing_call)
								return;
							on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
							switch(ivrs_pos){
								case ONE:       /*Press ONE to go back to Main menu*/
									return;
									break;
								case STAR:
									ivrs.level = LInvalid;
									return;
									break;
								default:
									audio_out(M_Incorrect);
									audio_out(M_Reenter);
									break;
							}
						}
						break;
					case TWO:           
						byps=1;
						my_eeprom_write_byte((unsigned char)byps,(unsigned char*)EEBYPASS);
						audio_out(M_BypassMode);
						audio_out(M_Selected);
						ivrs.level = LInvalid;
						return;
						break;
					case THREE:           /*Press Three to go back to Main menu*/
						return;
						break;
					case STAR:
						ivrs.level = LInvalid;
						return;
						break;
					default:
						audio_out(M_Incorrect);
						audio_out(M_Reenter);
						break;
				}        
				break;
			case M_Manual:
				while(!dtmf_recvd )
				{
					if(dtmf_wait_cnt == DTMF_WAIT_CNT){
						ivrs.level = LInvalid; 
						return;
					}
					audio_out(M_InManualMode);       /*Device is in Manual mode*/ 
					dtmf_wait();
					if(!ongoing_call)
						return;
				}
				on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
				switch(ivrs_pos){
					case ONE:           /*Press One to change the mode to AUTO*/
						hw.mode=M_Auto;
						CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
						my_eeprom_write_int((unsigned int) (hw.mode)\
								,(unsigned int  *)EEPROM_START_MODE);
						call_motor_off = 0;
						// start the motor if power is available when changed from 
						// manual mode to auto mode
						my_eeprom_write_byte((unsigned char) call_motor_off\
								,(unsigned char  *)EE_CALL_MOTOR_OFF);
						// make call motor off variable to 0
						prev_call_motor_off=call_motor_off;
						while(1)
						{
							if(0 == check_call_status())
							{
								ivrs.level = LInvalid;
								return;
							}
							dtmf_recvd = dtmf_wait_cnt = 0;
							while(!dtmf_recvd ){
								if(dtmf_wait_cnt == DTMF_WAIT_CNT){
									ivrs.level = LInvalid; 
									return;
								}
								audio_out(M_Automode);      	
								/* Device changed to Auto Mode, to go back to main menu, 
								 * press 1 */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_StarExit);
								if(!ongoing_call || pos > 0)
									break;	
								dtmf_wait();
								if(!ongoing_call)
									return;
							}
							if(!ongoing_call)
								return;
							on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
							switch(ivrs_pos){
								case ONE:       /*Press ONE to go back to Main menu*/
									return;
									break;
								case STAR:
									ivrs.level =LInvalid;
									return;
									break;
								default:
									audio_out(M_Incorrect);
									audio_out(M_Reenter);
									break;
							}
						}
						break;
					case TWO:       
						byps=1;
						my_eeprom_write_byte((unsigned char) byps, (unsigned char  *)EEBYPASS);
						audio_out(M_BypassMode);
						audio_out(M_Selected);
						ivrs.level =LInvalid;
						return;
						break;
					case THREE:        /*Press TWO to go back to Main menu*/
						return;
						break;
					case STAR:
						ivrs.level = LInvalid;
						return;
						break;
					default:
						audio_out(M_Incorrect);
						audio_out(M_Reenter);
						break;
				}        
				break;
			case M_Timer:
				dtmf_recvd = dtmf_wait_cnt = pos = 0;

				while(!dtmf_recvd ){
					if(dtmf_wait_cnt == DTMF_WAIT_CNT){
						ivrs.level = LInvalid; 
						return;
					}
					audio_out(M_MotorInTimer);			/* Device is in Timer Mode */
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ToChangeToBypass);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_Press1);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ToChangeToManual);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_Press2);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ToChangeToAuto);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_Press3);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_BackToMainMenu);    
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_Press4);    
					if(!ongoing_call || pos > 0)
						break;	
					dtmf_wait();
					if(!ongoing_call)
						return;
				}
				if(!ongoing_call)
					return;
				on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
				switch(ivrs_pos){
					case ONE:       
						byps=1;
						my_eeprom_write_byte((unsigned char) byps, (unsigned char  *)EEBYPASS);
						audio_out(M_BypassMode);
						audio_out(M_Selected);
						ivrs.level=LInvalid;
						return;
						break;
					case TWO:
						hw.mode=M_Manual;
						SET_PIN(LED_CTRL_PORT,AUTO_LED);
						my_eeprom_write_int((unsigned int) (hw.mode)\
								,(unsigned int  *)EEPROM_START_MODE);
						while(1)
						{
							if(0 == check_call_status())
							{
								ivrs.level = LInvalid;
								return;
							}
							dtmf_recvd=dtmf_wait_cnt=0;
							while(!dtmf_recvd)
							{
								if(dtmf_wait_cnt == DTMF_WAIT_CNT){
									ivrs.level = LInvalid;
									return;
								}
								audio_out(M_Manualmode);        
								/* Device changed to Manual Mode */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_BackToMainMenu);    
								/* to go back to main menu */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_Press1);            /* press 1 */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_StarExit);			
								/* Press start to exit */
								if(!ongoing_call || pos > 0)
									break;	
								dtmf_wait();
								if(!ongoing_call)
									return;
							}
							if(!ongoing_call)
								return;
							on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
							switch(ivrs_pos){
								case ONE:       /*Press ONE to go back to Main menu*/
									return;
									break;
								case STAR:
									ivrs.level=LInvalid;
									return;
									break;
								default:
									audio_out(M_Incorrect);
									audio_out(M_Reenter);
									break;
							}
						}
						break;
					case THREE:
						hw.mode=M_Auto;
						CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
						my_eeprom_write_int((unsigned int) (hw.mode)\
								,(unsigned int  *)EEPROM_START_MODE);
						while(1)
						{
							if(0 == check_call_status())
							{
								ivrs.level = LInvalid;
								return;
							}
							dtmf_recvd = dtmf_wait_cnt = 0;
							while(!dtmf_recvd)
							{
								if(dtmf_wait_cnt == DTMF_WAIT_CNT){
									ivrs.level = LInvalid;
									return;
								}
								audio_out(M_Automode);          
								/* Device changed to Auto Mode, 
								 * to go back to main menu, press 1 */
								if(!ongoing_call || pos > 0)
									break;	
								audio_out(M_StarExit);			
								/* Press star to exit */
								if(!ongoing_call || pos > 0)
									break;	
								dtmf_wait();
								if(!ongoing_call)
									return;
							}
							if(!ongoing_call)
								return;
							on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
							switch(ivrs_pos){
								case ONE:       /*Press ONE to go back to Main menu*/
									return;
									break;
								case STAR:
									ivrs.level =LInvalid;
									return;
									break;
								default:
									audio_out(M_Incorrect);
									audio_out(M_Reenter);
									break;
							}
						}
						break;
					case FOUR:			/* Press FOUR to go back to Main menu */
						return;
					default:
						audio_out(M_Incorrect);
						audio_out(M_Reenter);
						break;
				}
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);
				break;
		}
	}
}



/********************************************************************
 * when the farmer changes his phone number, this function will sms the 
 * serial number of kisan-raja and new farmer number to the vinfinet admin number 
 ***********************************************************************/ 
void check_far_num_change(void)
{
	if(num_changed==1)
	{
		num_changed=0;
		strcpy((char *)msg_info,"SER NUM ");
		strcat((char *)msg_info, (const char *)serial_number);
		strcat((char *)msg_info, "\nFNUM ");
		strcat((char *)msg_info, (const char *)ph[0]);
		Delay_sec(GSM_DELAY);
		send_sms((unsigned char *)admin_ph[0], msg_info);
		Delay_sec(GSM_DELAY);
	}
}


/* whenever the sim in kisan raja is replaced with a new sim, 
 * this function will sms serial number of the device and new sim IMSI number to the vinfinet admin number */

void check_new_sim(void)
{
	if(!check_sim_num())
	{
		strcpy((char *)msg_info, "SER-");
		strcat((char *)msg_info, (const char *)serial_number);
		strcat((char *)msg_info, "\nIMSI-");
		strcat((char *)msg_info, (const char *)response);

		Delay_sec(GSM_DELAY);
		send_sms((unsigned char *)admin_ph[0], msg_info);
		Delay_sec(GSM_DELAY);
	}
}

/*
 * This function will copy thief number, serial number, 
 * farmer number and new sim IMSI number into global array msg_info
 */

void get_number(unsigned int admin_num)
{
	if(admin_num==0)
	{
		strcpy((char *)msg_info, "THEF NUM");
		strcat((char *)msg_info, (const char *)caller_number);
	}
	else if(admin_num==1)
	{
		strcat((char *)msg_info, "\nSER");
		strcat((char *)msg_info, (const char *)serial_number);
		strcat((char *)msg_info, "\nFNUM");
		strcat((char *)msg_info, (const char *)ph[0]);

		if(check_sim_num()== 0)
		{
			strcat((char *)msg_info, "\nIMSI");
			strcat((char *)msg_info, (const char *)response);
		}
	}
}

/********************************************************************
 * This function will sms the admin information to the caller number. 
 ********************************************************************/
//*01#
void send_device_parameters_1(void)
{
	unsigned char buff[15];
	int temp;
	float temp_float_val=0;
	unsigned int temp_r_value=0,temp_y_value=0,temp_b_value=0,temp_crange=0;

	disconnect_call();

	strcpy((char *)msg_info,"SMPS ");
	temp_float_val = ((float)adc_conv(SMPS_OUTPUT_PIN) / MAX_ADC_OUTPUT) * ADC_VREF * SMPS_ADC_MULTIPLY_FACTOR;
	itoa((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT),(char*) buff, 10);

	if((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT) < 100)
	{
		buff[4] = '\0';
		buff[3] = buff[2];
		buff[2] = buff[0];
		buff[0] = '0';
	}
	else
	{
		buff[4] = '\0';
		buff[3] = buff[2];
		buff[2] = buff[1];
	}
	buff[1] = '.';
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nPH-R ");
	temp = adc_conv(VOLTAGE_PHASE_R);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);
	strcat((char *)msg_info, "\nPH-Y ");
	temp = adc_conv(VOLTAGE_PHASE_Y);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);
	strcat((char *)msg_info, "\nPH-B ");
	temp = adc_conv(VOLTAGE_PHASE_B);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nC1 ");
	temp = adc_conv(CURRENT_PHASE_R);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nC2 ");
	temp = adc_conv(CURRENT_PHASE_Y);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nC3 ");
	temp = adc_conv(CURRENT_PHASE_B);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	/*	strcat((char *)msg_info, "\nREF VAL");
		strcat((char *)msg_info, "\nPH-R ");
		itoa(r_phase_voltage, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);

		strcat((char *)msg_info, "\nPH-Y ");
		itoa(y_phase_voltage, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);

		strcat((char *)msg_info, "\nPH-B ");
		itoa(b_phase_voltage, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);
		
		*/
	strcat((char *)msg_info, "\nREF VAL");
	r_phase_current = my_eeprom_read_int((unsigned int*)R_PHASE_CURRENT);
	y_phase_current = my_eeprom_read_int((unsigned int*)Y_PHASE_CURRENT);
	b_phase_current = my_eeprom_read_int((unsigned int*)B_PHASE_CURRENT);

	strcat((char *)msg_info, "\nC1 ");
	itoa(r_phase_current, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nC2 ");
	itoa(y_phase_current,(char*) buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nC3 ");
	itoa(b_phase_current, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);


	if((run_in_which_phase == TWO_PHASE) || (g_phase == TWO_PHASE))
	{
		temp_r_value = my_eeprom_read_int((unsigned int*)TWPH_R_PHASE_CURRENT);
		temp_y_value = my_eeprom_read_int((unsigned int*)TWPH_Y_PHASE_CURRENT);
		temp_b_value = my_eeprom_read_int((unsigned int*)TWPH_B_PHASE_CURRENT);
		strcat((char *)msg_info, "\n2ph C1 ");
		itoa(temp_r_value,(char*) buff, 10);
		strcat((char *)msg_info, (const char *)buff);

		strcat((char *)msg_info, "\n2ph C2 ");
		itoa(temp_y_value, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);

		strcat((char *)msg_info, "\n2ph C3 ");
		itoa(temp_b_value, (char*)buff, 10);
		strcat((char *)msg_info, (const char *)buff);
	}

	Delay_sec(GSM_DELAY);
	send_sms(caller_number,msg_info);
	Delay_sec(GSM_DELAY);

	strcpy((char *)msg_info, "BAT ");
	temp_float_val = ((float)adc_conv(BATTERY_VOLTAGE_PIN) / MAX_ADC_OUTPUT) * ADC_VREF * BATTERY_ADC_MULTIPLY_FACTOR;
	convert_int_to_str((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT), buff);
	buff[4] = buff[3];
	buff[3] = buff[2];
	buff[2] = buff[1];
	buff[1] = '.';
	strcat((char *)msg_info, (const char *)buff);

	cur_range = my_eeprom_read_int((unsigned int*)EECRNGE);
	strcat((char *)msg_info, "\nCrnge ");
	itoa(cur_range, (char*)buff, 10);
	strcat((char *)msg_info, (const char*)buff);

	if((run_in_which_phase == TWO_PHASE) || (g_phase == TWO_PHASE))
	{
		temp_crange = my_eeprom_read_int((unsigned int*)TWPH_EECRNGE);
		strcat((char *)msg_info, "\n2ph-Crnge ");
		itoa(temp_crange, (char*)buff, 10);
		strcat((char *)msg_info, (const char*)buff);
	}

	/*
	   strcat((char *)msg_info, "\n%C-Rise ");
	   itoa(percentage_current_raise, (char*)buff, 10);
	   strcat((char *)msg_info, (const char *)buff);
	   */

	strcat((char *)msg_info, "\nwirecut_val ");
	itoa(wire_cut_v_level, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nLOW-RNG ");
	itoa(lower_volt_range, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nHGH-RNG ");
	itoa(higher_volt_range,(char*) buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nHISTER-RNG ");
	itoa(hyster_value,(char*) buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	if(all_3_phases_loaded == 1) // newly added for phase rev
	{
		strcat((char *)msg_info, "\nALL_PH_LOADED-1");
	}
	else
	{
		strcat((char *)msg_info, "\nALL_PH_LOADED-0");

	}

	/*strcat((char *)msg_info, "\nOVR-CUR");
	  itoa(over_current_limit, (char*)buff, 10);
	  strcat((char *)msg_info, (const char *)buff);*/

	if(single_phs_enable == 1) // send the information of single phase activation in message.
	{
		strcat((char *)msg_info, "\n1-PH_ACT");
	}
	if(phase_rev_pro_enable == 1) // newly added for phase rev
	{
		strcat((char *)msg_info, "\nPH_REV_PRO-1");
	}
	else
	{
		strcat((char *)msg_info, "\nPH_REV_PRO-0");

	}

	Delay_sec(GSM_DELAY);
	send_sms(caller_number,msg_info);
	Delay_sec(GSM_DELAY);
}



#if 1 
/********************************************************************
 * This function will sms the admin information to the caller number. 
 ********************************************************************/
//*09#	 
void send_device_parameters_2(void)
{
	char buff[15]={0}, l_var;
	unsigned int temp;
	unsigned char boot_prog_info =0;

	disconnect_call();

	Delay_sec(LONG_GSM_DELAY);
	strcpy((char *)msg_info, "SIG ");
	buff[0] = '\0';
	signal_strength(buff);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nDryrun ");
	l_var = dryrun.enable + '0';	
	strncat((char *)msg_info, (const char *)&l_var, 1);
	if(dryrun.enable==1)
	{
		strcat((char *)msg_info, "\nCrnge ");
		itoa(cur_range, buff, 10);
		strcat((char *)msg_info, buff);

		strcat((char *)msg_info, "\nWtr-dlay ");
		temp = my_eeprom_read_int((unsigned int *)EEDELAY);
		convert_int_to_str(temp,(unsigned char *) buff);
		strcat((char *)msg_info, (const char *)buff);

		strcat((char *)msg_info, "\nCload ");
		temp = my_eeprom_read_int((unsigned int *)EELCUR);
		itoa(temp, buff, 10);
		strcat((char *)msg_info, buff);
	}

	strcat((char *)msg_info, "\nPeak ");
	l_var = nonpeaktime_alerts_disable + '0';	
	strncat((char *)msg_info, (const char *)&l_var, 1);

	strcat((char *)msg_info, "\nRun-in-phs ");
	l_var = run_in_which_phase + '0';	
	strncat((char *)msg_info, (const char *)&l_var, 1);

	if(g_sub_phase == 0)
		strcat((char *)msg_info, "\n2-3 PHS ACT");
	else if(g_sub_phase == THREE_PHASE_KARNATAKA)
		strcat((char *)msg_info, "\n3 PHS ACT");

	
	if(boot_prog_info ==1)
	{
		strcat((char *)msg_info, "\nBOOT VER-");
		itoa(boot_ver, buff, 10);
		strcat((char *)msg_info, (const char *)buff);
	}
	else
	{
		strcat((char *)msg_info, "\nNO BOOT PROG");
	}
	strcat((char *)msg_info, "\nAPP-VER ");
	strcat((char *)msg_info, VERSION);

	strcat((char *)msg_info, "\nComlvl ");
	if(msg_flag==1)
		strcat((char *)msg_info, "MSG");
	else if(comm_level_flag)
		strcat((char *)msg_info, "EMGY");
	else
		strcat((char *)msg_info, "NORML");

	strcat((char *)msg_info, "\nRTC ");
	Delay_sec(GSM_DELAY);
	buff[0] = '\0';
	get_clock((unsigned char *)buff); 

	Delay_sec(GSM_DELAY);
	strcat((char *)msg_info, (const char *)buff);

	Delay_sec(GSM_DELAY);
	send_sms(caller_number, msg_info);
	Delay_sec(GSM_DELAY);

#if 0
	strcpy((char *)msg_info, "DATE ");
	Delay_sec(GSM_DELAY);
	buff[0] = '\0';
	recv_response(50); 
	get_date((unsigned char *)buff); 
	print_ssd_delay("    ",0,500);
	print_ssd_delay("    ",0,500);
	Delay_sec(GSM_DELAY);
	strcat((char *)msg_info, (const char *)buff);

#endif
	strcpy((char *)msg_info, "Fnum ");
	strcat((char *)msg_info, (const char *)ph[0]);

	strcat((char *)msg_info, "\nMode ");
	if(hw.mode == M_Manual)
		strcat((char *)msg_info, "MANU");
	else if(hw.mode == M_Auto)
		strcat((char *)msg_info, "AUTO");
	else
		strcat((char *)msg_info, "TMR");

	strcat((char *)msg_info, "\nSER-NUM ");
	strcat((char *)msg_info, (const char *)serial_number);

	strcat((char *)msg_info, "\nSTAR-DEL ");
	temp = my_eeprom_read_int((unsigned int *)START_MOTOR_DELAY);
	itoa(temp, buff, 10);
	strcat((char *)msg_info, buff);


	strcat((char *)msg_info, "\nAUTO-DEL ");
	temp = my_eeprom_read_int((unsigned int *)AUTO_MODE_DELAY);
	itoa(temp, buff, 10);
	strcat((char *)msg_info, buff);

	strcat((char *)msg_info, "\nPER-TIME-DEL ");
	temp= my_eeprom_read_int((unsigned int *)START_TIMR3_DELAY);
	itoa(temp, buff, 10);
	strcat((char *)msg_info, buff);

	strcat((char *)msg_info, "\nWIRE-VL ");
	temp = my_eeprom_read_int((unsigned int *)WIRECUT_LVL);
	itoa(temp, buff, 10);
	strcat((char *)msg_info, buff);

	debugging_enable = my_eeprom_read_byte((unsigned char *)CONFIGURE_DEBUGGING_FEATURE);
	if(debugging_enable==1)
		strcat((char *)msg_info, "\nDEBG");

	if((cyclic_timer == 1) && (twice_timer==0))
		strcat((char *)msg_info, "\nCY1");
	else if((cyclic_timer == 1) && (twice_timer==1))
		strcat((char *)msg_info, "\nCY2");


	Delay_sec(GSM_DELAY);
	send_sms(caller_number, msg_info);
	Delay_sec(GSM_DELAY);
} 


#endif

/*send the voltage and current information */
void send_volt_curr_info(void)
{
	unsigned char buff[15];
	int temp;

	disconnect_call();

	strcpy((char *)msg_info, "\nR-V-");
	temp = adc_conv(VOLTAGE_PHASE_R);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nY-V-");
	temp = adc_conv(VOLTAGE_PHASE_Y);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nB-V-");
	temp = adc_conv(VOLTAGE_PHASE_B);	
	convert_int_to_str(temp, buff);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nR-C-");
	temp = adc_conv(CURRENT_PHASE_R);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nY-C-");
	temp = adc_conv(CURRENT_PHASE_Y);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nB-C-");
	temp = adc_conv(CURRENT_PHASE_B);
	convert_int_to_str(temp, buff); 
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nL-V-");
	itoa(lower_volt_range, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	strcat((char *)msg_info, "\nH-V-");
	itoa(higher_volt_range,(char*) buff, 10);
	strcat((char *)msg_info, (const char *)buff);

	cur_range = my_eeprom_read_int((unsigned int*)EECRNGE);
	strcat((char *)msg_info, "\nD-CUT-");
	itoa(cur_range, (char*)buff, 10);
	strcat((char *)msg_info, (const char*)buff);
/*
	strcat((char *)msg_info, "\nOVR-CUR-");
	itoa(over_curr_limit, (char*)buff, 10);
	strcat((char *)msg_info, (const char *)buff);
*/
	Delay_sec(GSM_DELAY);
	send_sms(caller_number,msg_info);
	Delay_sec(GSM_DELAY);

}

/********************************************************** 
 * This function will set the device with factory settings 
 **********************************************************/ 
// *02#
void remote_frst(void)
{
	unsigned int i;
	unsigned char buff[15]={0}, c=0;

	buff[0] = '1', buff[1] = '2', buff[2] = '3', buff[3] = '4';
	my_eeprom_write((const void *)buff, (void *)EEPROM_PASSWD_OFFSET, 4);

	my_eeprom_write((const void *)buff, (void *)EECNO, NDIGIT_PH_NUM);

	my_eeprom_write((const void *)buff, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);	

	i = DEFAULT_ZERO; //0;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EELCUR);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)TWPH_EELCUR);

	i = DEFAULT_DELAY; //5;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEDELAY);

	i = DEFAULT_CRANG; //25;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EECRNGE);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)TWPH_EECRNGE);

	i = DEFAULT_VOLT; // 1023;		
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEFVOLT );

	i = DEFAULT_2_PHASE_CUR; //300;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EE_TWO_PHASE_CUR);

	i = DEFAULT_LOWR_VTG; //200; 
	my_eeprom_write_int((unsigned int) i, (unsigned int *)LOWER_VOLT_RANGE);

	i = DEFAULT_HIGHR_VTG; //150; 
	my_eeprom_write_int((unsigned int) i, (unsigned int *)HIGHER_VOLT_RANGE);

	i = DEFAULT_OVR_CUR; //300;// changed from 140 to 300 
	my_eeprom_write_int((unsigned int) i, (unsigned int *)OVER_CURRENT_LIMIT);

//	i = DEFAULT_PER_RAISE; //35; 
//	my_eeprom_write_int((unsigned int) i, (unsigned int *)PERCENTAGE_CURRENT_RAISE);

	i=DEFAULT_CUR_VALUES; //50;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)R_PHASE_CURRENT);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)Y_PHASE_CURRENT);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)B_PHASE_CURRENT);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)TWPH_R_PHASE_CURRENT);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)TWPH_Y_PHASE_CURRENT);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)TWPH_B_PHASE_CURRENT);

	i = DEFAULT_DELAY; //5; 	/** default delay for motor start in auto mode **/
	my_eeprom_write_int((unsigned int) i, (unsigned int *)AUTO_MODE_DELAY);	

	i= DEFAULT_ZERO; //0;
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEPROM_PREV_DAY);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEPROM_DAILY_COUNTER);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEPROM_WEEKLY_COUNTER);
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EEPROM_DAYS);

	i = DEFAULT_DELAY; //5; 	/** default delay for motor start in start motor **/
	my_eeprom_write_int((unsigned int) i, (unsigned int *)START_MOTOR_DELAY);

//	i= DEFAULT_SIG_VAL; //3; // default signal value 3. 
//	my_eeprom_write_int((unsigned int) i, (unsigned int *)EE_SIGNAL_VAL);

	i = DEFAULT_TMR3; //6; 	/** default delay for start timer3 is 6 hours**/
	my_eeprom_write_int((unsigned int) i, (unsigned int *)START_TIMR3_DELAY);

//	i = DEFAULT_HP_DELAY; //2; 	/** default delay for motor start in start motor for higher hp **/
//	my_eeprom_write_int((unsigned int) i, (unsigned int *)HIGHER_HP_DELAY);

	i = DEFAULT_ZERO; //0; 	/** default keep default reboot counter value to zero **/
	my_eeprom_write_int((unsigned int) i, (unsigned int *)EE_REBOOT_CNTR);


	hw.mode = M_Manual;
	my_eeprom_write_int((unsigned int) (hw.mode), (unsigned int*)EEPROM_START_MODE);

	c = DEFAULT_CALIB; //'F';
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EEFTB);

	c = DEFAULT_ZERO; //0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EECALIB_FLAG);

	c = DEFAULT_ENABLE; //1;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)ETHEFT);	/**  enable/disable theft feature **/

	c = DEFAULT_ZERO; //0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EETHFT_OPR); 
	/** initially make theft detected as zero **/	

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)ECMLVL);        
	/* Set the communication level to normal */


	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       
	/* dry-run protection feature will be disabled by default */
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CONFIGURE_NONPEAKTIME_ALERTS);      
	/* non-peak time alerts feature will be disabled by default */

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EEACTIVE);
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EEACTIVE_SECURE);

	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EEBYPASS);


	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EE_BOOT_INFO);

	c =  DEFAULT_ENABLE; //1;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)BAT_INFO);

	c = THREE_PHASE_ANDHRA;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EE_PHASE);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)SUB_PHASE);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)WATER_FLOW_SENSOR);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CONFIGURE_BATTERY_MONITOR);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)TMR2_TIME_CHECK_FLG);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CALIB_STATUS);

	c =  DEFAULT_ENABLE; //1;
	my_eeprom_write_byte((unsigned char)c, (unsigned char *)FIRMWARE_UPGRADE);

	c =  DEFAULT_ENABLE; //1;
	my_eeprom_write_byte((unsigned char)c, (unsigned char *)FIRMWARE_UPGRADE_2);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CONFIGURE_CALCULATE_DURATION);	

	cyclic_timer=DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) (cyclic_timer),(unsigned char  *)EEPROM_CYCLIC_TIMER);

	twice_timer=DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) (twice_timer),(unsigned char  *)EEPROM_TWICE_TIMER);

	debugging_enable = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) debugging_enable,(unsigned char  *)CONFIGURE_DEBUGGING_FEATURE);

	debugging_flag = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) debugging_flag,(unsigned char  *)CONFIGURE_DEBUGGING_FLAG);

	c = DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c,(unsigned char  *)EEBOOT_PROG_INFO);

	c=DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c,(unsigned char  *)EE_PER_REBOOT);// periodic reboot make it 0.

	c=DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c,(unsigned char  *)EE_CALL_MOTOR_OFF);
	// switch off the motor from call 0.

	c=DEFAULT_ZERO; // 0;
	my_eeprom_write_byte((unsigned char) c,(unsigned char  *)EE_BATRY_INFO);// switch off the motor from call 0.
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)ADMIN_ACTIVATION); 
	// deactivate the device

	notify_sms_enable =  DEFAULT_ENABLE; 
	my_eeprom_write_byte((unsigned char) notify_sms_enable,(unsigned char  *)CONFIGURE_NOTIFY_SMS);

	sms_ver =  DEFAULT_ENABLE; 
	my_eeprom_write_byte((unsigned char) sms_ver,(unsigned char  *)CONFIGURE_SMS_VER);

	c = DEFAULT_ZERO;
	my_eeprom_write_byte((unsigned char) c, (unsigned char  *)CONFIGURE_AUTO_MONITOR);

#if AUTOMATE
	automation_code =DEFAULT_ZERO;
	my_eeprom_write_byte((unsigned char)automation_code, (unsigned char *)AUTOMATION_CODE);
#endif
	i = DEFAULT_WIRE_V_VAL; 	/** default delay for motor start in auto mode **/
	my_eeprom_write_int((unsigned int) i, (unsigned int *)WIRECUT_LVL);	

	single_phs_enable =  DEFAULT_ZERO; 
	my_eeprom_write_byte((unsigned char)single_phs_enable,(unsigned char  *)CONFIGURE_SINGLE_PHS);

	phase_rev_pro_enable=DEFAULT_ENABLE;//newly added for phase rev
	my_eeprom_write_byte((unsigned char)phase_rev_pro_enable,(unsigned char  *)CONFIGURE_PHASE_REV_PRO_ENABLE);

	i = 40; // for production place.
	my_eeprom_write_int((unsigned int) i, (unsigned int *)MAX_CT_VAL_AUTOMATION);

	hyster_value = DEF_HYSTER_VALUE;
	my_eeprom_write_int((unsigned int) hyster_value, (unsigned int *)HYSTER_VALUE);

	//all_3_phases_loaded = DEFAULT_ZERO;
	all_3_phases_loaded = DEFAULT_ENABLE;
	my_eeprom_write_byte((unsigned char)all_3_phases_loaded,(unsigned char  *)ALL_3_PHASES_LOADED);
}


/***********************************************************************************************
 * This function will check for active calls. 
 * If any call is active, global variable 'ongoing_call' will be set to one, 
 * otherwise it will be set to zero. 
 ****************************************************************************************************/

unsigned char check_call_status(void)
{
	wdt_reset();
	send_to_gsm_string("AT+CPAS");
	recv_response(WAIT_GSM_RESPONSE);

	if (response[7]=='4') 
	{
		print_ssd_delay("ONGC",0,VERY_SHORT_PRINT_DELAY);
		ongoing_call=1;
		return 1;
	}
	else
	{    
		print_ssd_delay("DISC",0,VERY_SHORT_PRINT_DELAY);
		ongoing_call=0;
		return 0;
	}
}


/*********************************************************************
 * This function will check if the sim in the device has been changed.
 *********************************************************************/
unsigned char check_sim_num(void)
{
	unsigned char sim_num[17];

	my_eeprom_read((char *)sim_num, (const void *)EESIM_NUM, 15);
	sim_num[15] = '\0';

	Delay_sec(GSM_DELAY);
	send_to_gsm_string("AT+CIMI");
	recv_response(WAIT_GSM_RESPONSE);
	response[15] = '\0';

	if(strcmp((const char *)response, (const char *)sim_num) == 0)
	{
		//print_ssd_delay("GOOD",0,PRINT_DELAY);
		return 1;
	}
	else
	{
		//print_ssd_delay("BAD1",0,PRINT_DELAY);
		return 0;
	}
}


/************************************************************************************************************ 
 * This function will fetch the IMSI number of the sim card from the network provider and store it in eeprom. 
 ************************************************************************************************************/
//*22#
void reg_sim_num(void)
{
	Delay_sec(GSM_DELAY);
	send_to_gsm_string("AT+CIMI");
	recv_response(WAIT_GSM_RESPONSE);

	my_eeprom_write((const void *)response, (void *)EESIM_NUM, NDIGIT_IMSI_NUM);
}


/**************************************************************
 * check whether the caller is authorized to call the device. 
 **************************************************************/
	
unsigned char authorize(void)
{
	unsigned char chance=4;
	unsigned int num;

	wdt_reset();
	/* check if caller number matches with the farmer number stored in eeprom */
	if(strncmp((const char *)caller_number, (const char *)ph[0], NDIGIT_PH_NUM) == 0)	
	{
		wdt_reset();
		return 1;
	}

	while(--chance) 
	{                          /* Ask for password 3 times */
		wdt_reset();
		pos=0;
		audio_out(M_EnterPasswd);

		if(!check_call_status()){
			ivrs.level=LInvalid;
			return 0;
		}

		print_ssd_delay("PSSD",0,PRINT_DELAY);
		start_timer(WAIT_FOR_DTMF_INPUT);
		while(pos < 4)
		{
			if(timer_expired==1) 
				break;
			print_ssd_delay(NULL,pos,BLINK_DELAY);
		}		                                /* Wait till complete password is received */
		stop_timer();	
		if(timer_expired==1)
		{
			audio_out(M_NoResponse);
			continue;
		}

		if((dtmf_buf[0] == ASCII_STAR)&&(dtmf_buf[3] == ASCII_HASH)){
			dtmf_buf[3] = '\0';
			num = atoi((const char *)dtmf_buf+1);
			return admin_access_menu(num);
		}

		dtmf_buf[4] = '\0';
		if(!strcmp((const char *)hw.passwd, (const char *)dtmf_buf)){
			wdt_reset();
			return 1;
		}
		audio_out(M_IncorrectPass);
	}
	audio_out(M_ResetPass);
	ivrs.level=LInvalid;
	return 0;
}


/****************************************************
 * This function is to provide the admin access menu. 
 ****************************************************/
unsigned char admin_access_menu(unsigned int num)
{
	unsigned char temp, buff[10]={0};
	//unsigned int i;
	wdt_reset();
	switch(num)
	{
		case 1:
			send_device_parameters_1();	
			wdt_reset();
			break;
		case 2:
			disconnect_call();
			remote_frst();	
			_delay_ms(3000);	
			break;
		case 3:
			disconnect_call();
			strcpy((char *)hw.passwd,"1234");
			my_eeprom_write((const void *)hw.passwd, (void *)EEPROM_PASSWD_OFFSET, 4);
			break;
		case 4:
			init_set_time();
			disconnect_call();
			break;
		case 5: 
			dtmf_recvd = dtmf_wait_cnt = 0;
			ivrs_buf[0]=ONE;
			ivrs_buf[1]=TWO;
			ivrs_buf[2]=THREE;

			while(!dtmf_recvd)
			{    
				if(dtmf_wait_cnt == DTMF_WAIT_CNT)
				{    
					ivrs.level = LInvalid;
					return 0; 
				}     
				audio_out(M_CustomSettings3);
				dtmf_wait();
				if(!ongoing_call)
				{
					ivrs.level = LInvalid;
					return 0;
				}
			}     
			on_dtmf_receipt(ivrs_buf,11);
			if((ivrs_pos == 2) || (ivrs_pos == 3))
			{
				init_cur_range(ivrs_pos);
				disconnect_call();
			}
			else
			{
				audio_out(M_Incorrect);
				disconnect_call();
			}
			break;
		case 6:    
			init_water_check_delay();
			disconnect_call();
			break;
		case 7:		/** to set caller number as farmer number **/
			my_eeprom_write((const void *)caller_number, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);	
			strcpy((char *)ph[0], (const char *)caller_number);
			num_changed=1;
			break;
		case 8:                     /* to reset the mcu */
			disconnect_call();
			//print_ssd_delay("REST",0,1000);
			_delay_ms(3000);	
			break;
		case 9:
			send_device_parameters_2();
			break;
		case 10:                        
			configure_dryrun_flowsensor();
			break;
			/*case 11:
			  disconnect_call();
			  strcpy((char *)msg_info,"IMSI");
			  Delay_sec(GSM_DELAY);
			  send_to_gsm_string("AT+CIMI");
			  recv_response(WAIT_GSM_RESPONSE);
			  response[15] = '\0';
			  strcat((char *)msg_info, (const char *)response);
			  Delay_sec(GSM_DELAY);
			  send_sms(caller_number, msg_info);
			  Delay_sec(GSM_DELAY);
			  break;*/
		case 12:					/* To deactivate the device */
			audio_out(M_ThankUKisanRaja);
			disconnect_call();		
			temp=0;
			my_eeprom_write_byte((unsigned char)temp, (unsigned char  *)EEACTIVE);
			my_eeprom_write_byte((unsigned char)temp, (unsigned char  *)EEACTIVE_SECURE);
			_delay_ms(5000);
			break;
			/*	case 13:					// set admin number 
				if(admin_num_change())
				disconnect_call();
				break;*/
		case 14:					/* set caller number as admin number */
			my_eeprom_write((const void *)caller_number, (void *)EEADMIN_PH_NUM, NDIGIT_PH_NUM);	
			strcpy((char *)admin_ph[0], (const char *)caller_number);
			break;
		case 15:		/* To fetch the IMSI number of the sim card from the 
					   network provider and store it in eeprom */
			configure_higher_voltage_range();
			disconnect_call();
			break;
		case 16:
			configure_lower_voltage_range();
			disconnect_call();
			break;

		case 17:
			configure_hysteresis_range();
			disconnect_call();
			break;		
		case 18:
			configure_run_in_which_phase();
			disconnect_call();
			break;
		case 19:				/** to configure non peak time alerts **/
			dtmf_recvd = dtmf_wait_cnt = pos = 0;
			ivrs_buf[0]=ONE;
			ivrs_buf[1]=TWO;

			while(!dtmf_recvd)
			{
				if(dtmf_wait_cnt == DTMF_WAIT_CNT)
				{
					ivrs.level = LInvalid;
					return 0;
				}
				audio_out(M_CustomSettings3);
				dtmf_wait();
				if(!ongoing_call)
				{
					ivrs.level = LInvalid;
					return 0;
				}
			}
			on_dtmf_receipt(ivrs_buf,11);
			switch(ivrs_pos)
			{
				case ONE:
					nonpeaktime_alerts_disable = 1;
					my_eeprom_write_byte((unsigned char) nonpeaktime_alerts_disable\
							, (unsigned char  *)CONFIGURE_NONPEAKTIME_ALERTS);
					pos=0;
					audio_out(M_EnterStartTime);
					get_time();
					if(!check_call_status())
					{
						ivrs.level = LInvalid;
						return 0;
					}
					if(!ongoing_call)
					{
						nonpeaktime_alerts_disable = 0;
						my_eeprom_write_byte((unsigned char) nonpeaktime_alerts_disable, 
								(unsigned char  *)CONFIGURE_NONPEAKTIME_ALERTS);
						ivrs.level = LInvalid;
						return 0;
					}
					strcpy((char*)nonpeak_start, (const char *)dtmf_buf);
					my_eeprom_write((const void *)nonpeak_start, (void *)NONPEAK_START, 4);
					pos=0;
					audio_out(M_EnterDuration);
					get_time();
					if(!ongoing_call)
					{
						nonpeaktime_alerts_disable = 0;
						my_eeprom_write_byte((unsigned char)nonpeaktime_alerts_disable\
								, (unsigned char *)CONFIGURE_NONPEAKTIME_ALERTS);
						return 0;
					}
					strcpy((char*)nonpeak_end, (const char *)dtmf_buf);
					my_eeprom_write((const void *)nonpeak_end, (void *)NONPEAK_END, 4);
					audio_out(M_Enabled);
					break;
				case TWO:
					nonpeaktime_alerts_disable = 0;
					my_eeprom_write_byte((unsigned char)nonpeaktime_alerts_disable\
							, (unsigned char *)CONFIGURE_NONPEAKTIME_ALERTS);
					audio_out(M_Disabled);
					break;
				default:
					break;
			}
			disconnect_call();
			break;
			/*	case 20:
				configure_battery_monitoring();
				disconnect_call();
				break;	*/
		case 21:
			configure_firmware_upgrade();
			break;
		case 22:
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			reg_sim_num();
			Delay_sec(GSM_DELAY);
			break;
			/*case 23:
			  configure_over_current_limit();	
			  disconnect_call();
			  break;		*/
		case 24:
			configure_auto_mode_delay();
			disconnect_call();
			break;
		case 25:
			signal_strength((char*)buff);
			play_number((char*)buff);
			//print_ssd_delay((char*)buff,0,500);
			break;
		case 26:
			configure_sub_phase();
			disconnect_call();
			break;
			/*	case 27:
				configure_calculate_motor_duration();
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				break;*/
		case 28:
			configure_timer_details();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();	
			break;
		case 29:        /** reboot and put the device to calibration **/
			disconnect_call();	
			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
			_delay_ms(5000);
			break;
		case 30:
			init_set_date();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
		case 31:
			configure_start_motor_delay();
			disconnect_call();
			break;
		case 32:
			configure_debugging_feature();// to send debugging msg to admin number.
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
/*
		case 33:
			configure_signal_value();
			disconnect_call();
			break;
*/
		case 34:// reboot the GSM module.
			disconnect_call();
			recv_response(50);
			Delay_sec(2);
			switch_off_gsm();
			break;
		case 35:	
			store_current();
			disconnect_call();
			break;
		case 36:
			configure_start_timer3_hours();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
/*
		case 37:
			configure_higher_hp_motor_delay();
			disconnect_call();
			break;
*/
		case 38:
			configure_auto_start();
			disconnect_call();
			break;	
		case 39:
			configure_notify_sms();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
		case 40:
			configure_sms_version();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
		case 41:
			configure_wirecut_val();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			break;
		case 43:
			configure_phase_rev();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();		//newly added for phase rev
			break;

		case 44:
			configure_all_ph_loaded();
			audio_out(M_ThankUKisanRaja);
			disconnect_call();		//newly added for phase rev
			break;

		case 96:   
			audio_out(M_Three);	
			frst_activate_without_dryrun();
			_delay_ms(5000);	
			break;
		case 95:
			audio_out(M_Three);	
			frst_activate_without_dryrun();
			temp = 1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       /* enable dry-run protection feature */
			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
			_delay_ms(5000);	
			break;	
		case 86:
			audio_out(M_Two);	
			frst_activate_without_dryrun();
			temp = TWO_PHASE;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EE_PHASE);       
			/* set the device to run in 2 phase and 3 phase */
			//i = 250; 
			//my_eeprom_write_int((unsigned int) i, (unsigned int *)HIGHER_VOLT_RANGE);
			//i = 250;
			//my_eeprom_write_int((unsigned int) i, (unsigned int *)OVER_CURRENT_LIMIT);
			g_sub_phase = 0;
			my_eeprom_write_byte((unsigned char) g_sub_phase, (unsigned char  *)SUB_PHASE);
			_delay_ms(5000);	
			break;
		case 85:
			audio_out(M_Two);	
			frst_activate_without_dryrun();
			temp = 1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       
			/* enable dry-run protection feature */
			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
			temp = TWO_PHASE;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EE_PHASE);       
			/* set the device to run in 2 phase and 3 phase */
			//i = 250; 
			//my_eeprom_write_int((unsigned int) i, (unsigned int *)HIGHER_VOLT_RANGE);
			//i = 250;
			//my_eeprom_write_int((unsigned int) i, (unsigned int *)OVER_CURRENT_LIMIT);
			g_sub_phase = 0;
			my_eeprom_write_byte((unsigned char) g_sub_phase, (unsigned char  *)SUB_PHASE);
			_delay_ms(5000);	
			break;	
		case 76:
			audio_out(M_Four);	
			frst_activate_without_dryrun();
			temp = THREE_PHASE_KARNATAKA;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EE_PHASE);       
			/* set the device to run in 3 phase */
			g_sub_phase = THREE_PHASE_KARNATAKA;
			my_eeprom_write_byte((unsigned char) g_sub_phase, (unsigned char  *)SUB_PHASE);
			_delay_ms(5000);	
			break;
		case 66: // single phase activation without dry run
			audio_out(M_One);	
			frst_activate_without_dryrun();

			single_phs_enable = 1;
			my_eeprom_write_byte((unsigned char)single_phs_enable,(unsigned char*)CONFIGURE_SINGLE_PHS);

			phase_rev_pro_enable=DEFAULT_ZERO;//newly added for phase rev
			my_eeprom_write_byte((unsigned char)phase_rev_pro_enable,\
					(unsigned char  *)CONFIGURE_PHASE_REV_PRO_ENABLE);

			//configure_single_phase();

			disconnect_call();

			_delay_ms(5000);
			break;

		case 65: // single phase activation with dryrun.
			audio_out(M_One);	
			frst_activate_without_dryrun();

			single_phs_enable = 1;
			my_eeprom_write_byte((unsigned char)single_phs_enable,(unsigned char*)CONFIGURE_SINGLE_PHS);

			//configure_single_phase();

			temp = 1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       
			/* enable dry-run protection feature */

			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);

			phase_rev_pro_enable=DEFAULT_ZERO;//newly added for phase rev
			my_eeprom_write_byte((unsigned char)phase_rev_pro_enable,\
					(unsigned char  *)CONFIGURE_PHASE_REV_PRO_ENABLE);

			disconnect_call();
			_delay_ms(5000);

			break;

		case 75:
			audio_out(M_Four);	
			frst_activate_without_dryrun();
			temp = 1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       
			/* enable dry-run protection feature */
			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
			temp = THREE_PHASE_KARNATAKA;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EE_PHASE);       
			/* set the device to run in 3 phase */
			g_sub_phase = THREE_PHASE_KARNATAKA;
			my_eeprom_write_byte((unsigned char) g_sub_phase, (unsigned char  *)SUB_PHASE);
			_delay_ms(5000);	
			break;	
		case 69:
			ser_num_change();
			disconnect_call();
			break;
		case 98:
			init_set_time();
			Delay_sec(1);
			ser_num_change();
			Delay_sec(1);
			send_device_parameters_2();
			disconnect_call();
			break;
		case 99:
			audio_out(M_Three);	
			frst_activate_without_dryrun();
			temp = 1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);       
			/* enable dry-run protection feature */
			temp = 'F';
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
			temp =1;
			my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EECALIB_FLAG);
			_delay_ms(5000);	
			break;	
#if AUTOMATE
		case 90:
			audio_out(M_Disabled);
			audio_out(M_ThankUKisanRaja);
			disconnect_call();
			remote_frst();
			automation_done =0;
			my_eeprom_write_byte((unsigned char)automation_done, (unsigned char *)AUTOMATION_DONE);
			print_ssd_delay("\n\rFRST_OK\n\r",0,500);
			_delay_ms(5000); // rebooot the device		
			break;
#endif
		default:
			audio_out(M_WrongPassword);
			break;
	}

	return 0;
}


/***************************************************************************
 * This function is to enable or disable battery low sensing fucntionality.
 ***************************************************************************/
/*unsigned char configure_battery_monitoring(void)
  {
  dtmf_recvd = dtmf_wait_cnt = 0;

  ivrs_buf[0]=ONE;
  ivrs_buf[1]=TWO;

  while(!dtmf_recvd)
  {    
  if(dtmf_wait_cnt == DTMF_WAIT_CNT)
  {    
  ivrs.level = LInvalid;
  return 1; 
  }     
  audio_out(M_CustomSettings3);
  dtmf_wait();
  if(!ongoing_call)
  {
  ivrs.level = LInvalid;
  return 0;
  }
  }     
  on_dtmf_receipt(ivrs_buf,11);
  switch(ivrs_pos)
  {    
  case ONE: 
  enable_battery_monitor = 1; 
  my_eeprom_write((const void *)&enable_battery_monitor, (void *)CONFIGURE_BATTERY_MONITOR, 1);
  audio_out(M_Enabled);
  break;
  case TWO:
  enable_battery_monitor = 0; 
  my_eeprom_write((const void *)&enable_battery_monitor, (void *)CONFIGURE_BATTERY_MONITOR, 1);
  audio_out(M_Disabled);
  break;
  default:
  audio_out(M_Incorrect);
  break;
  }
  return 0;
  }
  */

/***************************************************************************
 * This function is to enable or disable auto start fucntionality.
 ***************************************************************************/
unsigned char configure_auto_start(void)
{
	dtmf_recvd = dtmf_wait_cnt = 0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return 1; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE: 
			enable_auto_monitor = 1; 
			my_eeprom_write_byte((unsigned char) enable_auto_monitor\
					, (unsigned char  *)CONFIGURE_AUTO_MONITOR);
			audio_out(M_Enabled);
			break;
		case TWO:
			enable_auto_monitor = 0; 
			my_eeprom_write_byte((unsigned char) enable_auto_monitor\
					, (unsigned char  *)CONFIGURE_AUTO_MONITOR);
			audio_out(M_Disabled);
			break;
		default:
			audio_out(M_Incorrect);
			break;
	}
	return 0;
}

/***********************************************************************
 * This function is to enable or disable calculating motor run duration.
 **********************************************************************/
#if 0
int configure_calculate_motor_duration(void)
{
	int i;
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return 0; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE:
			org_num(); 
			calculate_duration_enable = 1;
			my_eeprom_write((const void *)&calculate_duration_enable,(void *)CONFIGURE_CALCULATE_DURATION, 1);
			audio_out(M_Enabled);
			break;	
		case TWO:
			i=0;
			my_eeprom_write((const void *)&i, (void *)EEPROM_PREV_DAY, sizeof(int));
			my_eeprom_write((const void *)&i, (void *)EEPROM_DAILY_COUNTER, sizeof(int));
			my_eeprom_write((const void *)&i, (void *)EEPROM_WEEKLY_COUNTER, sizeof(int));
			my_eeprom_write((const void *)&i, (void *)EEPROM_DAYS, sizeof(int));

			calculate_duration_enable = 0; 
			my_eeprom_write((const void *)&calculate_duration_enable,(void *)CONFIGURE_CALCULATE_DURATION, 1);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return 1;
}   


#endif
/**********************************************************
 * This function is to enable or disable debugging feature.
 *********************************************************/

int configure_debugging_feature(void)
{
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return 0; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE:
			debugging_enable = 1;
			my_eeprom_write_byte((unsigned char) debugging_enable\
					,(unsigned char  *)CONFIGURE_DEBUGGING_FEATURE);
			my_eeprom_write((const void *)caller_number, (void *)EEDEBUG_PH_NUM, NDIGIT_PH_NUM);	
			strcpy((char *)debug_ph, (const char *)caller_number);
			audio_out(M_Enabled);
			break;	
		case TWO:
			debugging_enable = 0;
			my_eeprom_write_byte((unsigned char)debugging_enable\
					,(unsigned char *)CONFIGURE_DEBUGGING_FEATURE);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return 1;
}   


void configure_notify_sms(void)
{
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE:
			notify_sms_enable = 1;
			my_eeprom_write_byte((unsigned char)notify_sms_enable\
					,(unsigned char*)CONFIGURE_NOTIFY_SMS);
			audio_out(M_Enabled);
			break;	
		case TWO:
			notify_sms_enable= 0;
			my_eeprom_write_byte((unsigned char)notify_sms_enable\
					,(unsigned char *)CONFIGURE_NOTIFY_SMS);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return;
}   


void configure_single_phase(void)
{
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE:
			single_phs_enable = 1;
			my_eeprom_write_byte((unsigned char)single_phs_enable\
					,(unsigned char*)CONFIGURE_SINGLE_PHS);
			audio_out(M_Enabled);
			break;	
		case TWO:
			single_phs_enable = 0;
			my_eeprom_write_byte((unsigned char)single_phs_enable\
					,(unsigned char *)CONFIGURE_SINGLE_PHS);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return;
}   



void configure_sms_version(void)
{
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE:
			sms_ver = 1;
			my_eeprom_write_byte((unsigned char)sms_ver,(unsigned char *)CONFIGURE_SMS_VER);
			audio_out(M_Enabled);
			break;	
		case TWO:
			sms_ver= 0;
			my_eeprom_write_byte((unsigned char)sms_ver,(unsigned char *)CONFIGURE_SMS_VER);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return;
}   





/*********************************************************
 * This function is to enable or disable firmware upgrade.
 ********************************************************/
//*21#
unsigned char configure_firmware_upgrade(void)
{
	if(firm_passwd())
	{
		dtmf_recvd = dtmf_wait_cnt = 0;
		unsigned char temp = 0;

		ivrs_buf[0]=ONE;
		ivrs_buf[1]=TWO;

		while(!dtmf_recvd)
		{    
			if(dtmf_wait_cnt == DTMF_WAIT_CNT)
			{    
				ivrs.level = LInvalid;
				return 1; 
			}     
			audio_out(M_CustomSettings3);
			dtmf_wait();
			if(!check_call_status())
			{
				ivrs.level = LInvalid;
				return 0;
			}
		}     
		on_dtmf_receipt(ivrs_buf,11);
		switch(ivrs_pos)
		{    
			case ONE: 
				temp = 0;
				my_eeprom_write_byte((unsigned char)temp,(unsigned char*) FIRMWARE_UPGRADE);
				my_eeprom_write_byte((unsigned char)temp,(unsigned char*) FIRMWARE_UPGRADE_2);

				my_eeprom_write((const void *)caller_number, (void *)EEB_PH_NUM, NDIGIT_PH_NUM);	
				strcpy((char *)b_ph, (const char *)caller_number);

				audio_out(M_Enabled);				//firmware upgrade is enabled.
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				_delay_ms(5000);
				break;
			case TWO:
				temp = 1;
				my_eeprom_write_byte((unsigned char)temp,(unsigned char*) FIRMWARE_UPGRADE);
				my_eeprom_write_byte((unsigned char)temp,(unsigned char*) FIRMWARE_UPGRADE_2);
				audio_out(M_Disabled);				//firmware upgrade is disabled.
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				break;
			default:
				audio_out(M_Incorrect);
				break;
		}
		return 0;
	}
	return 1;
}




/******************************************************************************************************************** 
 * This function is used for factory reset and activation of device through admin access *96#, *95#, *86#, *85#
 ********************************************************************************************************************/
void frst_activate_without_dryrun(void)
{
	unsigned char temp;

	audio_out(M_ThankUKisanRaja);
	disconnect_call();
	remote_frst();
	my_eeprom_write((const void *)caller_number, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);	
	strcpy((char *)ph[0], (const char *)caller_number);
	temp = 1;
	my_eeprom_write_byte((unsigned char)temp, (unsigned char *)EEACTIVE);
	my_eeprom_write_byte((unsigned char)temp, (unsigned char *)EEACTIVE_SECURE);
	admin_activation = 1;
	my_eeprom_write_byte((unsigned char)admin_activation, (unsigned char *)ADMIN_ACTIVATION);
	reg_sim_num();

	/* Added to disable motor calibration during *76#, *86# *96#. */
	temp = 'T';
	my_eeprom_write_byte((unsigned char)temp, (unsigned char *)EEFTB);
}


/****************************************************************
 * This function is to configure dry run and water flow sensor.
 ****************************************************************/ 
//*10#
unsigned char configure_dryrun_flowsensor(void)
{
	unsigned char c;

	dtmf_recvd = dtmf_wait_cnt = 0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{    
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{    
			ivrs.level = LInvalid;
			return 0; 
		}     
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}     
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{    
		case ONE: 
			if(dryrun.enable==1)
			{    
				dryrun.enable = 0; 
				my_eeprom_write_byte((unsigned char) dryrun.enable\
						, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);
				audio_out(M_Disabled);
				disconnect_call();
				c = 0;
				my_eeprom_write_byte((unsigned char) c\
						, (unsigned char  *)EE_BOOT_INFO);
				_delay_ms(3000);
			}    
			else 
			{    
				dryrun.enable = 1; 
				my_eeprom_write_byte((unsigned char) dryrun.enable\
						, (unsigned char  *)CONFIGURE_DRYRUN_PROTECTION);
				audio_out(M_Enabled);
				disconnect_call();
				c = 'F';  
				my_eeprom_write_byte((unsigned char) c, (unsigned char  *)EEFTB);
				_delay_ms(5000);
			}    
			break;
		case TWO:
			/*if(water_flow_sensor==1)
			  {
			  water_flow_sensor=0;
			  my_eeprom_write_byte((unsigned char) water_flow_sensor, (unsigned char  *)WATER_FLOW_SENSOR);
			  audio_out(M_Disabled);
			  }
			  else
			  {
			  water_flow_sensor=1;
			  my_eeprom_write_byte((unsigned char) water_flow_sensor, (unsigned char  *)WATER_FLOW_SENSOR);
			  audio_out(M_Enabled);
			  }*/
			audio_out(M_Incorrect);
			break;
		default:
			break;
	}
	return 0;
}


/******************************************************************************
 * This function is used to play the audio of a number stored in string form.
 ******************************************************************************/ 
void play_number(char *buff)
{
	unsigned char i;

	for(i=0; buff[i] != '\0'; i++)	
	{
		switch(buff[i]-'0')
		{
			case 0:
				audio_out(M_Zero);
				break;
			case 1:
				audio_out(M_One);
				break;
			case 2:
				audio_out(M_Two);
				break;
			case 3:
				audio_out(M_Three);
				break;
			case 4:
				audio_out(M_Four);
				break;
			case 5:
				audio_out(M_Five);
				break;
			case 6:
				audio_out(M_Six);
				break;
			case 7:
				audio_out(M_Seven);
				break;
			case 8:
				audio_out(M_Eight);
				break;
			case 9:
				audio_out(M_Nine);
				break;
			default:
				break;
		}
	}
}

#if 0
/******************************************************************************
 * This function is to configure the OVER CURRENT LIMIT through admin access.
 * This value will be used to protect the motor from over current.
 ******************************************************************************/ 
unsigned char configure_over_current_limit(void)
{
	char buff[10];
	pos=0;

	over_current_limit = my_eeprom_read_int((unsigned int *)OVER_CURRENT_LIMIT);
	itoa(over_current_limit, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 3)
	{
		if(timer_expired==1) 
			return 1;
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[3] = '\0';
	over_current_limit = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)over_current_limit, (unsigned int *)OVER_CURRENT_LIMIT);
	itoa(over_current_limit, buff, 10);	
	play_number(buff);	

	return 0;	
}
#endif

/******************************************************************************
 * This function is to configure the MOTOR START DELAY IN AUTO MODE through admin access.
 ******************************************************************************/
//*24#
unsigned char configure_auto_mode_delay(void)
{
	char buff[10];
	pos=0;

	auto_mode_delay = my_eeprom_read_int((unsigned int *)AUTO_MODE_DELAY);
	itoa(auto_mode_delay, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 3)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[3] = '\0';
	auto_mode_delay = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)auto_mode_delay, (unsigned int*)AUTO_MODE_DELAY);	
	itoa(auto_mode_delay, buff, 10);	
	play_number(buff);
	return 0;	
}

unsigned char configure_wirecut_val(void)
{
	char buff[10];
	pos=0;
	wire_cut_v_level = my_eeprom_read_int((unsigned int *)WIRECUT_LVL);
	itoa(wire_cut_v_level, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 3)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[3] = '\0';
	wire_cut_v_level = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)wire_cut_v_level, (unsigned int*)WIRECUT_LVL);
	itoa(wire_cut_v_level, buff, 10);	
	play_number(buff);	
	return 0;	
}

#if 0
/******************************************************************************
 * This function is to configure the SIGNAL QUALITY VALUE through admin access.
 ******************************************************************************/ 
unsigned char configure_signal_value(void)
{
	char buff[10];
	pos=0;
	signal_val = my_eeprom_read_int((unsigned int *)EE_SIGNAL_VAL);
	itoa(signal_val, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	signal_val = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)signal_val, (unsigned int*)EE_SIGNAL_VAL);	
	itoa(signal_val, buff, 10);	
	play_number(buff);
	return 0;	
}

#endif

/***************************************************************************
 * This function is to configure the MOTOR START DELAY through admin access.
 **************************************************************************/
//*31#
unsigned char configure_start_motor_delay(void)
{
	char buff[10];
	pos=0;

	start_motor_delay = my_eeprom_read_int((unsigned int*)START_MOTOR_DELAY);
	itoa(start_motor_delay, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	start_motor_delay = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)start_motor_delay, (unsigned int*)START_MOTOR_DELAY);	
	itoa(start_motor_delay, buff, 10);	
	play_number(buff);
	return 0;	
}


#if 0
/*************************************************************************************
 * This function is to configure the Higher HP MOTOR START DELAY through admin access.
 *************************************************************************************/ 
unsigned char configure_higher_hp_motor_delay(void)
{
	char buff[10];
	pos=0;

	higher_hp_delay = my_eeprom_read_int((unsigned int *)HIGHER_HP_DELAY);
	itoa(higher_hp_delay, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	higher_hp_delay = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)higher_hp_delay, (unsigned int *)HIGHER_HP_DELAY);	
	itoa(higher_hp_delay, buff, 10);	
	play_number(buff);
	return 0;	
}

#endif
/****************************************************************************
 * This function is to configure the START_TIMER3 DELAY through admin access.
 ***************************************************************************/ 
unsigned char configure_start_timer3_hours(void)
{
	char buff[10];
	pos=0;

	timer3_hours = my_eeprom_read_int((unsigned int *)START_TIMR3_DELAY);
	itoa(timer3_hours, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1){ 
			stop_timer();	
			return 1;
		}
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	timer3_hours = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)timer3_hours, (unsigned int*)START_TIMR3_DELAY);	
	itoa(timer3_hours, buff, 10);	
	play_number(buff);
	return 0;	
}


/************************************************************************************
 * This function is to configure the PERCENTAGE_CURRENT_RAISE through admin access.
 ************************************************************************************/ 
#if 0
unsigned char configure_percentage_current_raise(void)
{
	char buff[10];
	pos=0;

	my_eeprom_read((void *)&percentage_current_raise, (const void *)PERCENTAGE_CURRENT_RAISE, sizeof(int));
	itoa(percentage_current_raise, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1) 
			return 1;
		print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	percentage_current_raise = atoi((const char *)dtmf_buf);
	my_eeprom_write((const void *)&percentage_current_raise, (void *)PERCENTAGE_CURRENT_RAISE, sizeof(int));	

	return 0;	
}

#endif
/******************************************************************************
 * This function is to configure the LOWER VOLTAGE RANGE through admin access.
 ******************************************************************************/ 
//*16#
unsigned char configure_lower_voltage_range(void)
{
	char buff[10];
	pos=0;

	lower_volt_range = my_eeprom_read_int((unsigned int *)LOWER_VOLT_RANGE);
	itoa(lower_volt_range, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 3)
	{
		if(timer_expired==1) 
			return 1;
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[3] = '\0';
	lower_volt_range = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)lower_volt_range, (unsigned int*)LOWER_VOLT_RANGE);	
	itoa(lower_volt_range, buff, 10);	
	play_number(buff);
	return 0;	
}

//*17#
char configure_hysteresis_range(void)
{
	char buff[10];
	pos=0;

	hyster_value = my_eeprom_read_int((unsigned int *)HYSTER_VALUE);
	itoa(hyster_value, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 2)
	{
		if(timer_expired==1) 
			return 1;
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[2] = '\0';
	hyster_value = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)hyster_value, (unsigned int*)HYSTER_VALUE);	
	itoa(hyster_value, buff, 10);	
	play_number(buff);
	return 0;

}
/******************************************************************************
 * This function is to configure the HIGHER VOLTAGE RANGE through admin access.
 * This value will be used to protect the motor from high voltage.
 ******************************************************************************/ 
//*15#
unsigned char configure_higher_voltage_range(void)
{
	char buff[10];
	pos=0;

	higher_volt_range = my_eeprom_read_int((unsigned int *)HIGHER_VOLT_RANGE);
	itoa(higher_volt_range, buff, 10);	
	play_number(buff);
	audio_out(M_Enter);
	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < 3)
	{
		if(timer_expired==1) 
			return 1;
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}		                                
	stop_timer();	
	dtmf_buf[3] = '\0';
	higher_volt_range = atoi((const char *)dtmf_buf);
	my_eeprom_write_int((unsigned int)higher_volt_range, (unsigned int*)HIGHER_VOLT_RANGE);	
	itoa(higher_volt_range, buff, 10);	
	play_number(buff);
	return 0;	
}



/*********************************************************************
 * This function is to configure the parameter 'run_in_which_phase'
 **********************************************************************/
//*18#
unsigned char configure_run_in_which_phase(void)
{
	pos = dtmf_recvd = 0; 
	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT){
			ivrs.level = LInvalid; 
			return 1;
		}
		if(run_in_which_phase == THREE_PHASE_KARNATAKA)
			audio_out(M_Three);
		else 
			audio_out(M_Two);

		audio_out(M_Enter);
		if(!dtmf_recvd){
			dtmf_wait();
			if(!ongoing_call)
				return 1;
		}
	}

	if(dtmf_buf[0] == '2')
	{    
		run_in_which_phase = TWO_PHASE;
		my_eeprom_write_byte((unsigned char)run_in_which_phase, (unsigned char*)EE_PHASE);
		audio_out(M_Two);
		audio_out(M_Enabled);
	}
	else if(dtmf_buf[0] == '3')
	{    
		run_in_which_phase = THREE_PHASE_KARNATAKA;
		my_eeprom_write_byte((unsigned char)run_in_which_phase, (unsigned char*)EE_PHASE);
		audio_out(M_Three);
		audio_out(M_Enabled);
	}    

	return 0;
}


/***********************************************************************************************************
 * This function is to configure the parameter 'g_sub_phase'.
 * This parameter is specific to *85# activation (or) *86# activation.
 * This parameter will be useful for the farmers who are 
 * using 2-phase to 3-phase converter and if they want to get calls only in three phase.
 ****************************************************************************************/
//*26#
unsigned char configure_sub_phase(void)
{
	pos = dtmf_recvd = 0; 
	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT){
			ivrs.level = LInvalid; 
			return 1;
		}
		if(g_sub_phase == THREE_PHASE_KARNATAKA)
			audio_out(M_Three);
		else 
			audio_out(M_Zero);

		audio_out(M_Enter);
		if(!dtmf_recvd){
			dtmf_wait();
			if(!ongoing_call)
				return 1;
		}
	}

	if(dtmf_buf[0] == '3') 		/** device will make a call only in three phase **/
	{    
		g_sub_phase = THREE_PHASE_KARNATAKA;
		my_eeprom_write_byte((unsigned char)g_sub_phase, (unsigned char *)SUB_PHASE);
		audio_out(M_Three);
		audio_out(M_Enabled);
	}    
	else if(dtmf_buf[0] == '0')	/** device will make a call both in 2-phase and 3-phase **/
	{
		g_sub_phase = 0;
		my_eeprom_write_byte((unsigned char)g_sub_phase, (unsigned char *)SUB_PHASE);
		audio_out(M_Zero);
		audio_out(M_Enabled);

	}

	return 0;
}
/**************************************************************
  This function is to configure the parameter phase rev program enable
 ***************************************************************/
unsigned char configure_phase_rev(void)
{
	pos = dtmf_recvd = 0; 
	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT){
			ivrs.level = LInvalid; 
			return 1;
		}
		if(phase_rev_pro_enable == 1)
			audio_out(M_One);
		else 
			audio_out(M_Zero);

		audio_out(M_Enter);
		if(!dtmf_recvd){
			dtmf_wait();
			if(!ongoing_call)
				return 1;
		}
	}

	if(dtmf_buf[0] == '1') 		/** device will make a call only in three phase **/
	{    
		phase_rev_pro_enable = 1;
		my_eeprom_write_byte((unsigned char)phase_rev_pro_enable, (unsigned char *)CONFIGURE_PHASE_REV_PRO_ENABLE);
		audio_out(M_One);
		audio_out(M_Enabled);
	}    
	else if(dtmf_buf[0] == '0')	/** device will make a call both in 2-phase and 3-phase **/
	{
		phase_rev_pro_enable = 0;
		my_eeprom_write_byte((unsigned char)phase_rev_pro_enable, (unsigned char *)CONFIGURE_PHASE_REV_PRO_ENABLE);
		audio_out(M_Zero);
		audio_out(M_Disabled);

	}

	return 0;
}


void configure_all_ph_loaded(void)
{
	pos = dtmf_recvd = 0; 
	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT){
			ivrs.level = LInvalid; 
			return ;
		}
		if(all_3_phases_loaded == 1)
			audio_out(M_One);
		else 
			audio_out(M_Zero);

		audio_out(M_Enter);
		if(!dtmf_recvd){
			dtmf_wait();
			if(!ongoing_call)
				return ;
		}
	}

	if(dtmf_buf[0] == '1') 		/** device will make a call only in three phase **/
	{    
		all_3_phases_loaded = 1;
		my_eeprom_write_byte((unsigned char)all_3_phases_loaded, (unsigned char *)ALL_3_PHASES_LOADED);
		audio_out(M_One);
	}    
	else if(dtmf_buf[0] == '0')	/** device will make a call both in 2-phase and 3-phase **/
	{
		all_3_phases_loaded = 0;
		my_eeprom_write_byte((unsigned char)all_3_phases_loaded, (unsigned char *)ALL_3_PHASES_LOADED);
		audio_out(M_Zero);
	}

	return ;
}

/*********************************************************************
 * This function is to store the present value of current to EEPROM. 
 *********************************************************************/
unsigned char store_current(void)
{
	unsigned int temp_r_cur=0,temp_y_cur=0,temp_b_cur=0,temp_cload=0,temp_crange=0;
	wdt_reset();
	pos = dtmf_recvd = 0; 
	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT){
			ivrs.level = LInvalid; 
			return 1;
		}
		audio_out(M_Enter);
		if(!dtmf_recvd){
			dtmf_wait();
			if(!ongoing_call)
				return 1;
		}
	}
	if(dtmf_buf[0] == '2') 		/** store the present values in 2 phase curent values **/
	{    
		audio_out(M_Two);
		temp_r_cur= adc_conv(CURRENT_PHASE_R);
		temp_y_cur = adc_conv(CURRENT_PHASE_Y);
		temp_b_cur = adc_conv(CURRENT_PHASE_B);

		if((temp_r_cur !=0) && (temp_y_cur !=0) && (temp_b_cur !=0))
		{	
			my_eeprom_write_int((unsigned int) temp_r_cur, (unsigned int *)TWPH_R_PHASE_CURRENT);
			my_eeprom_write_int((unsigned int) temp_y_cur, (unsigned int *)TWPH_Y_PHASE_CURRENT);
			my_eeprom_write_int((unsigned int) temp_b_cur, (unsigned int *)TWPH_B_PHASE_CURRENT);
			temp_cload = temp_r_cur + temp_y_cur +temp_b_cur;
			my_eeprom_write_int((unsigned int) temp_cload, (unsigned int *)TWPH_EELCUR);
			temp_crange= 0.7 * temp_cload;
			my_eeprom_write_int((unsigned int) temp_crange,(unsigned int *)TWPH_EECRNGE);	
		}
	}    
	else if(dtmf_buf[0] == '3')	/** device will make a call both in 2-phase and 3-phase **/
	{
		audio_out(M_Three);
		temp_r_cur= adc_conv(CURRENT_PHASE_R);
		temp_y_cur = adc_conv(CURRENT_PHASE_Y);
		temp_b_cur = adc_conv(CURRENT_PHASE_B);
		if((temp_r_cur !=0) && (temp_y_cur !=0) && (temp_b_cur !=0))
		{
			my_eeprom_write_int((unsigned int) temp_r_cur, (unsigned int *)R_PHASE_CURRENT);
			my_eeprom_write_int((unsigned int) temp_y_cur, (unsigned int *)Y_PHASE_CURRENT);
			my_eeprom_write_int((unsigned int) temp_b_cur, (unsigned int *)B_PHASE_CURRENT);
			temp_cload = temp_r_cur + temp_y_cur +temp_b_cur;
			my_eeprom_write_int((unsigned int) temp_cload, (unsigned int *)EELCUR);
			temp_crange= 0.7 * temp_cload;
			my_eeprom_write_int((unsigned int) temp_crange,(unsigned int *)EECRNGE);	
		}
	}
	return 0;
}

void activate_box(void)
{
	unsigned char temp;

	wdt_reset();
	if(ri)
	{
		send_serial0_automate('\n');
		send_serial0_automate('\r');
		do
		{
			temp = recv_response(100);
			print_ssd_delay((char*)response,0,500);
		}while (strcmp((const char*)response,"RING") && !temp);

		if(strcmp((const char*)response,"RING")){
			ri=0;
			return;
		}

		take_call();

		/*  copy the admin phone numbers*/
		strcpy((char*)admin_ph[0], "9980010810"); // customer care number
		strcpy((char*)admin_ph[1], "9008132026"); // shiva  kumar personal number
		strcpy((char*)admin_ph[2], "8041644294"); // vinfinet landline number
		strcpy((char*)admin_ph[3], "9886910823"); // vijay personal number
		//strcpy((char*)admin_ph[4], "9886112080"); // vinod personal number 1
		strcpy((char*)admin_ph[4], "9972336726"); // vinod personal number 2
		strcpy((char*)admin_ph[5], "9989374566"); // Rama krishna number 1
		strcpy((char*)admin_ph[6], "9963473344"); // rama krishna number 2
		strcpy((char*)admin_ph[7], "8553669213"); // A
		strcpy((char*)admin_ph[8], "8553669429"); // A
		strcpy((char*)admin_ph[9], "9742634509"); // kumar number
		strcpy((char*)admin_ph[10], MASTER_NUMBR); // Pallavi numbr change after testing.

		/* maximum phone numbers are 15, copy the admin phone numbers*/
		if(((!strcmp((const char *)caller_number, (const char *)admin_ph[0])) ||
					(!strcmp((const char *)caller_number, (const char *)admin_ph[1])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[2])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[3])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[4])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[5])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[6])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[7])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[8])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[9])) \
					||(!strcmp((const char *)caller_number, (const char *)admin_ph[10]))) \
				&& (admin_activation == 0)) 
		{
			pos=0;
			audio_out(M_Welcome);
			audio_out(M_Enter4DigitPasswd);
			start_timer(WAIT_FOR_DTMF_INPUT);
			while(pos < 4)
			{
				if(timer_expired==1)
					break;
				//print_ssd_delay(NULL,pos,50);
			}
			stop_timer();
			dtmf_buf[4]='\0';
			if(!strcmp((const char *)dtmf_buf,"6969"))
			{
				admin_activation = 1;
				my_eeprom_write_byte((unsigned char)admin_activation, (unsigned char *)ADMIN_ACTIVATION);
				audio_out(M_Welcome);
				audio_out(M_One);
				audio_out(M_ThankUKisanRaja);
				disconnect_call();		
				Delay_sec(2);	
				//print_ssd_delay("ACT1",0,1000);

				send_details();  	
				// send sms to admin number regarding serial number, date , and time.

				_delay_ms(5000);
			}
#if AUTOMATE
			else if(!strcmp((const char *)dtmf_buf,"1880"))
			{
				automate_serial_numbr_rtc();	
				return;
			}
			else if(!strcmp((const char *)dtmf_buf,"1990"))
			{
				automation_code =1;
				my_eeprom_write_byte((unsigned char)automation_code, (unsigned char *)AUTOMATION_CODE);
				my_eeprom_write((const void *)caller_number, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);
				Delay_sec(1);
				my_eeprom_read((void *)ph[0],(const void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);
				ph[0][10] = '\0';
				temp = 'F';  
				my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
				temp =1;    
				my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EECALIB_FLAG);
				Delay_sec(1);
				calib_flag = my_eeprom_read_byte((unsigned char *)EECALIB_FLAG);
				disconnect_call();		
			}
#endif
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				disconnect_call();
			}
			else
			{
				audio_out(M_WrongPassword);
				disconnect_call();
			}
		}
		else if(admin_activation == 1)
		{
			if(authorize())
			{
				return ;
			}
			else
			{
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
			}	

		}
		else
		{
			audio_out(M_Welcome);
			audio_out(M_Disabled);
			audio_out(M_CallCustCare);
			audio_out(M_ThankUKisanRaja);

			// below code is added so that because of not asking PSWD from admin numbr at all. 
			pos=0;
			start_timer(10);
			while(pos < 1)
			{
				if(timer_expired==1)
					break;
			}
			stop_timer();
			dtmf_buf[1]='\0';
			if(dtmf_buf[0] == '6')
			{
				if(authorize())
				{
					return ;
				}
			}
			disconnect_call();
		}
	}
#if AUTOMATE
	else
	{
		get_ser_gsm();
		call_master_device();
	}
#else
	else
	{
		wdt_reset();
		check_sig_reg(); /* switch off the GSM module if signal is less than 5 or simcard is not registered */
		wdt_reset();
	}
#endif
	wdt_reset();
}

#if 1
/****** send sms to admin number regarding serial number, date , and time. ******/
void send_details(void)
{
	char buff[15]={0};

	strcpy((char *)msg_info," SER ");
	strcat((char *)msg_info, (const char *)serial_number);

	strcat((char *)msg_info, "\nRTC ");
	Delay_sec(GSM_DELAY);
	buff[0] = '\0';
	get_clock((unsigned char *)buff); 
	Delay_sec(GSM_DELAY);
	strcat((char *)msg_info, (const char *)buff);

	/*strcat((char *)msg_info, "\nDATE ");
	  Delay_sec(GSM_DELAY);
	  buff[0] = '\0';
	  get_date((unsigned char *)buff); 
	  Delay_sec(GSM_DELAY);
	  strcat((char *)msg_info, (const char *)buff);
	  */
	Delay_sec(GSM_DELAY);
	send_sms(caller_number,msg_info);
	Delay_sec(GSM_DELAY);

}
#endif
	
void play_unfavourable_msg(Error err)
{
	switch(err) 
	{
		case E_NoPower:
			audio_out(M_OffNoPower);
			break;
		case E_NoWater:
			audio_out(M_NoWaterSwitchOff);
			break;
		case E_OverCurrent:
			audio_out(M_MotorSwitchOff1);
			audio_out(M_FailSwitchOn);
			break;
		case E_WaterWait:
			audio_out(M_MotorOnWaterCheck);
			break;
		case E_ManualStop:
			audio_out(M_OffManual);
			break;
		case E_PhaseRm:
			audio_out(M_OffNoPower);
			break;
		case E_NoOptVoltage:
			audio_out(M_OffNoOptVoltage);
			break;
		case E_WireCut:
			audio_out(M_StoppedWireCut);
			break;
		case E_HighVoltage:
			audio_out(M_OffNoOptVoltage);
			break;
		default:
			break;
	}
}


/* This function implements the Level 0 of the IVRS. Authentication is done in Level 0 */
	
void handle_level0(void)
{
	while(ivrs.level == L0)
	{
		switch(ivrs.state) 
		{
			case S_Welcome:
				audio_out(M_Welcome);
				ivrs.state = S_AskPassword;
				break;
			case S_AskPassword:
				if (authorize())
				{
					ivrs.level = L1;
				}
				else {
					ivrs.state = S_Exit;
				}
				break;
			case S_Exit:
				ivrs.level=LInvalid;
				return;
				break;
			default:
				break;
		}
	}
}

/* This function implements the Level 1 of the IVRS. In level-1, user can do the following operations
 * 1) Motor Start/Stop 2) Device Mode Change 3) Select timer mode and 4) More Services 
 */
	
void handle_level1(void)
{
	dtmf_recvd = dtmf_wait_cnt = pos = 0;

	/** Motor cannot be operated as power is not available **/
	if((hw.motor == M_Stopped) && ((hw.err == E_NoPower) \
				|| (hw.err == E_LowVoltage)||(hw.err == E_HighVoltage) || (hw.err == E_Ph_reverse)))
	{
		while(!dtmf_recvd)
		{
			if(dtmf_wait_cnt == DTMF_WAIT_CNT)
			{
				ivrs.level = LInvalid; 
				return;
			}

			if((g_phase == SINGLE_PHASE)||(g_phase == TWO_PHASE)||(g_phase == HIGH_VOLTAGE))
			{
				(g_phase == SINGLE_PHASE) ? audio_out(M_One) : audio_out(M_Two);
				audio_out(M_NoStartNoOptVoltage);
			}
			else if(hw.err == E_Ph_reverse) //  added for phase reverse
			{
				audio_out(M_Three);
				audio_out(M_Incorrect);
				audio_out(M_NoStartNoOptVoltage);
				/*	if(hw.motor == M_Stopped)
					{
					audio_out(M_MotorStopSuccess);

					}
					*/

			}
			else
				audio_out(M_NoStartNoPower);

			audio_out(M_ModeChange1);
			if(!dtmf_recvd)
			{
				dtmf_wait();
				if(!ongoing_call)
					return;
			}
		}

		on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
		switch(ivrs_pos)
		{
			case ONE:                   /* Press ONE to change the Mode */
				change_mode();
				break ;
			case TWO:                   /* Press TWO for TIMER */
				get_timer_mode_details();
				break;
			case THREE:                /* Press THREE for more services */ 
				ivrs.level=L2;
				break;
			case FOUR:                 /* Press FOUR to go back to main menu */
				break;
			case EIGHT: // send the voltage and current info
				send_volt_curr_info();
				break;
			case STAR:
				ivrs.level=LInvalid;
				break;
			case NINE:        		/* Press NINE to check sim balance */
				check_sim_balance();
				ivrs.level=LInvalid;
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);
				break;
		}
		return;
	}

	if(g_phase == TWO_PHASE)
		audio_out(M_Two);

	/** Motor can be operated as power is available **/
	switch(hw.motor)
	{
		case M_Started:
			while(!dtmf_recvd ){
				if(dtmf_wait_cnt == DTMF_WAIT_CNT){
					ivrs.level = LInvalid; 
					return;
				}
				if(hw.err == E_ManualStart)
				{
					audio_out(M_MotorStartedManually);
					if(dryrun.enable)
						audio_out(M_WaterPumping);
					audio_out(M_ToTurnOffMotor);
					audio_out(M_Press1);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ModeChange);
					audio_out(M_Press2);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ForTimer);
					audio_out(M_Press3);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_MoreService);
					audio_out(M_Press4);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_BackToMainMenu);
					audio_out(M_Press5);
					if(!ongoing_call || pos > 0)
						break;	
				}
				else{
					if(dryrun.enable==1)
						audio_out(M_WaterPumping);
					audio_out(M_MotorOn);
				}
				dtmf_wait();
				if(!ongoing_call)
					return;
			}
			if(!ongoing_call)
				return;	

			on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
			switch(ivrs_pos){
				case ONE:   /*Press ONE to switch OFF the Motor*/
					stop_motor();
					call_motor_off=1; // added newly 
					if(debugging_enable==1)
						debugging_flag = 1;

					if(hw.motor == M_Stopped)
						audio_out(M_MotorStopSuccess);
					/** when in timer mode, 
					 * if motor stopped through mobile, change the mode to manual **/
					if(hw.mode == M_Timer)
					{
						audio_out(M_Manualmode);
						hw.mode	= M_Manual;
						my_eeprom_write_int((unsigned int)hw.mode\
								, (unsigned int*)EEPROM_START_MODE);
					}
					wdt_reset();
					break;
				case TWO:                   /*Press TWO to change the Mode*/
					change_mode();
					break;
				case THREE:                 /*Press THREE for TIMER*/
					get_timer_mode_details();
					break;
				case FOUR:                  /*Press FOUR for more services*/
					ivrs.level=L2;
					break;
				case FIVE:                  /*Press FIVE to go back to main menu*/
					break;
				case EIGHT: // send the voltage and current info
					send_volt_curr_info();
					break;
				case STAR:
					ivrs.level =LInvalid;
					break;
				case NINE:        		/* Press NINE to check sim balance */
					check_sim_balance();
					ivrs.level=LInvalid;
					break;
				default:
					audio_out(M_Incorrect);
					audio_out(M_Reenter);
					break;
			}
			break;
		case M_Stopped:
			while(!dtmf_recvd)
			{
				if(dtmf_wait_cnt == DTMF_WAIT_CNT){
					ivrs.level = LInvalid; 
					return;
				}
				/****** motor stopped manually from starter ******/
				if(hw.err == E_ManualStop)	
				{
					audio_out(M_OffManual);	
					audio_out(M_ToTurnOnMotor);
					audio_out(M_Press1);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ModeChange);
					audio_out(M_Press2);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_ForTimer);
					audio_out(M_Press3);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_MoreService);
					audio_out(M_Press4);
					if(!ongoing_call || pos > 0)
						break;	
					audio_out(M_BackToMainMenu);
					audio_out(M_Press5);
					if(!ongoing_call || pos > 0)
						break;	
				}
				else if((hw.err == E_WireCut)||(hw.err == E_NoWater)||(E_OverCurrent == hw.err)){
					play_unfavourable_msg(hw.err);
					ivrs.level = LInvalid;
					return;
				}
				else{
					audio_out(M_PowerOnMotorOff);
				}
				dtmf_wait();
				if(!ongoing_call)
					return;
			}
			if(!ongoing_call)
				return;

			on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
			switch(ivrs_pos)
			{
				case ONE:                   /*Press ONE to switch ON the Motor*/
					start_motor();
					/******** check for wire cut in 'R' and 'B' phases. 
					 * Motor will not be started if R or B phase wires are cut *******/
					if(debugging_enable==1)
						debugging_flag = 1;

					if(hw.motor == M_Stopped)	
					{
						stop_motor_ct_err(); 	// included newly to stop the starter 
									//when CT are not connected

						audio_out(M_StoppedWireCut);
						prv_status.err = hw.err = E_WireCut;
						ivrs.level = LInvalid;
						return;
					}
					else
					{
						if(dryrun.enable==1)
						{
							audio_out(M_MotorOnWaterCheck);
							dryrun.startup_flg = 1;	
						}

						hw.err = prv_status.err = 0;
						//print_ssd_delay("MTON",0,SHORT_PRINT_DELAY);
						wdt_reset();
					}
					/** when in timer mode, if motor started through mobile, 
					 * change the mode to manual **/
					if(hw.mode == M_Timer)
					{
						audio_out(M_Manualmode);
						hw.mode	= M_Manual;
						my_eeprom_write_int((unsigned int)hw.mode\
								, (unsigned int*)EEPROM_START_MODE);
					}
					break;
				case TWO:                   /*Press TWO to change the Mode*/
					change_mode();
					break;
				case THREE:                 /*Press THREE for TIMER*/
					get_timer_mode_details();
					break;
				case FOUR:                  /*Press FOUR for more services*/
					ivrs.level=L2;
					break;
				case FIVE:                  /*Press FIVE to go back to main menu*/
					break;
				case EIGHT: // send the voltage and current info
					send_volt_curr_info();
					break;
				case STAR:
					ivrs.level=LInvalid;
					break;
				case NINE:        		/* Press NINE to check sim balance */
					check_sim_balance();
					ivrs.level=LInvalid;
					break;
				default:
					audio_out(M_Incorrect);
					audio_out(M_Reenter);
					break;
			}
			break;
		default:
			audio_out(M_Incorrect);
			audio_out(M_Reenter);
			break;
	}

	/***** check whether the call is active or not and return from here if the caller has disconnected *****/	
	if(!check_call_status())
	{
		ivrs.level = LInvalid;
		return;
	}

	hw.cond = check_favourable();
	dryrun.startup_flg = 0;		/** this flag is to wait for water during motor startup **/
}


/* This function implements the Communication Level based on the Farmer input through dtmf
 *      msg_flag = 0 ; comm_level_flag =0 =======> Normal Mode Communication
 *      msg_flag = 0 ; comm_level_flag =1 =======> Unfavourable and Theft Mode Communication
 *      msg_flag = 1 ; comm_level_flag =X =======> Message only Mode Communication
 */
	
void comm_level(void)
{
	unsigned char temp;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;
	ivrs_buf[2]=THREE;

	while(L2 == ivrs.level)
	{
		dtmf_wait_cnt = 0,dtmf_recvd = 0;
		while(!dtmf_recvd)
		{
			if(dtmf_wait_cnt == DTMF_WAIT_CNT)
			{
				ivrs.level = LInvalid; 
				return;
			}
			audio_out(M_CommSettings1);
			dtmf_wait();
			if(!ongoing_call)
				return;
		}

		on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
		switch(ivrs_pos)
		{
			case ONE: 
				msg_flag=0;
				comm_level_flag=1;
				audio_out(M_UnfavourableTheftActive);
				temp=1;
				my_eeprom_write_byte((unsigned char)temp,(unsigned char *)ECMLVL);
				ivrs.level= L1;
				break;
			case TWO: 
				msg_flag=0;
				comm_level_flag=0;
				audio_out(M_NormalComm);
				temp=0;
				my_eeprom_write_byte((unsigned char)temp,(unsigned char *)ECMLVL);
				ivrs.level= L1;
				break;
			case THREE: 
				msg_flag=1;
				comm_level_flag=0;
				temp=2;
				audio_out(M_MessageComm);
				my_eeprom_write_byte((unsigned char)temp,(unsigned char *)ECMLVL);
				ivrs.level= L1;
				break;
			case FOUR:                 /* Press FOUR to go back to main menu */
				ivrs.level = L1;
				break;
			case STAR:
				ivrs.level = LInvalid;
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);
				break;
		}
	}
}

#if 0
unsigned char get_firmware_ph_number()
{
	/*This function gives firmware phone number for sapbr settings and stores it in EEPROM */
	audio_out(M_EnterPhoneNo);
	pos=0;
	stop_timer();
	start_timer(30);
	while(pos<10)
	{
		if(timer_expired==1)
		{
			audio_out(M_NoResponse);
			return 0;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	dtmf_buf[10] = '\0';
	print_ssd_delay("PASS",0,1000);

	wdt_disable();
	eeprom_write_block((const void *)dtmf_buf, ( void *)EEPROM_FIRM_PHONE_NUM,NDIGIT_PH_NUM);
	wdt_enable(WDTO_30MS);
	audio_out(M_PhoneNoChanged);     /*Phone number changed successfully*/
	print_ssd_delay("EXIT",0,500);
	return 1;
}


/*********************************************************************
  This function gives organization phone number and stores it in EEPROM.
 *********************************************************************/

	unsigned char
org_num(void)
{
	unsigned char org_no[12];

	pos=0;
	audio_out(M_EnterPhoneNo);
	if(!ongoing_call)
		return 1;
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	if(timer_expired==1)
		return 1;

	dtmf_buf[10] = '\0';
	strcpy((char *)org_no, (const char *)dtmf_buf);
	pos=0;
	audio_out(M_ReEnterPhoneNo);                    /* Please re enter phone number */
	if(!ongoing_call)
		return 1;
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,pos,50);
	}
	stop_timer();
	if(timer_expired==1)
		return 1;

	dtmf_buf[10] = '\0';

	wdt_reset();
	if(!strcmp((const char *)org_no, (const char *)dtmf_buf))
	{
		//strcpy((char *)admin_ph[0], (const char *)f_no);
		my_eeprom_write((const void *)dtmf_buf, ( void *)EEPROM_ORG_PHONE_NUM,NDIGIT_PH_NUM);
		audio_out(M_PhoneNoChanged);     /*Phone number changed successfully*/
	}
	else {
		audio_out(M_PhoneMisMatch);  /*The numbers did not match successfully*/
		return 1;
	}

	print_ssd_delay("DON1",0,1000);
	return 0;
}


#endif
/*
   unsigned char org_num()
   {

   int i;
   audio_out(M_EnterPhoneNo);
   pos=0;
   stop_timer();
   start_timer(30);
   while(pos<10)
   {
   if(timer_expired==1)
   {
   audio_out(M_NoResponse);
   return 0;
   }
   print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
   }
   stop_timer();
   dtmf_buf[10] = '\0';
   print_ssd_delay("PASS",0,1000);

   my_eeprom_write((const void *)dtmf_buf, ( void *)EEPROM_ORG_PHONE_NUM,NDIGIT_PH_NUM);
   audio_out(M_PhoneNoChanged);     //Phone number changed successfully
   return 1;
   }*/




/* This function allows the user to change the farmer number and stores it in EEPROM */
	
unsigned char far_num_change(void)
{
	unsigned char f_no[11];

	dtmf_buf[0] = '0';
	dtmf_buf[1] = '0';
	dtmf_buf[2] = '0';
	dtmf_buf[3] = '0';
	dtmf_buf[4] = '0';
	dtmf_buf[5] = '0';
	dtmf_buf[6] = '0';
	dtmf_buf[7] = '0';
	dtmf_buf[8] = '0';
	dtmf_buf[9] = '0';
	pos=0;
	audio_out(M_EnterPhoneNo);
	if(!check_call_status())
	{
		ivrs.level=LInvalid;
		return 0;
	}
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			return 0;
		}
		//print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();

	dtmf_buf[10] = '\0';
	strcpy((char *)f_no, (const char *)dtmf_buf);
	pos=0;
	audio_out(M_ReEnterPhoneNo);			/* Please re enter phone number */
	if(!check_call_status())
	{
		ivrs.level=LInvalid;
		return 0;
	}
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			return 0;
		}
		//print_ssd_delay(NULL,pos,50);
	}
	stop_timer();

	dtmf_buf[10] = '\0';
	//	print_ssd_delay("PASS",0,1000);
	wdt_reset();
	if(!strcmp((const char *)f_no, (const char *)dtmf_buf))
	{
		strcpy((char *)ph[0], (const char *)f_no);
		ivrs.level = L1;
		my_eeprom_write((const void *)f_no, ( void *)EEPROM_FRMR_PHONE_NUM,NDIGIT_PH_NUM);
		num_changed=1;
		audio_out(M_PhoneNoChanged);     /*Phone number changed successfully*/
	}
	else {
		audio_out(M_PhoneMisMatch);  /*The numbers did not match successfully*/
		return 0;
	}

	//	print_ssd_delay("DON1",0,1000);
	return 1;
}

/* This function allows the user to change the admin number and stores it in EEPROM */
#if 0
	unsigned char
admin_num_change(void)
{
	unsigned char f_no[12];

	pos=0;
	audio_out(M_EnterPhoneNo);
	check_call_status();
	if(!ongoing_call)
		return 1;
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	if(timer_expired==1)
		return 1;

	dtmf_buf[10] = '\0';
	strcpy((char *)f_no, (const char *)dtmf_buf);
	pos=0;
	audio_out(M_ReEnterPhoneNo);                    /* Please re enter phone number */
	if(!ongoing_call)
		return 1;
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos<10)
	{
		if(timer_expired==1) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,pos,50);
	}
	stop_timer();
	if(timer_expired==1)
		return 1;

	dtmf_buf[10] = '\0';

	wdt_reset();
	if(!strcmp((const char *)f_no, (const char *)dtmf_buf))
	{
		strcpy((char *)admin_ph[0], (const char *)f_no);
		my_eeprom_write((const void *)f_no, ( void *)EEADMIN_PH_NUM,NDIGIT_PH_NUM);
		audio_out(M_PhoneNoChanged);     /*Phone number changed successfully*/
	}
	else {
		audio_out(M_PhoneMisMatch);  /*The numbers did not match successfully*/
		return 1;
	}

	print_ssd_delay("DON1",0,1000);
	return 0;
}

#endif
/******************************************************************************
 * THIS FUNCTION IS USED TO ENABLE OR DISABLE CYCLIC TIMER MODE CONFIGURATIONS.
 * ***************************************************************************/
//*28#
int configure_timer_details(void)
{
	dtmf_recvd=0;
	dtmf_wait_cnt=0;

	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;

	while(!dtmf_recvd)
	{
		if(dtmf_wait_cnt == DTMF_WAIT_CNT)
		{
			ivrs.level = LInvalid;
			return 0;
		}
		audio_out(M_CustomSettings3);
		dtmf_wait();
		if(!ongoing_call)
		{
			ivrs.level = LInvalid;
			return 0;
		}
	}
	on_dtmf_receipt(ivrs_buf,11);
	switch(ivrs_pos)
	{
		case ONE:
			cyclic_timer = 1;
			twice_timer=0;
			my_eeprom_write_byte((unsigned char) (cyclic_timer),(unsigned char  *)EEPROM_CYCLIC_TIMER);
			my_eeprom_write_byte((unsigned char) (twice_timer),(unsigned char  *)EEPROM_TWICE_TIMER);
			audio_out(M_One);
			audio_out(M_Enabled);
			break;
		case TWO:
			cyclic_timer = 1;
			twice_timer=1;
			my_eeprom_write_byte((unsigned char) (cyclic_timer),(unsigned char  *)EEPROM_CYCLIC_TIMER);
			my_eeprom_write_byte((unsigned char) (twice_timer),(unsigned char  *)EEPROM_TWICE_TIMER);
			audio_out(M_Two);
			audio_out(M_Enabled);
			break;
		case THREE:
			cyclic_timer = 0;
			twice_timer = 0;
			my_eeprom_write_byte((unsigned char) (cyclic_timer),(unsigned char  *)EEPROM_CYCLIC_TIMER);
			my_eeprom_write_byte((unsigned char) (twice_timer),(unsigned char  *)EEPROM_TWICE_TIMER);
			audio_out(M_Disabled);
			break;
		default:
			break;
	}
	return 1;
}



/* This function is used to change the serial number of the device from admin access menu */
	
unsigned char ser_num_change(void)
{
	dtmf_buf[0]='\0';
	pos=0;
	audio_out(M_EnterSerialNumber);
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos < NDIGIT_SER_NUM)
	{
		if(timer_expired) {
			audio_out(M_NoResponse);
			break;
		}
		print_ssd_delay(NULL,dtmf_buf[pos-1]-'0',50);
	}
	stop_timer();
	if(timer_expired)
		return 1;

	dtmf_buf[NDIGIT_SER_NUM] = '\0';

	if(pos == NDIGIT_SER_NUM)
	{
		strcpy((char*)serial_number, (const char*)dtmf_buf);
		my_eeprom_write((const void *)serial_number, ( void *)EESER_NUM, NDIGIT_SER_NUM);
		audio_out(M_Serial);     
		audio_out(M_Enabled);     
	}
	else
	{
		audio_out(M_WrongPassword);
	}  
	return 0;
}



/***********************************************************************************************************
 * This function allows the user to enter the security password for firmware upgrade and stores it in EEPROM. 
 ***********************************************************************************************************/

unsigned char firm_passwd(void)
{
	audio_out(M_Enter4DigitPasswd);		      /*Please enter 4 digit password*/
	pos=0;
	start_timer(30);
	while(pos<4) 
	{
		if(timer_expired==1 || (!check_call_status())) 
		{
			audio_out(M_NoResponse);
			return 0;
		}
		//	print_ssd_delay(NULL,pos,50);
	}
	stop_timer();
	dtmf_buf[4] = '\0';

	if(!strcmp((const char *)dtmf_buf,"9696")) 
	{
		my_eeprom_write((const void *)dtmf_buf,(void *)EEPROM_FIRM_PASSWD,4);
		audio_out(M_PasswordChanged);		/* Password changed successfully */
		return 1;
	}
	else 
	{
		audio_out(M_WrongPassword);			/* Wrong password */
		disconnect_call();
	}
	return 0;
}



/********************************************************************************
 * This function allows the user to change the password and stores it in EEPROM. 
 ********************************************************************************/

	 
unsigned char change_passwd(void)
{
	unsigned char pass[5];

	pos=0;
	audio_out(M_EnterPasswd);      /* Enter your old password */
	if(!check_call_status())
	{
		ivrs.level=LInvalid;
		return 0;
	}

	start_timer(WAIT_FOR_DTMF_INPUT);
	while(pos < PASSWD_LEN)
	{
		if(timer_expired==1)
		{
			audio_out(M_NoResponse);
			return 0;
		}    
		//print_ssd_delay(NULL,pos,BLINK_DELAY);
	}    
	stop_timer();

	my_eeprom_read((void *)pass, (const void *)EEPROM_PASSWD_OFFSET, PASSWD_LEN);	
	pass[PASSWD_LEN] = dtmf_buf[PASSWD_LEN] = '\0';

	if(strcmp((const char*)pass, (const char*)dtmf_buf) == 0)
	{			
		pos=0;
		audio_out(M_Enter4DigitPasswd);           /* Enter your new 4 digit password */
		if(!check_call_status())
		{    
			ivrs.level=LInvalid;
			return 0;
		}
		start_timer(WAIT_FOR_DTMF_INPUT);
		while(pos < PASSWD_LEN)
		{
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				return 0;
			}
			//	print_ssd_delay(NULL,pos,BLINK_DELAY);
		}
		stop_timer();

		dtmf_buf[PASSWD_LEN] = '\0';
		strcpy((char *)pass,(const char *)dtmf_buf);
		pos=0;
		audio_out(M_ReEnterPasswd);         /* Please re-enter your password to confirm */
		if(!check_call_status())
		{    
			ivrs.level=LInvalid;
			return 0;
		}
		start_timer(WAIT_FOR_DTMF_INPUT);
		while(pos < PASSWD_LEN)
		{
			if(timer_expired==1)
			{
				audio_out(M_NoResponse);
				return 0;
			}
			//	print_ssd_delay(NULL,pos,BLINK_DELAY);
		}
		stop_timer();

		dtmf_buf[PASSWD_LEN] = '\0';
		if(!strcmp((const char *)pass, (const char *)dtmf_buf)) 
		{
			strcpy((char *)hw.passwd,(const char*)pass);
			my_eeprom_write((const void *)hw.passwd, (void *)EEPROM_PASSWD_OFFSET, PASSWD_LEN);
			audio_out(M_PasswordChanged);       /* Password changed successfully */
			ivrs.level=L1;
		}
		else
		{
			audio_out(M_WrongPassword);         /* Wrong password */
			return 0;
		}
	}
	else
	{
		audio_out(M_WrongPassword);         /* Wrong password */
		return 0;
	}

	return 1;
}


/*This function implements the Level 2 of the IVRS. i) Communication level 
  settings change ii) Farmer number change iii) Custom Settings are done 
  in Level 3
  */
	
void handle_level2(void)
{
	while(ivrs.level == L2) 
	{
		dtmf_wait_cnt = 0,dtmf_recvd = 0;

		ivrs_buf[0]=ONE;
		ivrs_buf[1]=TWO;
		ivrs_buf[2]=THREE;
		ivrs_buf[3]=FOUR;

		while(!dtmf_recvd){
			if(dtmf_wait_cnt == DTMF_WAIT_CNT){
				ivrs.level = LInvalid; 
				return;
			}
			audio_out(M_MoreService3);
			dtmf_wait();
			if(!ongoing_call)
				return;
		}

		on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
		switch(ivrs_pos)
		{
			case ONE: 
				comm_level();
				break;
			case TWO: 
				far_num_change();
				break;
			case THREE: 
				ivrs.level= L3;
				break;
			case FOUR:
				ivrs.level = L1;
				break;
			case STAR:
				ivrs.level =LInvalid;
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);
				break;
		}
	}
}


/*This function implements the Level 3 of the IVRS. i) Theft Enable/Disable 
  ii) Farmer password change are done in Level 3 */
	
void handle_level3(void)
{
	while(ivrs.level == L3) 
	{
		dtmf_recvd=dtmf_wait_cnt=0;

		ivrs_buf[0]=ONE;
		ivrs_buf[1]=TWO;

		while(!dtmf_recvd)
		{
			if(dtmf_wait_cnt == DTMF_WAIT_CNT)
			{
				ivrs.level = LInvalid; 
				return;
			}
			audio_out(M_CustomSettings3);
			dtmf_wait();
			if(!ongoing_call)
				return;
		}

		on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
		switch(ivrs_pos)
		{
			case ONE:                   
				while(ivrs.level == L3)
				{
					dtmf_recvd=dtmf_wait_cnt=0;
					while(!dtmf_recvd)
					{
						if(dtmf_wait_cnt == DTMF_WAIT_CNT){
							ivrs.level = LInvalid; 
							return;
						}
						if(theft_enable)
							audio_out(M_DisableTheft1);
						else
							audio_out(M_EnableTheft1);
						dtmf_wait();
						if(!ongoing_call)
							return;
					}
					on_dtmf_receipt(ivrs_buf, MAX_DTMF_TONES);
					switch(ivrs_pos)
					{
						case ONE:
							if(theft_enable)
							{
								theft_enable=0;
								my_eeprom_write_byte( (unsigned char)theft_enable\
										,(unsigned char*)ETHEFT);
								ivrs.level=LInvalid;
								audio_out(M_TheftDisabled);
							}
							else
							{
								theft_enable=1;
								my_eeprom_write_byte( (unsigned char)theft_enable\
										,(unsigned char*)ETHEFT);
								ivrs.level=LInvalid;
								audio_out(M_TheftEnabled);
							}
							break;
						case TWO:
							ivrs.level = L1;
							break;
						case STAR:
							ivrs.level =LInvalid;
							break;
						default:
							audio_out(M_Incorrect);
							audio_out(M_Reenter);
							break;
					}
				}
				break ;
			case TWO:                   /* Press TWO for changing passwd */
				change_passwd();
				break;
			case THREE:
				ivrs.level = L1;
				break;
			case STAR:
				ivrs.level = LInvalid;
				break;
			default:
				audio_out(M_Incorrect);
				audio_out(M_Reenter);
				break;
		}
	}
}



/* This function will send AT command to GSM to answer the call */
	 
void take_call(void)
{
	unsigned char a=0,i=0;
	ri=0;
	wdt_reset();
	check_number();
	recv_response(WAIT_PENDING_URC);
	send_to_gsm_string("ATA");
	a=recv_response(WAIT_GSM_RESPONSE);
	for(i=0;i<4;i++)
	{
		if(0 == strcmp((const char*)response,"OK"))
		{
			break;
		}
		else
		{
			send_to_gsm_string("ATA");
			recv_response(WAIT_GSM_RESPONSE);
			Delay_sec(1);
		}
	}
	if(!a && strcmp((const char*)response,"OK"))
		print_ssd_delay((char*)response,0,PRINT_DELAY);
}

/* This function will send AT command to GSM to disconnect the call */
	 
void disconnect_call(void)
{
	wdt_reset();
	unsigned char i=0;
	call_status = 0;
	send_to_gsm_string("ATH");
	recv_response(WAIT_GSM_RESPONSE);
	for(i=0;i<4;i++)
	{
		if(0 == strcmp((const char*)response,"OK"))
		{
			break;
		}
		else
		{
			send_to_gsm_string("ATH");
			recv_response(WAIT_GSM_RESPONSE);
			Delay_sec(1);
		}
	}
	return ;
}


/**********************************************************************************************
 * This function acts as a wrapper function for the different levels (Level 0,1,2,3) of IVRS
 **********************************************************************************************/
	
void start_ivrs(void)
{
	unsigned char a;
	wdt_reset();

	if(call_status) 
	{
		ivrs.level = st.level;
		call_status = 0;
		if(debugging_enable==1)
		{
			debugging_flag = 1;
			kr_call=1;
		}
	}	
	else
	{
		do
		{
			a = recv_response(100);
		}  while (strcmp((const char*)response,"RING") && !a);
		print_ssd_delay((char*)response,0,100);
		if(strcmp((const char*)response,"RING")){
			ri =0;
			sms_ri = 1;
			return;
		}

		take_call();
		ivrs.level=L0;
	}	
	ivrs_buf[0]=ONE;
	ivrs_buf[1]=TWO;
	ivrs_buf[2]=THREE;
	ivrs_buf[3]=FOUR;
	ivrs_buf[4]=FIVE;
	ivrs_buf[5]=SIX;
	ivrs_buf[6]=SEVEN;
	ivrs_buf[7]=EIGHT;
	ivrs_buf[8]=NINE;
	ivrs_buf[10]=STAR;
	ivrs_buf[11]=HASH;

	for(;;) 
	{	

		switch (ivrs.level) 
		{
			case L0:
				ivrs.state = S_Welcome;
				handle_level0();
				break;

			case L1:
				handle_level1();
				break;

			case L2:
				handle_level2();
				break;

			case L3:
				handle_level3();
				break;

			case LContinue:
				return;
				break;

			case LInvalid:
				audio_out(M_ThankUKisanRaja);
				disconnect_call();
				prv_status.cond = hw.cond;
				prv_status.motor = hw.motor;
				hw.err=0;
				return;
		}

		if(!check_call_status()){
			ivrs.level=LInvalid;
		}

	}
}


/*
 * Description:- This function is to get prepaid account sim balance information 
 * from the network and SMS it to the user.
 *
 * Unstructured Supplementary Service Data(USSD) is a protocol 
 * used by GSM cellular telephones to communicate with the service provider's computers.
 *
 * AT command to check balance on uninor sim: "AT+CUSD=1,\"*222*2#\",15"
 *
 * Possible responses from GSM for the above command:- "OK", "ERROR", "+CME ERROR: <err>" 
 * sample unsolicited result code:- +CUSD: 2,"Balance 0.44 valid till 17/06/2012 U 
 * to U loc sec 108206 All India SMS 85",15
 */

void check_sim_balance(void)
{
	audio_out(M_Enter4DigitPasswd);     
	/* Substitute audio for "enter your USSD code" is "enter your four digit password" */
	pos = dtmf_recvd = 0;

	start_timer(WAIT_FOR_DTMF_INPUT);
	while((!hash_flg) && (!timer_expired))
		;
	stop_timer();
	if(timer_expired==1){	
		audio_out(M_NoResponse);
		return;
	}

	EIMSK &= ~(1 << INT0);				/* Disable DTMF interrupt */
	if(hash_flg)			
		/* 'hash_flg' variable will be set to one in the interrupt routine whenever user presses a # 
		 * key on his phone */
	{
		dtmf_buf[pos] = '\0';            /* terminate the USSD code received from the user with '\0' */
		disconnect_call();
		recv_response(WAIT_GSM_RESPONSE);
		Delay_sec(GSM_DELAY);		/* delay between two successive AT commands */

		send_serial_string(BEGIN_SIM_BALANCE_CMD);
		send_serial_string((char *)dtmf_buf);
		send_to_gsm_string(END_SIM_BALANCE_CMD);   

		recv_response(WAIT_GSM_RESPONSE);
		/*		int i;
				for(i=0; (response[i]!='\0'); i++) 
				print_ssd_delay(response+i,0,PRINT_DELAY);
				*/

		recv_sim_balance(WAIT_GSM_URC);              
		/* receives sim balance information from gsm into a global array "response[]" */

		strncpy((char *)msg_info, (const char *)response, MAX_SMS_LEN-1);
		msg_info[MAX_SMS_LEN-1] = '\0';
		if(msg_info[0] != '\0')
		{
			Delay_sec(LONG_GSM_DELAY);
			send_sms(caller_number, msg_info);        
			Delay_sec(LONG_GSM_DELAY);
		}
	}

	hash_flg=0;
	EIMSK |= (1 << INT0);			/* Enable DTMF interrupt */
}

#if 1
void recv_sms(void)
{
	wdt_reset();
	unsigned int num_index=0;
	int i=0,j=0;       
	unsigned char bufr[20];
	unsigned char msg_index[15]={0};

	//print_ssd_delay("SMS ",0,500);
	sms_ri = 0;
	if((check_msg_number()) == 0)
		return;

	send_to_gsm_string("AT+CMGR=1,1");
	recv_sim_balance(100);

	while(1)
	{
		if (response[i]=='\n')
		{
			i++;
			while(1)
			{
				if(response[i]!='\n' && response[i]!='\r')
				{
					bufr[j++]=response[i];
					bufr[j]='\0';
				}
				else if(response[i]=='\n')
				{
					break;
				}
				i++;
				if(i > RES_LEN)
					break; 
			}
			break;
		} 
		i++;
		if(i > RES_LEN)
			break;
	}

	delete_all_sms();

	if(strncmp((const char *)num_bufr, (const char *)ph[0], NDIGIT_PH_NUM) == 0)	
	{
		num_index = atoi((const char *)bufr);		// convert the string into an integer 
		sms_operations(num_index);
	}
	else if	((bufr[0] == ASCII_STAR)&&(bufr[5] == ASCII_HASH)) //ex *69*0#
	{
		strncpy((char*)msg_index,(const char*) bufr+1, 2);	// copy the password
		if((strcmp((const char*)msg_index,"69"))==0)
		{
		        sms_admin = 1;	
			num_index = atoi((const char *)bufr+4); // copy 2 digit admin access index number 
			//sms_admin_operations(num_index);
			sms_operations(num_index);
			sms_admin = 0;
		} 
	}

	i=0;
	for(i=0;i<MAX_MSG_LEN;i++)
	{
		bufr[i]='F';
		if(i < MAX_NUM_LEN)
		{ 
			num_bufr[i]='F';
		}
	}

	return;
}

void sms_operations(unsigned char num_index)
{
	wdt_reset();
	int menu_index=0;
	switch(num_index)
	{               
		case 0:
			stop_motor();
			if(M_Stopped == hw.motor)
				strcpy((char*)msg_info, SMS_MOTOR_OFF);
			/*
			   else
			   strcpy((char*)msg_info, "MOTR ON");*/
			prv_status.cond = hw.cond;
			prv_status.motor = hw.motor;
			hw.err =0;
			break;
		case 1:
			hw.cond=check_favourable();
			if(hw.cond == C_UnFavourable)
				strcpy((char*)msg_info, SMS_NO_POW);
			else
			{
				start_motor();
				if(M_Started == hw.motor)
					strcpy((char*)msg_info, START_MOTOR);
				/*else
				  strcpy((char*)msg_info, "MOTR OF");*/
				prv_status.cond = hw.cond;
				prv_status.motor = hw.motor;
				hw.err=prv_status.err =0;
			}

			break;
		case 2:       /** change to manual mode **/
			hw.mode=M_Manual;
			strcpy((char*)msg_info, SMS_MANUAL_MODE);
			break;
		case 3:       /** change to auto mode **/
			hw.mode=M_Auto;
			strcpy((char*)msg_info, SMS_AUTO_MODE);
			break;
		case 4: // remote factory rest
			if(sms_admin == 1)
			{
				remote_frst();	
				_delay_ms(3000);
			}
			break;
		case 5: // re boot the GSM module
			if(sms_admin == 1)
			{
				recv_response(50);
				Delay_sec(2);
				switch_off_gsm();
				menu_index=1;
			}
			break;
		case 7:		//to set caller number as farmer number 
			if(sms_admin == 1)
			{
				my_eeprom_write((const void *)num_bufr, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);	
				strcpy((char *)ph[0], (const char *)num_bufr);
				num_changed=1;
				strcpy((char*)msg_info, SMS_F_NO_CHANGE);
			}
			break;
		case 8:  
			if(sms_admin == 1)
			{		
				//print_ssd_delay("REST",0,500);
				_delay_ms(3000);
			}	
			break;
		default:
			menu_index = 1;
			break;
	}


	if(menu_index != 1)	
	{
		send_sms(num_bufr, msg_info);
	}

}


/***********************************************
  THIS FUNCTION IS USED TO PERFORM THE OPERATIONS.
 ***********************************************/

#if 0
void sms_admin_operations(unsigned char num_index)
{
	wdt_reset();
	int menu_index=0;
	switch(num_index)
	{               
		case 0:
			stop_motor();
			if(M_Stopped == hw.motor)
				strcpy((char*)msg_info, "MOTR OF");
			/*
			   else
			   strcpy((char*)msg_info, "MOTR ON");*/
			prv_status.cond = hw.cond;
			prv_status.motor = hw.motor;
			hw.err =0;
			break;
		case 1:
			hw.cond=check_favourable();
			if(hw.cond == C_UnFavourable)
				strcpy((char*)msg_info, "NO POWR");
			else
			{
				start_motor();
				if(M_Started == hw.motor)
					strcpy((char*)msg_info, "MOTR ON");
				/*else
				  strcpy((char*)msg_info, "MOTR OF");*/
				prv_status.cond = hw.cond;
				prv_status.motor = hw.motor;
				hw.err=prv_status.err =0;
			}

			break;
		case 2:       /** change to manual mode **/
			hw.mode=M_Manual;
			strcpy((char*)msg_info, "MANL");
			break;
		case 3:       /** change to auto mode **/
			hw.mode=M_Auto;
			strcpy((char*)msg_info, "AUTO");
			break;
		case 4: // remote factory rest
			remote_frst();	
			_delay_ms(3000);
			break;
		case 5: // re boot the GSM module
			recv_response(50);
			Delay_sec(2);
			switch_off_gsm();
			menu_index=1;
			break;
		case 7:		//to set caller number as farmer number 
			my_eeprom_write((const void *)num_bufr, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);	
			strcpy((char *)ph[0], (const char *)num_bufr);
			num_changed=1;
			strcpy((char*)msg_info, "FNUM");
			break;
		case 8:                 
			//print_ssd_delay("REST",0,500);
			_delay_ms(3000);	
			break;
		default:
			menu_index = 1;
			break;
	}


	if(menu_index != 1)	
	{
		send_sms(num_bufr, msg_info);
	}
}


/***********************************************
  THIS FUNCTION IS USED TO PERFORM THE OPERATIONS.
 ***********************************************/

void sms_operations(unsigned char num_index)
{
	wdt_reset();
	int menu_index=0;
	switch(num_index)
	{               
		case 0:
			stop_motor();
			if(M_Stopped == hw.motor)
				strcpy((char*)msg_info, "MOTR OF");
			/*else
			  strcpy((char*)msg_info, "MOTR ON");*/
			prv_status.cond = hw.cond;
			prv_status.motor = hw.motor;
			hw.err=0;
			break;
		case 1:
			hw.cond=check_favourable();
			if(hw.cond == C_UnFavourable)
				strcpy((char*)msg_info, "NO POWR");
			else
			{
				start_motor();
				if(M_Started == hw.motor)
					strcpy((char*)msg_info, "MOTR ON");
				/*else
				  strcpy((char*)msg_info, "MOTR OF");*/
				prv_status.cond = hw.cond;
				prv_status.motor = hw.motor;
				hw.err=prv_status.err =0;
			}
			break;
		case 2:       /** change to manual mode **/
			hw.mode=M_Manual;
			strcpy((char*)msg_info, "MANL");
			break;
		case 3:       /** change to auto mode **/
			hw.mode=M_Auto;
			strcpy((char*)msg_info, "AUTO");
			break;	

		default:
			menu_index = 1;
			break;
	}


	if(menu_index != 1)	
	{
		send_sms(ph[0], msg_info);
		wdt_reset();
	}
}
#endif

void delete_all_sms(void)
{
	wdt_reset();
	send_to_gsm_string("AT+CMGDA=\"DEL ALL\"");
	recv_response(WAIT_GSM_RESPONSE);
}

char check_msg_number(void)
{
	int i=0,j=0;

	send_to_gsm_string("AT+CMGR=1,1");		// AT command to read sms.
	recv_response(50);
	if((strcmp((const char*)response,"OK") == 0))
	{
		//print_ssd_delay("NOIS",0,500);
		return 0;
	}
	else
	{
		Delay_sec(2);
		recv_response(50);
		send_to_gsm_string("AT+CMGR=1,1");		// AT command to read sms.
		recv_sim_balance(100);
	}

	while(response[i]!='1' && (i < RES_LEN))
	{
		i++;
	}

	i++;
	while((j<=9) && (response[i]!='\0'))	// logic to filter phone number.
	{
		num_bufr[j++]=response[i++];
	}
	num_bufr[j]='\0'; 
	Delay_sec(1);
	return 1;
}
#endif


#if AUTOMATE
char automate_serial_numbr_rtc(void)
{

	if(serial_automate()==1)
	{
		if(gsm_time_automate() ==1)
		{
			if(automate_activate() == 1)
			{
				send_to_gsm_string("AT+VTS=*");
				Delay_sec(GSM_DELAY);
			}
		}
		else
		{
			while(1)
			{
				while_delay();
				print_ssd_delay("\rRTC_ERR",0,500);
			}
		}
	}
	else
	{
		while(1)
		{
			while_delay();
			print_ssd_delay("\rSER_ERR",0,500);
		}
	}
	wdt_reset();
	return 1;
}

unsigned char serial_automate(void)
{
	unsigned char buf[20];
	int j=0;

	print_ssd_delay("\n\rSER-WAIT60s",0,500);
	for(j=0;j<=13;j++)
	{
		//serial_number[j] = 0XFF; // delete this.
		buf[j]= 0xff;
	}
	wdt_reset();
	send_to_gsm_string("AT+VTS=*");
	//my_eeprom_write((const void *)serial_number, ( void *)EESER_NUM, NDIGIT_SER_NUM);
	my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
	if((strncmp((const char *)serial_number, (const char *)buf, NDIGIT_SER_NUM)) == 0)
	{
		wdt_reset();
		send_to_gsm_string("AT+VTS=0");
		_delay_ms(100);	
		wdt_reset();

		confirm_acknowledge();

		send_to_gsm_string("AT+VTS=*");
		Delay_sec(GSM_DELAY);

		pos=0;
		start_timer(LONG_WAIT_FOR_DTMF_INPUT);
		while(pos < 1)
		{
			if(timer_expired==1)
				break;
		}
		stop_timer();
		dtmf_buf[1]='\0';
		if(!strcmp((const char *)dtmf_buf,"1"))
		{
			print_ssd_delay("\n\rSER-NUM",0,500);
			strcpy((char*)serial_number, (const char*)SER_BATCH_NUM);
			strcat((char*)serial_number,(char*)temp_buff);
			my_eeprom_write((const void *)serial_number, ( void *)EESER_NUM, NDIGIT_SER_NUM);
			Delay_sec(1);
			my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
			send_serial0(' ');
			print_ssd_delay((char*)serial_number,0,500);
			wdt_reset();
			send_to_gsm_string("AT+VTS=1");
			_delay_ms(100);	
			wdt_reset();
			Delay_sec(3);
			return 1;
		}
		else if(!strcmp((const char *)dtmf_buf,"6"))
		{
			print_ssd_delay("\n\r",0,500);
			while(1)
			{
				while_delay();
				print_ssd_delay("\rDTMF-ERR",0,500);
			}
		}
		else if(timer_expired ==1)
		{
			print_ssd_delay("\n\rERR",0,500);
		}
		stop_timer();

		Delay_sec(GSM_DELAY);
		if(timer_expired==1)
		{
			disconnect_call();
		}
		return 0;
	}
	else
	{
		wdt_reset();
		send_to_gsm_string("AT+VTS=1");
		_delay_ms(100);	
		wdt_reset();
		Delay_sec(3);
		print_ssd_delay("\n\rSER-NUM",0,500);
		my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
		send_serial0(' ');
		print_ssd_delay((char*)serial_number,0,500);
		send_serial0('\n');
		send_serial0('\r');
		Delay_sec(GSM_DELAY);
		return 1;
	}
	wdt_reset();
}


char gsm_time_automate(void)
{
	print_ssd_delay("\n\rRTC-WAIT60s",0,500);

	/* Do not do anything till master device gets RTC from gsm */
	pos=0;
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos < 1)
	{
		if(timer_expired==1)
			break;	
	}
	stop_timer();
	send_to_gsm_string("AT+VTS=*");
	confirm_acknowledge();
	pos=0;
	send_to_gsm_string("AT+VTS=*");
	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos < 1)
	{
		if(timer_expired==1)
			break;
	}
	stop_timer();

	dtmf_buf[1]='\0';
	if(!strcmp((const char *)dtmf_buf,"1"))
	{
		set_clock((char*)temp_buff);
		Delay_sec(LONG_GSM_DELAY);
		get_gsm_time();
		pos=0;
		send_to_gsm_string("AT+VTS=1");
		wdt_reset();
		_delay_ms(100);	
		wdt_reset();
		wdt_reset();
		return 1;
	}
	else if(!strcmp((const char *)dtmf_buf,"6"))
	{
		print_ssd_delay("\n\r",0,500);
		while(1)
		{
			while_delay();
			print_ssd_delay("\rDTMF-ERR",0,500);
		}
	}
	else if(timer_expired ==1)
	{
		print_ssd_delay("\n\rERR",0,500);
		disconnect_call();
	}
	wdt_reset();
	return 0;
}

void confirm_acknowledge(void)
{
	int j=0;

	pos=0;
	wdt_reset();
	start_timer(30);
	while(pos < 4)
	{
		if(timer_expired==1)
			break;
	}
	dtmf_buf[4]='\0';
	stop_timer();
	strncpy((char*)temp_buff,(const char*)dtmf_buf,4);
	temp_buff[4]='\0';

	send_to_gsm_string("AT+VTS=*");

	for(j=0;j<4;j++)
	{
		send_serial_string("AT+VTS=\0");
		wdt_reset();
		_delay_ms(50);
		wdt_reset();
		send_serial(temp_buff[j]);
		wdt_reset();
		_delay_ms(50);
		wdt_reset();
		send_serial('\r');
		send_serial('\n');
	}
	wdt_reset();
}

char automate_activate(void)
{
	unsigned char temp;
	print_ssd_delay("\n\rACT-WAIT",0,500);
	wdt_reset();

	start_timer(LONG_WAIT_FOR_DTMF_INPUT);
	while(pos < 4)
	{
		if(timer_expired==1)
			break;
	}
	stop_timer();
	dtmf_buf[4]='\0';
	if(!strcmp((const char *)dtmf_buf,"1990"))
	{
		/** THIS PART INVOLVES AUTOMATION CODE ACTIVATION **/
		automation_code =1;
		my_eeprom_write_byte((unsigned char)automation_code, (unsigned char *)AUTOMATION_CODE);
		/** STORE CALLER NUMBR AS FARMER NUMBR **/
		my_eeprom_write((const void *)caller_number, (void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);
		/* Read Farmer phone number from EEPROM */
		Delay_sec(1);
		my_eeprom_read((void *)ph[0],(const void *)EEPROM_FRMR_PHONE_NUM, NDIGIT_PH_NUM);
		ph[0][10] = '\0';
		temp = 'F';  /** ENABLE CALIBRATION **/
		my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
		temp =1;     /** SET DEFAULT TIME AS 1-MINUTE **/
		my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EECALIB_FLAG);
		Delay_sec(1);
		calib_flag = my_eeprom_read_byte((unsigned char *)EECALIB_FLAG);
		//print_ssd_delay("\n\rEXIT\n\r",0,500);	
		disconnect_call();		
		wdt_reset();
		return 1;
	}
	else
	{
		print_ssd_delay("\n\r",0,500);
		while(1)
		{
			print_ssd_delay("\rACT-FAIL",0,500);
		}
	}
	print_ssd_delay("\n\r",0,500);
	wdt_reset();
	return 0;
}


void production_test_fun(void)
{
	char j=0;

	display_parameters();
	motor_calibration();
	if(thft__byps_press ==0)
	{
		send_serial0('\r');
		for(j=0;j<50;j++)
		{
			send_serial0(' ');
		}
		wdt_reset();
		_delay_ms(400);
		wdt_reset();
		print_ssd_delay("\rPRES BYPS SWITCH",0,500);
		wdt_reset();
		_delay_ms(400);
		wdt_reset();
	}
	if(thft__byps_press ==1)
	{
		send_serial0('\r');
		for(j=0;j<50;j++)
		{
			send_serial0(' ');
		}
		wdt_reset();
		_delay_ms(400);
		wdt_reset();
		print_ssd_delay("\rREMOV_THFT_JMPR",0,500);
		wdt_reset();
		_delay_ms(400);
		wdt_reset();
	}
	check_sig_reg();
	get_ser_gsm();
	check_bypass_and_theft();
}


void get_password(void)
{
	int len =0,i=0,j=0;
	unsigned char temp_buf[4]={0};
	wdt_reset();
	if(pswd_rx ==1)
	{
		print_ssd_delay("\n\r",0,500);
		pswd_rx =0;
		len = strlen((const char*) data_buf);
		if((!strcmp((const char *)data_buf,"6969")) && (len ==4))
		{
			print_ssd_delay("PSWD_OK\n\r",0,500);
			password_set =1;
		}
		else if((!strcmp((const char *)data_buf,"01")) && (len ==2)) // check the start relay
		{

			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(5);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);

		}
		else if((!strcmp((const char *)data_buf,"02")) && (len ==2)) // check the stop relay
		{
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
			Delay_sec(2);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);

		}
		else if((!strcmp((const char *)data_buf,"03")) && (len ==2))
		{
			automation_code = 1;
			display_device_vals();
			automation_code = my_eeprom_read_byte((unsigned char *)AUTOMATION_CODE);
		}	
		else if((!strncmp((const char *)data_buf,"04",2)) && (len ==5))
		{
			for(i=2;i<5;i++)
			{
				temp_buf[j++]=data_buf[i];

			}
			temp_buf[3]='\0';	
			max_ct_val = atoi((const char *)temp_buf);
			my_eeprom_write_int((unsigned int) max_ct_val, (unsigned int *)MAX_CT_VAL_AUTOMATION);
			automation_code = 1;
			print_ssd_delay("M_CT_VAL\n\r",0,500);
			print_ssd_delay(NULL,max_ct_val,500);
			print_ssd_delay("\n\r",0,500);
			automation_code = my_eeprom_read_byte((unsigned char *)AUTOMATION_CODE);
		}
		else
		{
			print_ssd_delay("PSWD_ERR\n\r",0,500);
		}
	}
	wdt_reset();
}


void get_ser_gsm(void)
{
	int len =0;
	wdt_reset();
	send_serial0('\r');
	if(recieved ==1)
	{
		recieved=0;
		print_ssd_delay("\n\rSER-",0,500);
		len = strlen((const char*) data_buf);
		if((isdigit(data_buf[0])) && (len ==4))
		{
			strcpy((char*)serial_number, (const char*)SER_BATCH_NUM);
			strcat((char*)serial_number,(char*)data_buf);
			my_eeprom_write((const void *)serial_number, ( void *)EESER_NUM, NDIGIT_SER_NUM);
			Delay_sec(1);
			my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
			send_serial0(' ');
			print_ssd_delay((char*)serial_number,0,500);
			send_serial0('\n');
			send_serial0('\r');
		}
		else
		{
			print_ssd_delay("ERR",0,500);
		}
	}
	if(time_received ==1)
	{
		time_received =0;
		len = strlen((const char*) data_buf);
		if((isdigit(data_buf[0])) && (len ==4))
		{
			print_ssd_delay("\n\rWAIT 15SEC\n\r",0,500);
			set_clock((char*)data_buf);
			Delay_sec(1);
			get_gsm_time();
			send_serial0('\n');
			send_serial0('\r');
		}
		else
		{
			print_ssd_delay("\n\rRTC_ERR",0,500);
		}
	}
	wdt_reset();
}


void get_gsm_time(void)
{
	char buff[15]={0};

	buff[0] = '\0';
	get_clock((unsigned char *)buff); 
	Delay_sec(1);
	print_ssd_delay("\n\rRTC",0,500);
	send_serial0(' ');
	print_ssd_delay(buff,0,500);
}

void call_master_device(void)
{
	unsigned char master_num[]=MASTER_NUMBR;

	wdt_reset();
	if((automation_done ==0) && (password_set ==1))
	{
		if(master_call ==0)
		{
			master_call =1;
			strcpy((char*)ph,(const char*)master_num);
			notify_sms_enable =0;
			if(notify() == 0)
			{
				print_ssd_delay("\n\r",0,500);
				while(1)
				{
					while_delay();
					print_ssd_delay("\rCAL-ERR",0,500);
				}
				notify_sms_enable =1;
			}
			notify_sms_enable =1;
			print_ssd_delay("\n\r",0,500);
		}	
	}
	else
	{
		wdt_reset();
		check_sig_reg(); /* switch off the GSM module if signal is less than 5 or simcard is not registered */
		wdt_reset();
	}
}

#if AUTOMATE
void while_delay(void)
{
	int i=0;
	blink_auto_led();
	send_serial0('\r');
	for(i=0;i<=20;i++)
	{
		send_serial0(' ');
	}
	wdt_reset();
	_delay_ms(100);
	wdt_reset();
}
#endif

void display_parameters(void)
{
	unsigned char buf[15];
	float temp_float_val=0;
	unsigned char temp=0;
	static char i=0;	
	char buff[2]={0};
	char j=0;
	unsigned char boot_prog_info =0;

	if(i==0)
	{
		SET_PIN(LED_CTRL_PORT,AUTO_LED);
		SET_PIN(LED_CTRL_PORT,FAV_LED);

		print_ssd_delay("\n\rAUTOMATION ACT\n\r",0,500);
		for(j=0;j<50;j++)
		{
			send_serial0('*');
		}
		my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
		print_ssd_delay("\n\rSER_NUM",0,500);
		print_ssd_delay((char*)serial_number,0,500);
		send_serial0('\n');
		send_serial0('\r');

		boot_prog_info = my_eeprom_read_byte((unsigned char *)EEBOOT_PROG_INFO);
		if(boot_prog_info ==1)
		{
			print_ssd_delay("BOOT VER-",0,500);
			boot_ver = my_eeprom_read_int((unsigned int *)EEBOOT_VERSION);
			itoa(boot_ver, buff, 10);
			print_ssd_delay(buff,0,500);
		}
		else
		{
			print_ssd_delay("NO BOOT PROG",0,500);
		}
		print_ssd_delay("\n\rAPP-VER",0,500);
		print_ssd_delay(VERSION,0,500);

		get_gsm_time();
		print_ssd_delay("\n\rFNUM",0,500);
		print_ssd_delay((char*)ph,0,500);
		print_ssd_delay("\n\rSIG",0,500);
		signal_strength(buff);	
		print_ssd_delay((char*)buff,0,PRINT_DELAY);
		send_serial0('\n');
		send_serial0('\r');

		print_ssd_delay("BAT",0,500);
		temp_float_val = ((float)adc_conv(BATTERY_VOLTAGE_PIN) / MAX_ADC_OUTPUT) \
				 * ADC_VREF * BATTERY_ADC_MULTIPLY_FACTOR;
		convert_int_to_str((temp_float_val*TWO_DIGITS_AFTER_DECIMAL_POINT), buf);
		buf[4] = buf[3];
		buf[3] = buf[2];
		buf[2] = buf[1];
		buf[1] = '.';
		print_ssd_delay((char*)buf,0,500);
		send_serial0('\n');
		send_serial0('\r');

		strncpy((char*)batry1,(const char*)buf,5);

		temp = 'F';  /** ENABLE CALIBRATION **/
		my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EEFTB);
		temp =1;     /** SET DEFAULT TIME AS 1-MINUTE **/
		my_eeprom_write_byte((unsigned char) temp, (unsigned char  *)EECALIB_FLAG);
		Delay_sec(1);
		calib_flag = my_eeprom_read_byte((unsigned char *)EECALIB_FLAG);
		i=2;
	}
}
#endif
