/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

/*Initialize the ADC registers */

void init_adc(void)
{
	ADCSRA &= ~(1 << ADEN);    /* Disable ADC */
	ADMUX = 0XE0;   	   /* selected internal 2.56V Voltage Reference with external
				      capacitor at AREF pin. Result should be left justified */
	DDRF = 0x00;		   /* configure adc pins PF0 to PF7 as input pins */
	ADCSRA = 0X06;             /* Enable ADC and set prescalar to 64 */
}	


/* Read the analog voltage and convert it to digital and return the result */ 
unsigned int adc_conv(unsigned char i)
{
	unsigned char low_byte=0, samples=0,k=0;
	unsigned int value=0, final_res=0;
	unsigned int max = 0;

	/* Select the channel */
	wdt_reset();
	switch(i)		
	{
		case VOLTAGE_PHASE_B:			/* B-phase voltage */
			ADMUX = 0xE0;
			break;

		case VOLTAGE_PHASE_Y:			/* Y-phase voltage */
			ADMUX = 0xE1;
			break;

		case VOLTAGE_PHASE_R:			/* R-phase voltage */
			ADMUX = 0xE2;
			break;

		case CURRENT_PHASE_R:			/* CT-1,  R-phase current */
			ADMUX = 0xE3;
			break;

		case CURRENT_PHASE_Y:			/* CT-2,  Y-phase current */
			ADMUX = 0xE4;
			break;

		case CURRENT_PHASE_B:			/* CT-3,  B-phase current */
			ADMUX = 0xE5;
			break;

		case SMPS_OUTPUT_PIN:			/* SMPS output voltage */
			ADMUX =0xE6;
			break;

		case BATTERY_VOLTAGE_PIN:		/* Battery voltage */
			ADMUX =0xE7;
			break;
	}

	/* For phase reversal boxes, Filtering capacitos of resistor network are removed  
	 * RYB output will not be constant it will be half wave sine output,
	 * hence take the maximum value and return it.
	 * If phase reversal option is not enabled, or phase reversal box is not used then 
	 * PH_REV_OPTION will be Zero and do the normal conversion, no need to take the max value*/

#if PH_REV_OPTION
	if((i == VOLTAGE_PHASE_R) || (i == VOLTAGE_PHASE_Y) || (i == VOLTAGE_PHASE_B))
	{

		while(samples < MAX_ADC_SAMPLES)
		{
			k=0;
			while(k <  20)
			{
				ADCSRA |= 0xC0;     /* Enable ADC and Start the conversion */

				while ((ADCSRA & 0x40))		/* wait for conversion to be completed */
					;

				low_byte = ADCL;
				value = ADCH;
				value <<= 2;
				value |= (low_byte >> 6);

				_delay_ms(2);
				wdt_reset();

				if(value > max)
				{
					max=value;
				}	
				k++;
			}
			final_res=final_res+ max;
			samples++;		//logic to get max of ADC values.
		}
		final_res /=( MAX_ADC_SAMPLES);
	}
	else
	{
		while(samples++ < MAX_ADC_SAMPLES)
		{
			ADCSRA |= 0xC0;     /* Enable ADC and Start the conversion */
			while ((ADCSRA & 0x40))		/* wait for conversion to be completed */
				;

			low_byte = ADCL;
			value = ADCH;
			value <<= 2;
			value |= (low_byte >> 6);
			final_res += value;
		}
		final_res /= MAX_ADC_SAMPLES;
	}
#else
	while(samples++ < MAX_ADC_SAMPLES)
	{
		ADCSRA |= 0xC0;     /* Enable ADC and Start the conversion */
		while ((ADCSRA & 0x40))		/* wait for conversion to be completed */
			;

		low_byte = ADCL;
		value = ADCH;
		value <<= 2;
		value |= (low_byte >> 6);
		final_res += value;
	}
	final_res /= MAX_ADC_SAMPLES;
#endif

	/* if the volatge measured in multimeter and if voltage got in message to be equal
	 * then below multiplication is required
	 * For box with capacitors removed will have different multiplication factor
	 * and box with capacitor will have defferent multiplication factor*/


#if VOLTAGE_MACHING
	if((i == VOLTAGE_PHASE_R) || (i == VOLTAGE_PHASE_Y) || (i == VOLTAGE_PHASE_B))
	{
#if PH_REV_OPTION

		final_res = (unsigned int)(final_res / DEV_FACTOR_PH_REV);
#else
		if(final_res >= 451)
		{
			final_res = (unsigned int)(final_res * 1.13);
		}
		else if((final_res >= 401) && ((final_res <= 450)))
		{
			final_res = (unsigned int)(final_res * 1.15);
		}
		else if((final_res >= 351) && ((final_res <= 400)))
		{
			final_res = (unsigned int)(final_res * 1.18);
		}
		else if((final_res >= 301) && ((final_res <= 350)))
		{
			final_res = (unsigned int)(final_res * 1.25);
		}
		else if((final_res >= 251) && ((final_res <= 300)))
		{
			final_res = (unsigned int)(final_res * 1.26);
		}
		else if((final_res >= 201) && ((final_res <= 250)))
		{
			final_res = (unsigned int)(final_res * 1.44);
		}
		else if((final_res >= 151) && ((final_res <= 200)))
		{
			final_res = (unsigned int)(final_res * 1.64);
		}
#endif	
	}
	/*Since we are not modifieng CT output for phase reversal box 
	 * multiplication factor willl not be there but we are deviding the 
	 * value to match it */
	/* do not do the devision when single phase is enabled*/
	else if(((i == CURRENT_PHASE_R) || (i == CURRENT_PHASE_Y) || (i == CURRENT_PHASE_B)) \
			&& (single_phs_enable == 0))
	{
#if CT_VAL_MATCHING
		final_res = (unsigned int)(final_res / 10);

		if(final_res >= 4)
		{
			final_res = final_res - 2; 

		}
#endif		
	}
#endif
	return final_res;
}

#if PH_REV_OPTION
unsigned int adc_conv_ph_rev(unsigned char i)
{
	unsigned char low_byte=0, samples=0;
	unsigned int value=0, final_res=0;

	/* Select the channel */
	wdt_reset();
	switch(i)		
	{
		case VOLTAGE_PHASE_B:			/* B-phase voltage */
			ADMUX = 0xE0;
			break;

		case VOLTAGE_PHASE_Y:			/* Y-phase voltage */
			ADMUX = 0xE1;
			break;

		case VOLTAGE_PHASE_R:			/* R-phase voltage */
			ADMUX = 0xE2;
			break;

		case CURRENT_PHASE_R:			/* CT-1,  R-phase current */
			ADMUX = 0xE3;
			break;

		case CURRENT_PHASE_Y:			/* CT-2,  Y-phase current */
			ADMUX = 0xE4;
			break;

		case CURRENT_PHASE_B:			/* CT-3,  B-phase current */
			ADMUX = 0xE5;
			break;

		case SMPS_OUTPUT_PIN:			/* SMPS output voltage */
			ADMUX =0xE6;
			break;

		case BATTERY_VOLTAGE_PIN:		/* Battery voltage */
			ADMUX =0xE7;
			break;
	}

	while(samples++ < MAX_ADC_SAMPLES)
	{
		ADCSRA |= 0xC0;     /* Enable ADC and Start the conversion */

		while ((ADCSRA & 0x40))		/* wait for conversion to be completed */
			;

		low_byte = ADCL;
		value = ADCH;
		value <<= 2;
		value |= (low_byte >> 6);
		final_res += value;	
	}

	final_res /= MAX_ADC_SAMPLES;

	return final_res;
}
#endif

