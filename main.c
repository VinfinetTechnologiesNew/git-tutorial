/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author		: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)-s
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 * **************************************************=***************/

#include "fns.h"

unsigned char ph[NPHONES][NDIGIT_PH_NUM + NULL_CHARACTER];
unsigned char serial_number[NDIGIT_SER_NUM + NULL_CHARACTER];
hw_info hw;
hw_info prv_status;
ivrs_info st;

int main(void)
{
	unsigned char active_box = 0,active_secure =0;
#if VOLT_CUR_CONTI_DISP	
	char buff[20];
	unsigned int val = 0;
#endif

	init_hw(); 	/* Initi alize Ports,UART,Timers,Interrupt,GSM,Audio IC */
	init_ivrs();	/* Initialize IVRSs before GSM initialisation */
	firm_upgrade_info();  // inform to user once the bootloader is upgraded.
	first_time_factory_reset();
	init_operations();
	init_wdt();
	print_ssd_delay("INIT",0,PRINT_DELAY);
	read_eeprom(); 					/* Read important device parameters from EEPROM at boot time */
	print_ssd_delay("BOOT",0,PRINT_DELAY); 

	active_box = my_eeprom_read_byte((unsigned char *)EEACTIVE);
	active_secure = my_eeprom_read_byte((unsigned char *)EEACTIVE_SECURE);
	if((theft_detected==0) && (active_box==1) && (active_secure ==1))
	{
		if(byps==0)
		{
			motor_calibration();    
		}
		check_new_sim();
	}
	periodic_reboot_update();

	active_box = my_eeprom_read_byte((unsigned char *)EEACTIVE);
	active_secure = my_eeprom_read_byte((unsigned char *)EEACTIVE_SECURE);

#if VOLT_CUR_CONTI_DISP 
	send_serial0_string("\n\n\r");
	while(1)
	{
		send_serial0_string("\r");
		val = adc_conv(VOLTAGE_PHASE_R);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("R-");
		send_serial0_string(buff);

		val = adc_conv(VOLTAGE_PHASE_Y);

		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tY-");
		send_serial0_string(buff);

		val = adc_conv(VOLTAGE_PHASE_B);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tB-");
		send_serial0_string(buff);

		val = adc_conv(CURRENT_PHASE_R);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		
		send_serial0_string("\tCR-");
		send_serial0_string(buff);
		val = adc_conv(CURRENT_PHASE_Y);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tCY-");
		send_serial0_string(buff);

		val = adc_conv(CURRENT_PHASE_B);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tCB-");
		send_serial0_string(buff);
	}
#endif
	/* Super loop starts from here */
	for(;;) 
	{
		switchon_gsm();	/* switch on gsm if it is off */
		check_var_values(); // check the variable corruption
#if AUTOMATE
		get_password();
#endif

		if((active_box == 1) && (active_secure == 1)) 	// if both the variables are Zero 
			//then thicondition is true.
		{
			check_bypass_and_theft();
			check_far_num_change();
			if(!byps && !theft_detected)
			{
				wirecut_detection(); // added newly
				sms_ri=0;
				if(ri || call_status)	/* Ring indicator(ri) indicates an incoming call and 
							   "call_status" indicates an outgoing call */
				{
					start_ivrs();
				}
				wirecut_detection(); // added newly
				if((sms_ri ==1) && (sms_ver ==1))
				{
					recv_sms();
				}
				else
				{
					check_sig_reg(); /* switch off the GSM module if signal 
							    is less than 5 or simcard is not registered */
				}
				switchon_gsm();	/* switch on gsm if it is off */
				if(debugging_flag==1) 
					send_debugging_msg();

				operate();
			} /* Make a call or attend a call iff KISANRAJA not in BYPASS mode and THEFT not detected */
		}
#if AUTOMATE
		else if((automation_code == 1) && (password_set == 1))
		{

			production_test_fun();

		}
#endif
		else if((active_box == 0) && (active_secure == 0))
		{
			CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
			CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
			print_ssd_delay("NACT",0,SHORT_PRINT_DELAY);
			SET_PIN(LED_CTRL_PORT,AUTO_LED);
			SET_PIN(LED_CTRL_PORT,FAV_LED);			print_ssd_delay("NACT",0,SHORT_PRINT_DELAY);
			activate_box();
		}
		else
		{

			active_box = 1;
			active_secure = 1;
			my_eeprom_write_byte((unsigned char) active_box, (unsigned char  *)EEACTIVE);
			my_eeprom_write_byte((unsigned char) active_secure, (unsigned char  *)EEACTIVE_SECURE);

		}
	}
	return 0;

}


