/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

static volatile unsigned char audio_done;
extern ivrs_info ivrs;

void init_audio(void)
{
	/** Set portB as output port for sending voice group address to audio Ic **/
	DDRB = 0xFF;

	/** configure audio Ic control pins (strobe and reset pins) as output **/
	SET_PIN(DDRG, STB);
	SET_PIN(DDRG, RST2);

	/** configure audio play complete interrupt pin as input pin **/
	CLEAR_PIN(DDRE, PE7);

	/** Make reset high and strobe low, so that no audio is played **/
	SET_PIN(CTLPORT, RST2);
	CLEAR_PIN(CTLPORT, STB);

    audio_done = 0;
}


/********************************************************************************************************************
 * This function will play the voice group at a particular address. Address is supplied as argument to this function.
 ********************************************************************************************************************/	 
void audio_out(unsigned int addr)
{
	wdt_reset();

	init_audio();
	audio_done = dtmf_ok = 0;
	wdt_reset();

	/** check if the address is within the available voice groups **/
	if(addr < AUDIO1_MAX)
		CLEAR_PIN(PORTG, RST2);
	else
		return;

	CLEAR_PIN(CTLPORT, STB);	/** Stop any audio message in play **/
	Delay_sec(1);

	ADRPORT = reverse_bits_in_byte((unsigned char)addr);	/** Select the audio (voice group) to be played **/ 

	SET_PIN(CTLPORT, STB);	/* Trigger audio playback */

	/** configure audio play complete interrupt pin as input pin **/
	CLEAR_PIN(DDRE, PE7);

	start_timer(MAX_PLAY_TIME);

	/* when the audio being played is complete, 'audio_done' variable will be set to one in the ISR for interrupt-7 */
	while((!audio_done) && (!timer_expired))	
	{
		wdt_reset();         
		_delay_ms(50);
		/* If any KEY is pressed by the user(dtmf received), then stop the audio being played */
		if(dtmf_ok == 1)
		{
			/** Make reset high and strobe low to stop the audio that is currently being played **/
			SET_PIN(DDRG, STB);
			SET_PIN(DDRG, RST2);
			CLEAR_PIN(CTLPORT, STB);	
			wdt_reset();         
			_delay_ms(50);
			wdt_reset();         
			SET_PIN(CTLPORT, RST2);
			//CLEAR_PIN(CTLPORT, STB);	
			audio_done=1;
		}
	}
	stop_timer();
	wdt_reset();
	check_call_status();
}


/** Audio Complete ISR **/
ISR(INT7_vect)
{
	CLEAR_PIN(CTLPORT, STB);	/** Stop any audio message in play **/
	audio_done = 1;
}


/******************************************************************************************************************* 
 * Function to reverse the bits in a byte.
 * This function is used in audio_out function to reverse the bits of address before sending it to the audio Ic. 
 *
 * Reversing of bits is required as the connections from microcontroller to audio Ic are in the following fashion:-
 * PB0 --- S8
 * PB1 --- S7
 * ..........
 * ..........
 * ..........
 * PB7 --- S1
 *
 * note:- S1, S2 .... S8 are address lines of audio Ic, and PB0, PB1, .... PB7 are portb pins of microcontroller.
 *******************************************************************************************************************/
unsigned char reverse_bits_in_byte(unsigned char address)
{
	unsigned char bit=0, counter=0, result=0;

	for(counter=0; counter <= 7; counter++)
	{   
		bit = address & 0x01;
		address >>= 1;
		result <<= 1;
		result += bit;
	}   

	return result;
}


