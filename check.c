/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

#define VOLTAGE_DIFF 	100
#define SMPS_DIFFRENCE 	200
#define REF_PHASE_VOLT 	520

unsigned char msg_info[150]; 
//unsigned int r_phase_voltage=REF_PHASE_VOLT, b_phase_voltage=REF_PHASE_VOLT, y_phase_voltage=REF_PHASE_VOLT;
unsigned int r_phase_current, y_phase_current, b_phase_current, lower_volt_range;//, percentage_current_raise;
unsigned int higher_volt_range;//, over_current_limit;
unsigned char run_in_which_phase, g_phase, g_sub_phase;//, water_flow_sensor;
extern unsigned char motor_on;
extern hw_info hw;
extern ivrs_info ivrs, st;			                           

extern unsigned int load_cur, cur_range;//, fav_volt;
extern unsigned char data_ssd[];
extern unsigned char data_array[];


/**************************************************************
 *
 * This function will check all the three phase voltages and 
 * determine whether it is favourable condition to start the motor. 
 * If it is not favourable, it identifies the error and 
 * updates a variable 'hw.err' accordingly and returns status (C_UnFavourable).
 ******************************************************/

Cond check_favourable(void)
{
	unsigned int previous_r_phase=0, previous_y_phase=0, previous_b_phase=0; 
	unsigned int present_r_phase=0, present_y_phase=0, present_b_phase=0;
	unsigned int previous_smps_val=0,present_smps_val=0;
	unsigned char r_fuse=0, y_fuse=0, b_fuse=0;

	static unsigned char r_g_phase=0,y_g_phase=0,b_g_phase=0;
	static unsigned char smps_flag=0;

	unsigned char twph_cond1=0;
	unsigned char singleph_cond1=0, singleph_cond2=0, singleph_cond3=0;
	unsigned char singleph_cond4=0, singleph_cond5=0,singleph_cond6=0;
	char i=0;
	unsigned char r_b_phase_removed,single_phase;
	char ph_rev_cond = 0;

#if VOLT_DISP_DEBUG
	char buff[20];
	unsigned int val = 0;
#endif


	g_phase=0;

	hw.motor = motor_status();

	if(hw.motor == M_Started)
	{
		wirecut_detection();
	}


	/** check in how many phases favourable voltage is available*/

	previous_r_phase = adc_conv(VOLTAGE_PHASE_R);
	previous_y_phase = adc_conv(VOLTAGE_PHASE_Y);
	previous_b_phase = adc_conv(VOLTAGE_PHASE_B);

#if VOLT_DISP_DEBUG
		send_serial0_string("\n\n\r");
		val = adc_conv(VOLTAGE_PHASE_R);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\rR-");
		send_serial0_string(buff);

		val = adc_conv(VOLTAGE_PHASE_Y);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tY-");
		send_serial0_string(buff);

		val = adc_conv(VOLTAGE_PHASE_B);
		buff[3]=(val%10)+'0';
		buff[2]=(((val%100)-(val%10))/10)+'0';		
		buff[1]=(((val%1000)-(val%100))/100)+'0';
		buff[0]=(((val%10000)-(val%1000))/1000)+'0';
		buff[4] = '\0';
		send_serial0_string("\tB-");
		send_serial0_string(buff);

#endif
	if(single_phs_enable == 0)
	{
		if((previous_r_phase > MIN_PHASE_VOLTAGE) || (previous_y_phase > MIN_PHASE_VOLTAGE) \
				|| (previous_b_phase > MIN_PHASE_VOLTAGE) )
		{
			Delay_sec(2);

			present_r_phase = adc_conv(VOLTAGE_PHASE_R);
			present_y_phase = adc_conv(VOLTAGE_PHASE_Y);
			present_b_phase = adc_conv(VOLTAGE_PHASE_B);

#if 0
			print_ssd_delay(NULL,present_r_phase,PRINT_DELAY);
			print_ssd_delay(NULL,present_y_phase,PRINT_DELAY);
			print_ssd_delay(NULL,present_b_phase,PRINT_DELAY);
#endif


			if((abs(previous_r_phase - present_r_phase)) > VOLTAGE_DIFF)
			{
				print_ssd_delay("RET1",0,500);
				return prv_status.cond;
			}
			if((abs(previous_y_phase - present_y_phase)) > VOLTAGE_DIFF)
			{
				print_ssd_delay("RET1",0,500);
				return prv_status.cond;
			}
			if((abs(previous_b_phase - present_b_phase)) > VOLTAGE_DIFF)
			{
				print_ssd_delay("RET1",0,500);
				return prv_status.cond;
			}
		}
	}
	else
	{
		present_r_phase = adc_conv(VOLTAGE_PHASE_R);
		present_y_phase = adc_conv(VOLTAGE_PHASE_Y);
		present_b_phase = adc_conv(VOLTAGE_PHASE_B);
	}

	hw.motor = motor_status();

	wirecut_detection(); 

	/** once the resistor network values are stabilized, check for wirecut, low-voltage and favourable conditions **/

	/* modified single phase ,wire cut detection logic including SMPS voltage,starts */

	if(present_r_phase < MIN_PHASE_VOLTAGE) 
		r_fuse = 1;
	if(present_y_phase < MIN_PHASE_VOLTAGE)
		y_fuse = 1;
	if(present_b_phase < MIN_PHASE_VOLTAGE)
		b_fuse = 1;

	if(single_phs_enable ==0)
	{

		/******** low voltage and over voltage check *************/
		/*
		   if((present_r_phase > lower_volt_range) && (present_r_phase < higher_volt_range))
		   r_g_phase = 1;
		   if((present_y_phase > lower_volt_range) && (present_y_phase < higher_volt_range))
		   y_g_phase = 1;
		   if((present_b_phase > lower_volt_range) && (present_b_phase < higher_volt_range))
		   b_g_phase = 1;
		   */
		if((r_g_phase == 0) && ((present_r_phase > (lower_volt_range + hyster_value))\
					&&(present_r_phase < (higher_volt_range - hyster_value))))
		{
			r_g_phase = 1;

		}
		else if((r_g_phase == 1) && ((present_r_phase < (lower_volt_range - hyster_value))\
					||(present_r_phase > (higher_volt_range + hyster_value))))
		{
			r_g_phase = 0;

		}

		if((y_g_phase == 0) && ((present_y_phase > (lower_volt_range + hyster_value))\
					&&(present_y_phase < (higher_volt_range - hyster_value))))
		{
			y_g_phase = 1;

		}
		else if((y_g_phase == 1) && ((present_y_phase < (lower_volt_range - hyster_value))\
					||(present_y_phase > (higher_volt_range + hyster_value))))
		{
			y_g_phase = 0;

		}

		if((b_g_phase == 0) && ((present_b_phase > (lower_volt_range + hyster_value))\
					&&(present_b_phase < (higher_volt_range - hyster_value))))
		{
			b_g_phase = 1;

		}
		else if((b_g_phase == 1) && ((present_b_phase < (lower_volt_range - hyster_value))\
					||(present_b_phase > (higher_volt_range + hyster_value))))
		{
			b_g_phase = 0;

		}


		if(adc_conv(SMPS_OUTPUT_PIN) > MIN_SMPS_VOLTAGE)
		{
			smps_flag = 1; // SMPS is giving 5v
		}
		else if(adc_conv(SMPS_OUTPUT_PIN) < MIN_SMPS_VOLTAGE_LOW)
		{
			smps_flag = 0; // SMPS voltage is not proper
		}


		if((r_fuse == 1) && (y_fuse == 1) && (b_fuse == 1)) // no power
		{
			stop_motor();
			SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
#if AUTOMATE
			if((automation_code ==1) && (password_set==1))
			{
				print_ssd_delay("NO POWR",0,500);
			}
#else
			print_ssd_delay("NOCU",0,SHORT_PRINT_DELAY);
#endif
			strcpy((char *)msg_info,SMS_NO_POW);
			if(run_in_which_phase == TWO_PHASE)	
				CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			prv_status.err = hw.err = E_NoPower;
			wdt_reset();

			return C_UnFavourable;
		}
		else 
		{
			/*** high voltage check ****/
			if((present_r_phase > (higher_volt_range + hyster_value)) \
					||(present_y_phase > (higher_volt_range + hyster_value)) \
					|| (present_b_phase > (higher_volt_range + hyster_value)))
			{
				stop_motor();
				SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
#if AUTOMATE
				if(automation_code ==1)
				{
					print_ssd_delay("HGH VTG",0,500);
				}
#else
				print_ssd_delay("HIGH",0,SHORT_PRINT_DELAY);
#endif
				hw.err = E_HighVoltage;
				g_phase = HIGH_VOLTAGE;
				wdt_reset();
				return C_UnFavourable;
			}

			singleph_cond1 = ((r_g_phase == 1) && (y_g_phase == 0) && (b_g_phase == 0));
			singleph_cond2 = ((r_g_phase == 0) && (y_g_phase == 0) && (b_g_phase == 1));
			singleph_cond4 = ((r_g_phase == 0) && (y_g_phase == 0) && (b_g_phase == 0));

			twph_cond1 = ((r_g_phase == 1) && (y_g_phase == 0) && (b_g_phase == 1));

			/* For SMPS if all the 3 phases are loaded then 
			 * all_3_phases_loaded variable will be 1.
			 * else it is 0 */
			if(all_3_phases_loaded == 0)
			{
				if(smps_flag == 1)
				{
					singleph_cond3 = ((r_g_phase == 0) && (y_g_phase == 1) && (b_g_phase == 0));
					singleph_cond5 = ((r_g_phase == 0) && (y_g_phase == 1) && (b_g_phase == 1));
					singleph_cond6 = ((r_g_phase == 1) && (y_g_phase == 1) && (b_g_phase == 0));
				}
			}

			//wirecut_cond1 = ((r_g_phase == 1) && (y_g_phase == 0) && (b_g_phase == 1) && (smps_flag == 0));

			if(twph_cond1 == 1)		/******** Two phase **********/
			{
				if(run_in_which_phase == THREE_PHASE_ANDHRA)
				{
					stop_motor();
#if AUTOMATE
					if((automation_code ==1) && (password_set ==1))
					{
						if(display_flag ==1)
							print_ssd_delay("\n\r",0,500);
					}
#else
					print_ssd_delay("WIR1",0,PRINT_DELAY);
#endif
					strcpy((char *)msg_info, SMS_FUSE_REMOVED);
					SET_PIN(LED_CTRL_PORT, FAV_LED);
					prv_status.err = hw.err = E_WireCut;
					g_phase = THREE_PHASE_ANDHRA;
					wdt_reset();
					return C_UnFavourable;
				}
				else if(run_in_which_phase == THREE_PHASE_KARNATAKA)
				{
					g_phase = TWO_PHASE;
					/** for *76#, g_sub_phase=3 --> default value [ call only in 3-phase ]
					  g_sub_phase=0 --> configure through admin access *26# 
					  [ call in both 2-phase and 3-phase ] **/	
					if(g_sub_phase != 0)
					{
						stop_motor();
						SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
						print_ssd_delay("2 PH",0,SHORT_PRINT_DELAY);
						hw.err = E_LowVoltage;
						wdt_reset();
						return C_UnFavourable;
					}
				}
				else if(run_in_which_phase == TWO_PHASE)
				{
					strcpy((char *)msg_info, SMS_2PH_FAV);
					g_phase = TWO_PHASE;	
					/** for *86#, g_sub_phase=0 --> default value [ call in both 2-phase and 3-phase ]
					  g_sub_phase=3 --> configure through admin access *26# [ call only in 3-phase ] **/	
					if(g_sub_phase == THREE_PHASE_KARNATAKA)
					{
						hw.err=0;
						wdt_reset();
						return C_UnFavourable;
					}
				}
			}
			else if(singleph_cond1 || singleph_cond2 || singleph_cond3 \
					|| singleph_cond4 || singleph_cond5 || singleph_cond6)/* single phase */
			{
				stop_motor();
				SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
				print_ssd_delay("1 PH",0,SHORT_PRINT_DELAY);
				hw.err = E_LowVoltage;
				g_phase = SINGLE_PHASE;
				wdt_reset();
				return C_UnFavourable;
			}
			/******* Wire cut *******/
			else if((((y_g_phase == 1) && (smps_flag == 0)) && (M_Stopped == motor_status())) \
					&& (all_3_phases_loaded == 0))	
			{
				/* when motor is running do not detect the wire cut ,
				 * wire cut while motor is running will be detected by wirecut_detection() function 
				 * ,if the SMPS connector is loosely connected then wire cut be detected wrongly....*/
				stop_motor();
				if(automation_code ==1)
				{
					print_ssd_delay("\rR/B PHS CUT",0,500);
					if(display_flag ==1)
						print_ssd_delay("\n\r",0,500);
				}
				else
					print_ssd_delay("WIR2",0,PRINT_DELAY);

				strcpy((char *)msg_info, SMS_FUSE_REMOVED);
				SET_PIN(LED_CTRL_PORT, FAV_LED);
				prv_status.err = hw.err = E_WireCut;
				wdt_reset();

				return C_UnFavourable;
			}
			// favourable condition do not enter into bthis loop
			else if((!((r_g_phase == 1) && (y_g_phase == 1) && (b_g_phase == 1))) && (all_3_phases_loaded == 1))
			{
				// read in read eeprom default 200 wire_cut_v_level=200;
				// this need to be removed ,this value is made configurable.
				if((y_g_phase == 1) && ((present_r_phase < wire_cut_v_level) \
							|| ((present_b_phase < wire_cut_v_level))))
				{

					if(((present_r_phase < MIN_PHASE_VOLTAGE) && (b_g_phase == 1)) \
							|| ((r_g_phase == 1) && (present_b_phase < MIN_PHASE_VOLTAGE)))
					{
						r_b_phase_removed=1;
					}
					else if(((present_r_phase < wire_cut_v_level) \
								&& (present_b_phase < wire_cut_v_level)))
					{
						if(((present_r_phase > MIN_PHASE_VOLTAGE) \
									&& (present_b_phase > MIN_PHASE_VOLTAGE )))
						{
							if((abs(present_r_phase - present_b_phase)) < 100)
							{
								r_b_phase_removed=1; // or b_phase_removed
							}
							else
							{
								single_phase=1;
							}
						}
						else
						{
							single_phase=1;
						}
					}
					else
					{
						single_phase=1;
					}
				}
				else
				{
					single_phase = 1;
				}

				if(r_b_phase_removed == 1)
				{
					stop_motor();
#if AUTOMATE
					if((automation_code ==1) && (password_set ==1))
					{

						if(display_flag ==1)
							print_ssd_delay("\n\r",0,500);

					}
#else
					print_ssd_delay("WIR4",0,PRINT_DELAY);
					strcpy((char *)msg_info, SMS_FUSE_REMOVED);
#endif
					SET_PIN(LED_CTRL_PORT, FAV_LED);
					prv_status.err = hw.err = E_WireCut;
					wdt_reset();
					return C_UnFavourable;

				}
				else if(single_phase == 1)
				{
					stop_motor();
					SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
					print_ssd_delay("1PH1",0,SHORT_PRINT_DELAY);
					hw.err = E_LowVoltage;
					g_phase = SINGLE_PHASE;
					wdt_reset();
					return C_UnFavourable;
				}		
			}
		}
	}
	else if(single_phs_enable == 1)
	{
		/* SMPS voltage fluctuation check is done only when single phase is activated.*/
		previous_smps_val = adc_conv(SMPS_OUTPUT_PIN);
		Delay_sec(2);
		present_smps_val = adc_conv(SMPS_OUTPUT_PIN);

		if((abs(previous_smps_val - present_smps_val)) > SMPS_DIFFRENCE )
		{
			print_ssd_delay("SMPF",0,SHORT_PRINT_DELAY);
			return prv_status.cond;
		}

		if(adc_conv(SMPS_OUTPUT_PIN) < MIN_SMPS_VOLTAGE)
		{
			SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED 
			print_ssd_delay("NOCU",0,SHORT_PRINT_DELAY);
			strcpy((char *)msg_info,SMS_NO_POW);
			//if(run_in_which_phase == TWO_PHASE)	
			//	CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			hw.err = E_NoPower;
			wdt_reset();
			return C_UnFavourable;
		}


	}

	/* modified single phase ,wire cut detection logic including SMPS voltage ,ends*/


	// checking the phase reversal

	wirecut_detection();

	ph_rev_cond = check_phase_rev();

	if(ph_rev_cond == 0)
	{
		// phases are reversed

		Delay_sec(1);
		ph_rev_cond = check_phase_rev();

		if(ph_rev_cond == 0)
		{

			stop_motor();
			wdt_reset();
			for(i=0;i<4;i++)
			{

				CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
				_delay_ms(200);
				SET_PIN(LED_CTRL_PORT,FAV_LED);
				_delay_ms(200);
			}
			wdt_reset();
			SET_PIN(LED_CTRL_PORT, FAV_LED);        // Turn OFF the favourable LED

			print_ssd_delay("PREV",0,SHORT_PRINT_DELAY);
			hw.err = E_Ph_reverse;
			if(prv_status.err != hw.err )
			{
				prv_status.err = hw.err; 
				prv_status.cond = 0;

			}
			strcpy((char *)msg_info, SMS_PHASE_REVERSE);
			if(hw.motor == M_Stopped)
			{
				strcat((char *)msg_info, SMS_MOTOR_OFF);

			}
			wdt_reset();
			return C_UnFavourable;
		}

	}
	else if(ph_rev_cond == 2)
	{
		// error condition, not phase reversed
	}


	wirecut_detection();

	/** case 2: Motor stopped manually from starter **/
	if((hw.motor == M_Stopped) && motor_on)
	{
		if(run_in_which_phase == TWO_PHASE)	
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
		motor_on = 0;
		if(single_phs_enable == 1)
		{
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
			Delay_sec(2);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
			Delay_sec(1);
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
			Delay_sec(1);
			//stop_motor();
		}
		print_ssd_delay("MSTP",0,PRINT_DELAY);
		strcpy((char *)msg_info,SMS_MANUAL_STOP);
		hw.err=E_ManualStop;
		wdt_reset();
		call_motor_off=0; // added newly 
		return C_Favourable;
	}

	/** Load three phase or two phase calibration values **/
	if((g_phase == TWO_PHASE))
	{
		if((adc_conv(VOLTAGE_PHASE_Y) < MIN_PHASE_VOLTAGE) && ((run_in_which_phase == TWO_PHASE) \
					||((run_in_which_phase == THREE_PHASE_KARNATAKA) && (g_sub_phase == 0))))
		{
			r_phase_current = my_eeprom_read_int((unsigned int*)TWPH_R_PHASE_CURRENT);
			y_phase_current = my_eeprom_read_int((unsigned int*)TWPH_Y_PHASE_CURRENT);
			b_phase_current = my_eeprom_read_int((unsigned int*)TWPH_B_PHASE_CURRENT);
			cur_range = my_eeprom_read_int((unsigned int*)TWPH_EECRNGE);
		}
		else
		{
			r_phase_current = my_eeprom_read_int((unsigned int*)R_PHASE_CURRENT);
			y_phase_current = my_eeprom_read_int((unsigned int*)Y_PHASE_CURRENT);
			b_phase_current = my_eeprom_read_int((unsigned int*)B_PHASE_CURRENT);
			cur_range = my_eeprom_read_int((unsigned int*)EECRNGE);
		}
	}

	/** case 3: Motor started manually from starter **/
	if((hw.motor == M_Started) && (!motor_on)) 
	{
		if(run_in_which_phase == TWO_PHASE)	
			SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
		motor_on = 1;
		print_ssd_delay("MSTR",0,PRINT_DELAY);
		strcpy((char *)msg_info,SMS_MANUAL_START);
		hw.err=E_ManualStart;
		wdt_reset();
		call_motor_off=0; // added newly 
		if(dryrun.enable == 1)
			dryrun.startup_flg = 1;	/** check for dryrun at motor startup even 
						  when motor is switched ON from starter **/
	}

	if((hw.motor == M_Started) )
	{		
		/** case 4: Check for wire cut **/
		wirecut_detection();
		/***** End of wire cut check *****/

		/** case 5: Check for Dry Run  **/
		if(dryrun.enable ==1)
		{
			if(dryrun.startup_flg==1)
			{
				Delay_sec(dryrun.water_wait);
				dryrun.startup_flg = 0;
			}
			dryrun_detection();
			/** when dryrun is enabled, double check for manual stop  **/
			if((hw.motor == M_Stopped) && motor_on)
			{
				if(run_in_which_phase == TWO_PHASE)	
					CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
				motor_on = 0;
				if(single_phs_enable == 1)
				{
					SET_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
					Delay_sec(2);
					CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
					Delay_sec(1);
					CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
					Delay_sec(1);
					//	stop_motor();
				}
				print_ssd_delay("MSTP",0,PRINT_DELAY);
				strcpy((char *)msg_info,SMS_MANUAL_STOP);
				hw.err=E_ManualStop;
				wdt_reset();
				call_motor_off = 0;
				return C_Favourable;
			}
		}
		/***** End of dry run check *****/
	}

	if(automation_code ==0)
		print_ssd_delay("FAVR",0,SHORT_PRINT_DELAY);
	CLEAR_PIN(LED_CTRL_PORT, FAV_LED);		/* Turn ON the power LED */

	if((hw.err != E_ManualStart) && (hw.err != E_WireCut) && (hw.err != E_NoWater) && (E_OverCurrent != hw.err))
	{
		strcpy((char *)msg_info, SMS_FAV);
		hw.err=0;
	}

	return C_Favourable;
}


void dryrun_detection(void)
{ 
	unsigned char local_dry_run_flg = 0, i;
	unsigned int previous_ct_value=0,present_ct_value=0;
	unsigned char r_fuse=0, y_fuse=0, b_fuse=0;
	unsigned int present_r_phase=0, present_y_phase=0, present_b_phase=0,present_smps_val=0;

	if((motor_status() == M_Started))
	{
		present_r_phase = adc_conv(VOLTAGE_PHASE_R);
		present_y_phase = adc_conv(VOLTAGE_PHASE_Y);
		present_b_phase = adc_conv(VOLTAGE_PHASE_B);
		present_smps_val = adc_conv(SMPS_OUTPUT_PIN);

		if(present_r_phase < MIN_PHASE_VOLTAGE) 
			r_fuse = 1;
		if(present_y_phase < MIN_PHASE_VOLTAGE)
			y_fuse = 1;
		if(present_b_phase < MIN_PHASE_VOLTAGE)
			b_fuse = 1;

		/* since in single phase resistor network values are not considered below code is added. */
		if(single_phs_enable == 1) 
		{
			if(present_smps_val < MIN_SMPS_VOLTAGE)
			{
				r_fuse = 1;
				y_fuse = 1;
				b_fuse = 1;
			}
			else
			{
				r_fuse = 0;
				y_fuse = 0;
				b_fuse = 0;
			}
		}

		if(!((r_fuse == 1) && (y_fuse == 1) && (b_fuse == 1))) // inform dryrun only if power available.
		{
			// for 3 CT's consider all 3 ct values for dry run detection. 
			for(i = CURRENT_PHASE_R, previous_ct_value=0 ; i < MAX_CT_VALUE; i++)
			{
				previous_ct_value += adc_conv(i);
			}
			if(previous_ct_value < cur_range)
			{
				Delay_sec(2);
				if(motor_status() == M_Started)
				{
					previous_ct_value=0;
					present_ct_value=0;

					for(i = CURRENT_PHASE_R, previous_ct_value=0 ; i < MAX_CT_VALUE; i++)
					{
						previous_ct_value += adc_conv(i);
					}

					Delay_sec(2);

					for(i = CURRENT_PHASE_R, present_ct_value=0 ; i < MAX_CT_VALUE; i++)
					{
						present_ct_value += adc_conv(i);
					}

					if((abs(previous_ct_value - present_ct_value)) > 100)
					{
						// power went or motor stopped manually 
					}
					else
					{
						if(present_ct_value < cur_range)
						{
							local_dry_run_flg=1; // dry run has happened
						}
					}
				}
			}
		}

		if(local_dry_run_flg==1)
		{
			prv_status.motor = hw.motor;
			stop_motor();
			//print_ssd_delay("DRYN",0,PRINT_DELAY);
			strcpy((char *)msg_info,SMS_NO_WATER);
			hw.err = E_NoWater;
			prv_status.err=hw.err;
			SET_PIN(LED_CTRL_PORT, FAV_LED);
			call_motor_off=1; // added newly 
		}
	}
}


unsigned char wirecut_detection(void)
{
	unsigned int val=0;
	unsigned char i=0, wire1=0, wire2=0;
	char wire_count=0;
	char wire_cut_detected = 0;
	unsigned int check1=0,check2=0,check3=0;


	hw.motor = motor_status();

	if(hw.motor == M_Stopped )
	{
		return 0; // if motor is already stopped do not check for wire cut ,just exit from this function.
	}

	if(run_in_which_phase == THREE_PHASE_ANDHRA)
	{
		wire1=0;
		wire2=0;
		/* FOR 3 CT's wirecut logic. */
		for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
		{		
			val = adc_conv(i);
			if(val < MIN_CUR_VAL)
				wire1++;
		}
		if((wire1 > 0) && (wire1 < 3))
		{	
			Delay_sec(WAIT_FOR_CURRENT_TO_STABILIZE);	/* wait for current values to be stabilized */

			for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
			{		
				val = adc_conv(i);
				if(val < MIN_CUR_VAL)
					wire2++;
			}
		}

	}
	else if((run_in_which_phase == THREE_PHASE_KARNATAKA) || (run_in_which_phase == TWO_PHASE))
	{
		if(g_phase == TWO_PHASE)
		{
			val=0;
			wire1=0;
			wire2=0;
			for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
			{		
				val = adc_conv(i);
				if(val < MIN_CUR_VAL)
					wire1++;
			}
			if((wire1 > 0) && (wire1 < 3))
			{
				check1=0;
				check2=0;
				check3=0;
				wire_count=0;

				Delay_sec(WAIT_FOR_CURRENT_TO_STABILIZE);// wait for current values to be stabilized

				if(adc_conv(CURRENT_PHASE_R) < MIN_CUR_VAL)
				{
					check1=1;
					wire_count++;
				}

				if(adc_conv(CURRENT_PHASE_Y) < MIN_CUR_VAL)
				{
					check2=1;
					wire_count++;
				}

				if(adc_conv(CURRENT_PHASE_B) < MIN_CUR_VAL)
				{
					check3=1;
					wire_count++;
				}

				if( (wire_count == 1))
				{
					wire_count=0;
					Delay_sec(1);
					if(adc_conv(CURRENT_PHASE_R) < MIN_CUR_VAL)
					{
						check1=1;
						wire_count++;
					}

					if(adc_conv(CURRENT_PHASE_Y) < MIN_CUR_VAL)
					{
						check2=1;
						wire_count++;
					}

					if(adc_conv(CURRENT_PHASE_B) < MIN_CUR_VAL)
					{
						check3=1;
						wire_count++;
					}

					if(wire_count == 1)
					{
						wire_cut_detected = 1;	
					}
				}

			}
		}

		else
		{
			wire1=0;
			wire2=0;
			for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
			{		
				val = adc_conv(i);
				if(val < MIN_CUR_VAL)
					wire1++;
			}
			if((wire1 > 0) && (wire1 < 3))
			{	
				Delay_sec(WAIT_FOR_CURRENT_TO_STABILIZE);
				/* wait for current values to be stabilized */

				for(i = CURRENT_PHASE_R; i < MAX_CT_VALUE; i++)
				{		
					val = adc_conv(i);
					if(val < MIN_CUR_VAL)
						wire2++;
				}
			}
		}

	}
	if(((wire1 > 0) && (wire1 == wire2)) || (wire_cut_detected == 1))// || wire3)
	{
		if(run_in_which_phase == TWO_PHASE)	
			CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);
		prv_status.motor = hw.motor;
		stop_motor();
		print_ssd_delay("WIR3",0,PRINT_DELAY);
		strcpy((char *)msg_info, SMS_WIRE_CUT);
		prv_status.err = hw.err = E_WireCut;
		SET_PIN(LED_CTRL_PORT, FAV_LED);
		call_motor_off=1; // added newly 
		return 1;
	}
	/*
	else
	{
		dryrun_detection();

	}
	*/

	return 0;
}	

