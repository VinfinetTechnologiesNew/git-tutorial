/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

unsigned int timer0_val,timer2_val;
volatile unsigned int timer0_sec_count;
volatile unsigned int tick,tick2; 
volatile unsigned char dtmf_recvd, dtmf_ok, pos, hash_flg, dtmf_buf[BUF_LEN];  
volatile unsigned int tmr1_count;
unsigned int timer1_delay;//,wait_for_eeprom_writex
volatile unsigned char ri;			/* Ring Indicator */
ivrs_info ivrs;				/* Status of IVRS */

volatile unsigned char timer_expired, timer1_expired, timer2_expired, theft_detected, spike_theft;
//volatile unsigned char g_debug_menu_index, g_rx0_buff[RX0_BUFF_SIZE];
volatile unsigned char recv_index,start_recieve,recieve_array,recv_time,recv_pswd;//,time_received;

/****************************************************************
 * This ISR will be triggered after receiving a byte from USART0.
 ****************************************************************/ 

ISR(USART0_RX_vect)
{
	recv_index= UDR0;
	if(recv_index == RECV_SER)
	{
		buf_pos=0;
		start_recieve = 1;
		return;
	}
	else if(recv_index == RECV_TIME)
	{
		buf_pos = 0;
		recv_time = 1;
		return;
	}
	else if(recv_index == RECV_PSWD) // for k
	{
		buf_pos = 0;
		recv_pswd = 1;
		return;
	}
	else if((((recv_index >= 'A') && (recv_index <= 'Z')) || ((recv_index >= 'a') \
					&& (recv_index <= 'z')) || ((recv_index >= '0')\
				       	&& (recv_index <= '9')) || (recv_index == END_SER )) \
			&& ((start_recieve == 1) ||(recv_time == 1) || (recv_pswd == 1))) 
	{
		if((recv_index == SERIAL_BYTE) || (recv_index == TIME_BYTE) || (recv_index == PSWD_BYTE)) //for r
		{
			data_index = recv_index;
			buf_pos=0;
			recieve_array = 1;
			return;
		}
		if( (recieve_array == 1))
		{
			if((recv_index == END_SER ))// && (buf_pos == NDIGIT_SER_NUM))
			{
				recieve_array = 0;
				start_recieve = 0;
				data_buf[buf_pos] = '\0';
				buf_pos=0;
				if(recv_time == 1)
				{
					time_received =1;
					recv_time = 0;
				}
				else if(recv_pswd ==1)
				{
					pswd_rx =1;
					recv_pswd =0;
				}
				else
				{
					recieved = 1;
				}
				return;
			}
			else
			{
				data_buf[buf_pos]=recv_index;
				++buf_pos;
				return;
			}
		}
	}
	else
	{
		return;
	}
}




/*************************
 * ISR for DTMF reception.
 *************************/
ISR(INT0_vect)
{
	/** configure pins PC4 to PC7 as input lines for detecting dtmf tones **/
	DDRC &= 0x0F;
	
	// to avoid wrong DTMF detection below line sof code is added,
	/* this delay is to avoid the wrong DTMF detection */
	CLEAR_PIN(DDRD, PD0); // configure DTMF interrupt pin as input.
	wdt_reset();
	_delay_ms(DTMF_ISR_WAIT); // by default DTMF_ISR wait is 30 mili seconds
	wdt_reset();
	if(DTMF_STD_PIN == 0)
	{
		return;

	}
	// code to handle wrong DTMF detection ends here.

	DDRC &= 0x0F;

	if(pos > BUF_LEN)
		pos = BUF_LEN;

	dtmf_buf[pos] = read_dtmf()+ '0';
	dtmf_recvd = 1,dtmf_ok=1;

	if(dtmf_buf[pos] == (STAR + '0')) {
		ivrs.level = LInvalid;
		dtmf_buf[pos] = ASCII_STAR;		/* ascii value of '*' --> 42 */
	}

	if(dtmf_buf[pos] == (HASH + '0'))	
	{
		hash_flg = 1;
		dtmf_buf[pos] = ASCII_HASH;		/* ascii value of '#' --> 35 */
	}else
		hash_flg = 0;

#if DTMF_CHECK_DEBUG
	wdt_reset();
	SET_PIN(LED_CTRL_PORT,AUTO_LED);
	CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
	_delay_ms(100);
	SET_PIN(LED_CTRL_PORT,AUTO_LED);
	wdt_reset();
#endif

	pos++;
}


/*************************
 * ISR for Ring Indicator.
 *************************/
ISR(INT1_vect)
{
	ri = 1;
}


/**************************
 * ISR for Theft Detection.
 **************************/
ISR(INT6_vect)
{
    if(THEFT_DETECT && theft_enable){
        theft_detected = 1;
		spike_theft = 1;
        EIMSK &= ~( 1<<INT6 );
    }
}



/*****************
 * Read Dtmf Pins. 
 *****************/
	
unsigned char read_dtmf(void )
{
	volatile uchar val=0, tmp=0;

	tmp = PINC & 0xF0;		/** read the pins PC4 to PC7 **/

	/** the following line reverses the bits in higher nibble. **/
	val |= ((tmp >> 3) & 0x10) | ((tmp << 3) & 0x80) | ((tmp >> 1) & 0x20) | ((tmp << 1) & 0x40);
	val >>= 4;

	/* Dtmf value for 0 is 10 */
	if(val == ZERO)
		val = 0;

	return val;
}

/*********************** 
 * 8 bit timer/counter0  
 ***********************/
	
void start_timer(unsigned int sectors)
{
	mili_sec_counter = 0;
	mili_sec_counter_2 = 0;
	tick= 0;
	timer0_val = sectors;
	timer0_sec_count = 0;
	timer_expired = 0;

	TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));	/* set clock source bits as zero to stop timer */

	TCNT0 = INITIALIZE_COUNTER0;
	//TCCR0 |= (1 << CS02) | (1 << CS01) | (1 << CS00);	/* select prescalar as 1024 */ 
	TCCR0 |= (1 << CS00); 					// no pre scaling, clock is directly connected
	TIMSK |= (1 << TOIE0);
	
	wdt_reset();
}

	
void stop_timer(void)
{
	tick = 0;
	TIMSK &= (~(1 << TOIE0));
	TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));	/* set clock source bits as zero to stop timer */
	wdt_reset();
}



/*************************** 
 * ISR for Timer0 overflow. 
 **************************/
ISR(TIMER0_OVF_vect)
{
	TCNT0 = INITIALIZE_COUNTER0;
	tick++;
	if(tick >= ITERATIONS_FOR_ONE_SECOND_COUNTER0)
	{
		// for every one mili seconds this if condition becomes true
		mili_sec_counter++;
		mili_sec_counter_2++;

		tick= 0;
		if(mili_sec_counter > 0XEFFF)
		{
			mili_sec_counter=0;
		}

		if(mili_sec_counter_2 > 1000) 			// for every 1sec this if condition will become true 
		{
			timer0_sec_count++;
			mili_sec_counter_2 = 0;

			if(timer0_sec_count >= timer0_val)
			{
				timer_expired = 1;
				TIMSK &= (~(1 << TOIE0));
				TCCR0 &= ~((1 << CS02) | (1 << CS01) | (1 << CS00));	/* set clock source bits as
											   zero to stop timer */
			}
			wdt_reset();
		}

	}
}


/***** 16 bit timer/counter1 *****/
/** Argument to this function is in multiples of one minute **/
	 
void start_timer1(unsigned int delay)
{
	timer1_delay = delay * ITERATIONS_FOR_ONE_MINUTE_COUNTER1;
	TIMSK |= ( 1 << TOIE1);
	TCNT1 = INITIALIZE_COUNTER1;					
	TCCR1B |= (1 << CS12) | (~(1 << CS11)) | (1 << CS10);		/** select prescaling of 1024 **/
	timer1_expired = 0;
	tmr1_count = 0;
	wdt_reset();
}

	
void stop_timer1(void)
{
	TCCR1B &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));		/* set clock source bits as zero to stop timer */
	TIMSK &= ~( 1 << TOIE1);
	wdt_reset();
}

/***************************
 * ISR for Timer1 overflow. 
 ***************************/
ISR(TIMER1_OVF_vect)
{
	TCNT1 = INITIALIZE_COUNTER1;			
	tmr1_count++;
	if(tmr1_count >= timer1_delay)
	{
		timer1_expired=1;
		tmr1_count=0;
		TCCR1B &= ~((1 << CS12) | (1 << CS11) | (1 << CS10));	/* set clock source bits as zero to stop timer */
		TIMSK &= ~( 1 << TOIE1);
		wdt_reset();
	}
}


/********************************
 * 8 bit timer/counter2 with PWM. 
 *********************************/
	
void start_timer2(unsigned int sectors1)
{
	TCCR2 &= 0x00;   /* Stop timer */
	timer2_val = ITERATIONS_FOR_ONE_SECOND_COUNTER2 * sectors1;         
	TCNT2 = INITIALIZE_COUNTER2;
	TCCR2 |= (1 << CS22) | (1 << CS20);		/* select prescalar of 1024 */
	TIMSK |= (1 << TOIE2);
	timer2_expired = 0;
	wdt_reset();
}


	
void stop_timer2(void)
{
	tick2 = 0;
	TIMSK &= (~(1 << TOIE2));
	TCCR2 &= 0x00;   /* Stop timer */
	wdt_reset();
}


/*************************** 
 * ISR for timer2 overflow. 
 ***************************/
ISR(TIMER2_OVF_vect)
{
	TCNT2 = INITIALIZE_COUNTER2;
	tick2++;
	if(tick2 >= timer2_val)
	{
		timer2_expired = 1;
		tick2 = 0;
		TIMSK &= (~(1 << TOIE2));
		TCCR2 &= 0x00;   /* Stop timer */
	}
	if(((tick2 % ITERATIONS_FOR_ONE_SECOND_COUNTER2) == 0) && (!timer2_expired))
	{
		wdt_reset();
	}
}

	
void init_interrupt(void)
{
	SET_PIN(DDRD, RI);
	PORTD |= (1 << PD3);
	SET_PIN(DDRC, PC5);
	SET_PIN(DDRC, PC0);
	EICRA = 0;
	/* Set the interrupts as Edge Triggered */
	EICRA |= (1 << ISC11);
	EICRA |= (1 << ISC01) | (1 << ISC00);
	EICRB = 0;
	EICRB |= (1 << ISC71) | (1 << ISC70) | (1 << ISC60);
	/* Unmask interrupts
	 * INT0 for DTMF, INT1 for Ring Indicator, INT7 for audio stop indicator, INT6 for
	 * theft Detection
	 */
	EIMSK |= (1 << INT0) | (1 << INT1) | (1 <<  INT7) | (1 << INT6);
	/* Enable Interrupts */
	SREG |= (1 << 7);
	MCUCR = 0;
	SET_PIN(PORTD, PD1);// ENABLE PULL UP
	//PORTD1=1;// enable pull up on GSM_RING signal

}

	
void send_to_gsm_string(char *s)
{
	wdt_reset();
	_delay_ms(1);
	send_serial_string(s);
	send_serial('\r');
	send_serial('\n');
	wdt_reset();
}

	
void init_hw(void)
{
	init_mcu_ports();
	init_interrupt();
	sei();
	init_serial();
	init_audio();
	init_adc();

	disp_device_info();


	timer3_hours = my_eeprom_read_byte((unsigned char *)START_TIMR3_DELAY);
	start_timer3(timer3_hours); /*8 hour reboot is brought back,using the inbult timer3 of the controller. */ 
#if AUTOMATE
	automation_code = my_eeprom_read_byte((unsigned char *)AUTOMATION_CODE);
#endif

#if PH_REV_TEST
	read_eeprom(); /* Read important device parameters from EEPROM at boot time */
	while(1)
	{
		SET_PIN(LED_CTRL_PORT,AUTO_LED);
		SET_PIN(LED_CTRL_PORT,FAV_LED);
		check_phase_rev();
	}
#endif

	init_gsm();
	delete_all_sms(); 
}

void disp_device_info(void)
{


	unsigned int high = 0,low=0,lock=0,ext=0;
	unsigned char buffer[10];
	unsigned char c=0,i=0;

	send_serial0_string((char *)"\n\n\rAPP_SW_VER - ");
	send_serial0_string((char *)VERSION);

	my_eeprom_read((void *)serial_number, (const void *)EESER_NUM, NDIGIT_SER_NUM);
	serial_number[NDIGIT_SER_NUM] = '\0';

	send_serial0_string((char *)"\n\n\rSER_NO - ");
	send_serial0_string((char *)serial_number);


	/*send_serial0_string_debug("\n\nIMEI - ");
	  send_to_gsm_string("AT+GSN");
	  recv_response(100);
	  response[20] = '\0';
	  send_serial0_string_debug(response);
	  */

	//Fuse values are saved in DECIMAL no. format
	_delay_ms(1);	 //DELAY to complete operation
	low=boot_lock_fuse_bits_get(GET_LOW_FUSE_BITS);
	_delay_ms(1);
	high=boot_lock_fuse_bits_get(GET_HIGH_FUSE_BITS);
	_delay_ms(1);
	lock=boot_lock_fuse_bits_get(GET_LOCK_BITS);
	_delay_ms(1);
	ext=boot_lock_fuse_bits_get(GET_EXTENDED_FUSE_BITS);
	_delay_ms(1);

	send_serial0_string((char *)"\n\n\rEXT    - ");
	itoa (ext,(char *)buffer,16);
	for(i=0;i<4;i++)
	{
		c=buffer[i];
		buffer[i] = toupper(c);
	}
	send_serial0_string((char *)buffer);

	send_serial0_string((char *)"\n\n\rHIGH  - ");
	itoa (high,(char *)buffer,16);
	for(i=0;i<4;i++)
	{
		c=buffer[i];
		buffer[i] = toupper(c);
	}
	send_serial0_string((char *)buffer);

	send_serial0_string((char *)"\n\n\rLOW  - ");
	itoa (low,(char *)buffer,16);
	for(i=0;i<4;i++)
	{
		c=buffer[i];
		buffer[i] = toupper(c);
	}
	send_serial0_string((char *)buffer);

	send_serial0_string((char *)"\n\n\rLOCK - ");
	itoa (lock,(char *)buffer,16);
	for(i=0;i<4;i++)
	{
		c=buffer[i];
		buffer[i] = toupper(c);
	}
	send_serial0_string((char *)buffer);

	send_serial0_string((char *)"\n\n\rDONE...");


}


char check_phase_rev(void)
{
	unsigned int y_time_val[2] = {'\0'};
	int i = 0;
	unsigned int mul_value = 0,mul_value_1=0,mul_value_2=0;
	const int min_volt = 20;
	const int min_low_val = 5,max_low_val = 8;
	char y_phs_proper = 0,b_phs_proper = 0;
#if PH_REV_TEST
	char k = 0;
#endif


	if(phase_rev_pro_enable != 1)//newly added
	{
		return 2;
	}


	if(adc_conv(SMPS_OUTPUT_PIN) < MIN_SMPS_VOLTAGE)
	{
		// smps is zero, either phase R or phase B is cut.
		return 2;
	}


	if((adc_conv(VOLTAGE_PHASE_R) < MIN_PHASE_VOLTAGE) \
			|| (adc_conv(VOLTAGE_PHASE_Y) < MIN_PHASE_VOLTAGE) \
			|| (adc_conv(VOLTAGE_PHASE_B) < MIN_PHASE_VOLTAGE) ) 
	{
		// any one phase is zero, it is wirecut
		return 2;
	}

	//start_timer(1);
	i=0;
	start_timer2(5);

	while(timer2_expired == 0)
	{
		if(adc_conv_ph_rev(VOLTAGE_PHASE_R) > min_volt)
		{
			y_phs_proper = 0;
			b_phs_proper = 0;

			while(adc_conv_ph_rev(VOLTAGE_PHASE_R) > min_volt)
			{
				if(timer2_expired == 1)
				{
					return 2;
				}
			}
			while(adc_conv_ph_rev(VOLTAGE_PHASE_R) <= min_volt)
			{
				if(timer2_expired == 1)
				{
					return 2;
				}
			}
			start_timer(1);
			if(adc_conv_ph_rev(VOLTAGE_PHASE_Y) > min_volt)
			{
				while(adc_conv_ph_rev(VOLTAGE_PHASE_Y) > min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}
				while(adc_conv_ph_rev(VOLTAGE_PHASE_Y) <= min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}
			}
			else
			{
				while(adc_conv_ph_rev(VOLTAGE_PHASE_Y) <= min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}

			}
			y_time_val[0] = mili_sec_counter;
			start_timer(1);
			if(adc_conv_ph_rev(VOLTAGE_PHASE_B) > min_volt)
			{
				while(adc_conv_ph_rev(VOLTAGE_PHASE_B) > min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}
				while(adc_conv_ph_rev(VOLTAGE_PHASE_B) <= min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}
			}
			else
			{
				while(adc_conv_ph_rev(VOLTAGE_PHASE_B) <= min_volt)
				{
					if(timer2_expired == 1)
					{
						return 2;
					}
				}

			}
			y_time_val[1] = mili_sec_counter;

			for(i=0;i<2;i++)
			{

				if(y_time_val[i] < 10)
				{
					if((y_time_val[i] >= min_low_val) && (y_time_val[i] <= max_low_val) )
					{
						// phases are proper
						if(i == 0)
							y_phs_proper = 1;
						else
							b_phs_proper = 1;

					}
					else
					{
						// phase is not proper
						if(i == 0)
							y_phs_proper = 0;
						else
							b_phs_proper = 0;


					}
				}

				else if(y_time_val[i] < 100)
				{
					mul_value = (y_time_val[i]/10);
					if((mul_value % 2) == 0 )
					{

						if((y_time_val[i] >= (min_low_val +(10 * mul_value))) \
								&& (y_time_val[i] <= (max_low_val +( 10 * mul_value))))
						{
							// phases are proper

							if(i == 0)
								y_phs_proper = 1;
							else
								b_phs_proper = 1;
						}
						else
						{
							if(i == 0)
								y_phs_proper = 0;
							else
								b_phs_proper = 0;

						}
					}
					else
					{
						if(i == 0)
							y_phs_proper = 0;
						else
							b_phs_proper = 0;

					}
				}
				else if(y_time_val[i] < 1000)
				{
					//	print_ssd_delay(NULL,y_time_val[0],500);
					mul_value_1 = (y_time_val[i]/100);
					mul_value_2 = (y_time_val[i] % (mul_value_1 * 100));
					mul_value = (mul_value_2/10);

					if((mul_value % 2) == 0 )
					{

						if((mul_value_2 >= (min_low_val +(10 * mul_value))) \
								&& (mul_value_2 <= (max_low_val +( 10 * mul_value))))
						{
							// phases are proper
							if(i == 0)
								y_phs_proper = 1;
							else
								b_phs_proper = 1;
						}
						else
						{
							if(i == 0)
								y_phs_proper = 0;
							else
								b_phs_proper = 0;
						}
					}
					else
					{
						if(i == 0)
							y_phs_proper = 0;
						else
							b_phs_proper = 0;

					}
				}
			}
			if((y_phs_proper == 0) && (b_phs_proper == 0))
			{
				// sequence is wrong
#if PH_REV_TEST
				wdt_reset();
				for(k=0;k<10;k++)
				{

					CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
					_delay_ms(50);
					SET_PIN(LED_CTRL_PORT,AUTO_LED);
					_delay_ms(50);
				}
				wdt_reset();
#endif			
				return 0;

			}
			else
			{
				//sequence is correct
#if PH_REV_TEST
				wdt_reset();
				for(k=0;k<10;k++)
				{

					CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
					_delay_ms(50);
					SET_PIN(LED_CTRL_PORT,FAV_LED);
					_delay_ms(50);
				}
				wdt_reset();
#endif			
				return 1;
			}
		}
	}
	return 2;
}

void init_mcu_ports(void)
{
	/** configure start and stop motor triggering pins as output pins **/ 
	SET_PIN(DDRA, MOTOR_CTRL_STRT);
	SET_PIN(DDRA, MOTOR_CTRL_STP);

	CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STP);
	CLEAR_PIN(MOTOR_CTRL_PORT, MOTOR_CTRL_STRT);

	/** configure pins used for AUTO_LED and FAV_LED as output pins **/ 
	SET_PIN(DDRD, AUTO_LED);              
	SET_PIN(DDRD, FAV_LED);                

	/** configure pins PC4 to PC7 as input lines for detecting dtmf tones **/
	DDRC &= 0x0F;

	/* Set Bypass pins as input */
	CLEAR_PIN(DDRE, PE5); 



#if THREE_BUTTON
	/* Set flow sensor pins as output */
	//CLEAR_PIN(DDRE, PE4);
	SET_PIN(DDRE, PE4);
	CLEAR_PIN(PORTE, PE4); 
#else
	/* Set flow sensor pins as input */
	CLEAR_PIN(DDRE, PE4); 
#endif

	/* configure ring indicator as input pin */
	CLEAR_PIN(DDRD, PD1); 

	/* Make theft pin as an input */
	CLEAR_PIN(DDRE, PE6); 

	/* configure GSM ON/OFF pin as output pin */	
	SET_PIN(DDRD, PD7);

	/* Set GSM STATUS pin as input */
	CLEAR_PIN(DDRD, PD6);

	/* CONFIGURE UN-USED PINS AS OUTPUT PINS **/	
	SET_PIN(DDRA, PA2);
	SET_PIN(DDRA, PA3);
	SET_PIN(DDRA, PA4);
	SET_PIN(DDRA, PA5);
	SET_PIN(DDRA, PA6);
	SET_PIN(DDRA, PA7);
	SET_PIN(DDRC, PC0);
	SET_PIN(DDRC, PC1);
	SET_PIN(DDRC, PC2);
	SET_PIN(DDRC, PC3);
	SET_PIN(DDRG, PG0);
	SET_PIN(DDRG, PG1);
	SET_PIN(DDRE, PE2);
	SET_PIN(DDRE, PE3);
}


void Delay_sec(unsigned int sec)
{
	if(sec > 30)
	{
		sec = 30;
	}

	start_timer2(sec);
	while(!timer2_expired)
		;
	stop_timer2();
}


/****************************************************************************
 * This function is for reading from eeprom without disabling watchdog timer. 
 ****************************************************************************/ 
void my_eeprom_read(void *val, const void *addr, size_t nbytes)
{
	//start_timer2(60);
	cli();
	eeprom_busy_wait();
	eeprom_read_block(val, addr, nbytes);
	eeprom_busy_wait();
	sei();
	//stop_timer2();
}


/**************************************************************************
 * This function is for writing to eeprom without disabling watchdog timer. 
 **************************************************************************/ 
void my_eeprom_write(const void *val, void *addr, size_t nbytes)
{
	cli();
	eeprom_busy_wait();
	eeprom_write_block(val, addr, nbytes);
	eeprom_busy_wait();
	sei();
}

/* Timer3 used for getting a delay for hours */

 
void start_timer3(unsigned int delay)
{
	if(delay == 0) // if delay is Zero then it means that periodic timer is disabled
	{
		stop_timer3();
		return;
	}

	timer3_delay = (delay * ITERATIONS_FOR_ONE_HOUR_COUNTER3);
	ETIMSK |= ( 1 << TOIE3);
	TCNT3 = INITIALIZE_COUNTER3;					
	TCCR3B |= (1 << CS32) | (1 << CS30);		/* select prescalar of 1024 */
	timer3_expired = 0;
	tmr3_count = 0;
	wdt_reset();
}

	
void stop_timer3(void)
{
	TCCR3B &= 0x00;   /* Stop timer */
	// or try this TCCR3B &= ~((1 << CS32) | (1 << CS30));	/* set clock source bits as zero to stop timer */
	ETIMSK &= ~( 1 << TOIE3);
	wdt_reset();
}

/***************************
 * ISR for Timer3 overflow. 
 ***************************/
ISR(TIMER3_OVF_vect)
{
	TCNT3 = INITIALIZE_COUNTER3;			
	tmr3_count++;
	if(tmr3_count >= timer3_delay)
	{
		TCCR3B &= 0x00;   /* Stop timer */
		ETIMSK &= ~( 1 << TOIE3);
		if((hw.motor == M_Started) && (run_in_which_phase == TWO_PHASE))
		{
			timer3_expired = 0;
			tmr3_count = 0;

			if(timer3_hours == 0) // if delay is Zero then it means that periodic timer is disabled
			{
				return;
			}
			else
			{

				timer3_delay = (timer3_hours * ITERATIONS_FOR_ONE_HOUR_COUNTER3);
				ETIMSK |= ( 1 << TOIE3);
				TCNT3 = INITIALIZE_COUNTER3;					
				TCCR3B |= (1 << CS32) | (1 << CS30);		/* select prescalar of 1024 */
			}
			//timer3_expired = 0;
			//tmr3_count = 0;

		}
		else
		{	
			per_reboot = 1;
			my_eeprom_write_byte((unsigned char)per_reboot, (unsigned char *)EE_PER_REBOOT);
			_delay_ms(5000);// controller will get rebooted
		}

	}
}


void my_eeprom_write_byte(unsigned char val,unsigned char *addr)
{
	cli();
	eeprom_busy_wait();
	eeprom_write_byte(addr,val);
	eeprom_busy_wait();
	sei();
}


unsigned char my_eeprom_read_byte(unsigned char *addr)//, unsigned char val)
{
	unsigned char val =0;
	cli();
	eeprom_busy_wait();
	val = eeprom_read_byte((unsigned char*)addr);
	eeprom_busy_wait();
	sei();
	return val;
}

void my_eeprom_write_int(unsigned int val,unsigned int *addr)
{
	cli();
	eeprom_busy_wait();
	eeprom_write_word(addr,val);
	eeprom_busy_wait();
	sei();
}


unsigned int my_eeprom_read_int(const unsigned int *addr)
{
	unsigned int val =0;
	cli();
	eeprom_busy_wait();
	val = eeprom_read_word((const unsigned int*)addr);
	eeprom_busy_wait();
	sei();
	return val;
}

void blink_auto_led(void)
{
	CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
	wdt_reset();
	_delay_ms(500);
	wdt_reset();
	SET_PIN(LED_CTRL_PORT,AUTO_LED);
	wdt_reset();
	_delay_ms(500);
	wdt_reset();
}
