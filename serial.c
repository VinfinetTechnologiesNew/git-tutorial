/*
 * Description  :This file contains the main() function.
 *
 * Copyright 2010 (C) Vinfinet
 *
 * Author       : Prem Mallappa (prem.mallappa@gmail.com)
 * Author       : Pradeep.A.D  (pradeep@vinfinet.com)
 * Author       : Agnesh  (agnesh@vinfinet.com)
 * Author       : Tilak BG (tilak@vinfinet.com)
 * Author       : K Pradip reddy (pradeepreddy.k@vinfinet.com)
 *
 * History      : Updated last on 05 December 2011
 */

#include "fns.h"

unsigned int serial_var;

void init_serial(void)
{
	UBRR1H = 0x00;  				//baud rate
	UBRR1L = 51;    				//baud rate  = 9600
	UCSR1B = (1 << TXEN1) | (1 << RXEN1);   	//enable the transmit
	UCSR1C = (1 << UCSZ11) | (1 << UCSZ10);     // 8 data bits
	UCSR1A = 0;

	UBRR0H = 0x00;  				//baud rate
	UBRR0L = 51;    				//baud rate  = 9600
	UCSR0B = (1 << TXEN0) | (1 << RXEN0) | (1 << RXCIE0);   	//enable the transmit
	UCSR0C = (1 << UCSZ01) | (1 << UCSZ00);     		// 8 data bits
	UCSR0A = 0;
}

void send_serial0(char a)
{
	while (!( UCSR0A & (1<< UDRE0)));
	UDR0  = a;

	wdt_reset();
}

void send_serial0_automate(char a)
{
#if AUTOMATE
	while (!( UCSR0A & (1<< UDRE0)));
	UDR0  = a;
#endif
	wdt_reset();
}



char recv_serial0(char trials)
{
	unsigned char b = 0;
	unsigned int temp=0;

	while (!(UCSR0A & (1 << RXC0)) && serial_var)
	{
		temp++;
		serial_var--;
		if (!serial_var && b < trials)
		{
			wdt_reset();
			serial_var = 65000;
			b++;
		}
		if (temp==500)
		{
			temp=0;
			wdt_reset();
		}
	}
	return UDR0;
}


void send_serial0_string(char *a)
{
	int i;
	for (i = 0; a[i] !='\0'; i++)
	{
		if(i > MAX_RESP_LEN)
		{
			break;
		}
		send_serial0(a[i]);
	}
}


void send_serial(char a)
{
	while (!( UCSR1A & (1<< UDRE1)));
	UDR1  = a;
	_delay_ms(1);
	wdt_reset();
}



char recv_serial(char trials)
{
	unsigned char b = 0;
	unsigned int temp=0;

	while (!(UCSR1A & (1 << RXC1)) && serial_var)
	{
		temp++;
		serial_var--;
		if (!serial_var && b < trials)
		{
			wdt_reset();
			serial_var = 65000;
			b++;
		}
		if (temp==500)
		{
			temp=0;
			wdt_reset();
		}
	}
	return UDR1;
}



void send_serial_string(char *a)
{
	int i;
	for (i = 0; a[i] !='\0'; i++)
	{
		if(i > MAX_RESP_LEN)
		{
			break;
		}
		send_serial(a[i]);
	}
}

