/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"

unsigned int serial_var;
unsigned char response[MAX_RESP_LEN];
unsigned char debugging_enable,calib_flag,investigate_flag;

	
unsigned char notify(void)
{
	unsigned char a=1, clk[5], local_peak_time_flag = 0;
	int cmp_ret;

	/** [Generic comments] Begin code to handle non-peak time alerts   ****/
	/**[Technical comments] check if non-peak time alerts feature is enabled **/
	if(nonpeaktime_alerts_disable && (theft_detected == 0))
	{   
		get_clock(clk);
		recv_response(WAIT_GSM_RESPONSE);
		Delay_sec(GSM_DELAY);
		cmp_ret = strcmp((const char *)nonpeak_start,(const char *) nonpeak_end); 
		if(cmp_ret > 0)
		{
			if((strcmp((const char *)clk, (const char *)nonpeak_start)>=0) \
					|| (strcmp((const char *)clk, (const char *)nonpeak_end)<=0))
				local_peak_time_flag = 1;
		}
		else if(cmp_ret < 0)
		{
			if((strcmp((const char *)clk, (const char *)nonpeak_start)>=0) \
					&& (strcmp((const char *)clk, (const char *)nonpeak_end)<=0))
				local_peak_time_flag = 1;
		}   

		if(local_peak_time_flag == 1)
		{
			//print_ssd_delay("PEAK",0,PRINT_DELAY);
			hw.cond = check_favourable();
			prv_status.cond = hw.cond;
			prv_status.motor = hw.motor;
			return 0;
		}
	}
	/** [Generic comments] End of code to handle non-peak time alerts  ****/

	if(0 == (check_creg()))
	{
		Delay_sec(2);
		if( (0 == (check_creg())))
		{
			print_ssd_delay("REGF",0,500);
			send_to_gsm_string("AT+CPOWD=1");       // power off the gsm.
			recv_response(100);
			Delay_sec(5);
			switchon_gsm();	
			//switch_off_gsm();
		}
	}
	print_ssd_delay("\n\r1111",0,PRINT_DELAY);
	if(!inform_user())
		return 0;

	start_timer(MAX_RINGING_TIME);
	do	/* If the farmer number is BUSY or there is NO CARRIER or 
		   NO DIALTONE try intimating the farmer two times and then SMS */
	{
		recv_response(SHORT_WAIT_GSM_RESPONSE);
		a=strcmp((const char*)response,"OK");
		if(a && (!strcmp((const char*)response,"NO CARRIER")\
					||!strcmp((const char*)response,"BUSY")\
					||!strcmp((const char*)response,"NO DIALTONE")\
					||!strcmp((const char*)response,"ERROR")||timer_expired))
		{
			disconnect_call();
			stop_timer();
			print_ssd_delay((char*)response,0,PRINT_DELAY);
			print_ssd_delay("\n\r2222",0,PRINT_DELAY);
			Delay_sec(LONG_GSM_DELAY);
			inform_user();
			start_timer(MAX_RINGING_TIME);
			do	/* second try */
			{
				recv_response(SHORT_WAIT_GSM_RESPONSE);
				a=strcmp((const char*)response,"OK");
				if(a && (!strcmp((const char*)response,"NO CARRIER")\
							||!strcmp((const char*)response,"BUSY")\
							||!strcmp((const char*)response,"NO DIALTONE")\
							||!strcmp((const char*)response,"ERROR")||timer_expired))
				{
					disconnect_call();
					stop_timer();
					print_ssd_delay((char*)response,0,PRINT_DELAY);
					if(notify_sms_enable == 1)
					{
						print_ssd_delay("3333",0,PRINT_DELAY);
						Delay_sec(LONG_GSM_DELAY);
						send_sms(ph[0],msg_info); 	/** send sms, 
										  if the farmer has not answered 
										  the call twice **/
						recv_response(WAIT_GSM_RESPONSE);
						Delay_sec(GSM_DELAY);
					}
					prv_status.cond = hw.cond;
					prv_status.motor = hw.motor;
					return 0;
				}
			}while(a);
		}
	}while(a);

	stop_timer();
	return 1; 		/* 1 is returned only if farmer answers the call */
}
/* This function will inform the status of kisan raja to the farmer based 
 * on the communication level chosen by the farmer */
	
unsigned char inform_user(void)
{
	char num[NDIGIT_PH_NUM+5];

	if(msg_flag==1)	/* If communication level is MESSAGE_ONLY, then send message to the farmer */
	{
		Delay_sec(GSM_DELAY);
		send_sms(ph[0], msg_info);
		prv_status.cond = hw.cond;
		prv_status.motor = hw.motor;
		return 0;
	} 
	/* If communication level is set for only emergency calls, then inform only for theft and wirecut cases */
	else if(comm_level_flag && (theft_detected == 0)) 
	{
		if(hw.err != E_WireCut) /**  inform for wirecut **/
		{
			prv_status.cond = hw.cond;
			prv_status.motor = hw.motor;
			return 0;
		}
	}   

	num[0]='+';
	num[1]='9';
	num[2]='1';
	strncpy(num+3,(char *)ph,NDIGIT_PH_NUM);
	num[NDIGIT_PH_NUM+3]=';';
	num[NDIGIT_PH_NUM+4]='\0';

	make_call((char *)num);

	return 1;      /*1 is returned if a call is made to the farmer*/
}

	unsigned char
recv_response(char trials)
{
	int i = 0;
	unsigned char a = 0;
	wdt_reset();
	/*	for(j=0;j<=MAX_RESP_LEN;j++)
		{
		response[j] = '\0';
		}*/
	strcpy((char*)response, "no response");

	do
	{
		serial_var = POLL_FOR_GSM_RESPONSE;
		a = recv_serial(trials);
	} while ((a == '\n' || a == '\r') && serial_var);

	if(!serial_var)
		return 1;

	response[i++] = a;
	do
	{
		serial_var = POLL_FOR_GSM_RESPONSE;
		a = recv_serial(trials);
		response[i++] = a;

		if(i == (MAX_RESP_LEN-1))
			break;
	} while(((a != '\n') && (a != '\r')) && serial_var);

	response[i-1] = '\0';
	wdt_reset();

	if (!serial_var)
		return 1;
	else
		return 0;
}

/*
 * This function has been specifically written to capture the response of command 
 * "AT+CUSD=1,\"----\",15" (i.e to receive sim balance information).
 * The generic function recv_response() will not work in this case because the 
 * response of the above AT command contains \n and \r within it. 
 * recv_response() will only read the response which starts with \r (or) \n and ends with \r (or) \n.
 *
 * This function will read the entire resposne from GSM without looking for terminating \r\n characters.
 */

unsigned char recv_sim_balance(char trials)
{
	int i = 0;
	unsigned char a = 0;

	response[i] = '\0';

	do
	{
		serial_var = POLL_FOR_GSM_RESPONSE;
		a = recv_serial(trials);
	}while ((a == '\n' || a == '\r') && serial_var);

	if (!serial_var)
		return 1;

	response[i++] = a;
	do
	{
		serial_var = POLL_FOR_GSM_RESPONSE;
		a = recv_serial(trials);
		response[i++] = a;
	}while(serial_var && (i<(MAX_RESP_LEN-1)));

	response[i] = '\0';
	if (!serial_var)
		return 1;
	else 
		return 0;
}

	
void check_number(void)
{
	unsigned char i=0, j=0, a=0;

	Delay_sec(1);

	do		/* Wait for Caller Number */
	{
		a = recv_response(100);
		j++;
	}while (j < 3 && strncmp((const char*)response, "+CLIP", 5) && !a);

	if(a == 1)
		return;
	else if(j >= 3)			/* No Caller Number Found */
	{
		a = 0;
		do
		{
			a = recv_response(100);
		} while (strcmp((const char*)response, "RING") && a);

		if(a != 1)
		{
			/* Still Ringing, so pick-up the call */
			print_ssd_delay("ERR1", 0, 500);
			return;
		}
	}

	/* Extract the caller's number */
	while(response[i++] != '\"')
	{
		if (response[i - 1] == '\0')
			return;
		
		if(i >= MAX_RESP_LEN)
			return;
	}

	/* To trim '+91' at the beginning of response string */
	j = 0;
	i = i + 3;
	do
	{
		caller_number[j++] = response[i++];
	} while((response[i] != '\"') && (j<10));

	caller_number[j] = '\0'; 
}


/*******************************************************************************************************************
 * This function is to monitor battery voltage. In case of low battery, send sms to the farmer and shutdown the gsm.
 *******************************************************************************************************************/
#if 0
void battery_low_sensing(void)
{
	unsigned int val; 
	val = adc_conv(BATTERY_VOLTAGE_PIN);

	if((val <= THRESHOLD_BATTERY_VOLTAGE) && (informed == 0) ) // threshold voltage is 3.4v
	{
		strcpy((char *)msg_info,"BATTERY IS LOW, PLEASE RECHARGE THE BATTERY\n");

		Delay_sec(GSM_DELAY);
		send_sms(ph[0],msg_info);   /*send sms to user */
		informed = 1;
		my_eeprom_write((const void *)&informed,(void *)EE_BATRY_INFO, 1);// switch off the motor from call 0.
	}
	else if((val > NORMAL_BATT_VOLTAGE)) // normal voltage means more than 3.8 volts
	{
		informed = 0;
		my_eeprom_write((const void *)&informed,(void *)EE_BATRY_INFO, 1);// switch off the motor from call 0.
	}

}

#endif

//*25#
unsigned char signal_strength(char *buff)
{
	
	unsigned char i = 0, j = 0;
	char num_count = 0;
	
	wdt_reset();
	send_to_gsm_string("AT+CSQ");
	recv_response(WAIT_GSM_RESPONSE);
	if(strcmp((const char*)response, "no response") == 0)
	{
		strcpy(buff, "no response");
		return 0;
	}
	while((response[i] != ':') && (response[i] != '\0'))
	{
		if(i >= MAX_RESP_LEN)
		{
			return 0;
		}

		i++;
	}

	i++;

	/* after detection of :, maximum digits will be only 2*/
	while((response[i] != ',') && (response[i] != '\0'))
	{  
		if((i >= MAX_RESP_LEN) || (num_count > 2))
		{
			return 0;
		}	
		buff[j++] = response[i++];
		num_count++;
	}  
  
	buff[j] = '\0';
	wdt_reset();
	return 1;

}

