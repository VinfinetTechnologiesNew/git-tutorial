/*****************************************************************
 * Copyright 2012 (C) Vinfinet
 *
 * Author	: K Pradip Reddy (pradeepreddy.k@vinfinet.com)
 * Author       : Shiva Kumar (shivakumar@vinfinet.com)
 * Author       : Pallavi B S (pallavibs@vinfinet.com)
 *
 ******************************************************************/

#include "fns.h"
unsigned char call_status ;       /*this variable is set when device is making the call and reset 
				    *when that call is disconnected*/

void switchon_gsm(void)
{
	wdt_reset();
	if(0 == GSM_STATUS_PIN)
	{
		Delay_sec(2);
		if(0 == GSM_STATUS_PIN)
		{
			print_ssd_delay("GSM ",0,500);
			SET_PIN(DDRD, PD7);	/* configure GSM ON/OFF pin as output pin */
			SET_PIN(PORTD, PD7);
			Delay_sec(3);		
			CLEAR_PIN(PORTD, PD7);
			CLEAR_PIN(DDRD, PD7);	/** once the GSM is powered ON, 
						  we are making the GSM trigger pin as input pin to avoid unwanted 
						  GSM shutdown **/
			Delay_sec(5);

			init_gsm_settings();

			CLEAR_PIN(DDRD, PD7);	/** once the GSM is powered ON, 
						  we are making the GSM trigger pin as input pin to avoid unwanted 
						  GSM shutdown **/
		}
	}
	wdt_reset();
}

void init_gsm_settings(void)
{
	int i=0;

	recv_response(WAIT_GSM_RESPONSE);
	for(i=0;i<5;i++)
	{
		send_serial0_automate('.');
		CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
		send_to_gsm_string("AT");
		Delay_sec(1);

		send_serial0_automate('.');
		SET_PIN(LED_CTRL_PORT,FAV_LED);
		send_to_gsm_string("AT");
		Delay_sec(1);
	}

	i=0;
	for(i=0;i<2;i++)
	{	
		send_serial0_automate('.');
		CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
		send_to_gsm_string("AT+IPR=9600");
		Delay_sec(1);

		SET_PIN(LED_CTRL_PORT,FAV_LED);
		send_serial0_automate('.');
		send_to_gsm_string(ECHO_OFF);
		Delay_sec(1);

		send_serial0_automate('.');
		SET_PIN(LED_CTRL_PORT,FAV_LED);
		send_to_gsm_string("AT&W");
		Delay_sec(1);
	}

	CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("ATH");
	Delay_sec(1);

	SET_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+CNMI=1");
	Delay_sec(1);

	CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string(CLI);
	Delay_sec(1);

	SET_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string(MSG_FRMT);
	Delay_sec(1);

	CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+COLP=1");
	Delay_sec(1);

	SET_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+CFUN=1");
	Delay_sec(1);

	CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+CSDT=0");// disable detecting sim card
	Delay_sec(1);

	SET_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+CLVL=80");
	Delay_sec(1);

	CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT+CMIC=0,2");
	Delay_sec(1);

	SET_PIN(LED_CTRL_PORT,FAV_LED);
	send_serial0_automate('.');
	send_to_gsm_string("AT&W");
	Delay_sec(1);

	send_serial0_automate('\n');
	send_serial0_automate('\r');
	print_ssd_delay("GSM_OK",0,500);
}



void init_gsm(void)
{
	int i=0;
	if(0 == GSM_STATUS_PIN)
	{
		_delay_ms(2000);		
		if(0 == GSM_STATUS_PIN)
		{
#if AUTOMATE
			if(automation_code == 1)
			{
				send_serial0('\n');
				send_serial0('\r');
				for(i=0;i<80;i++)
				{
					send_serial0('*');
				}
				print_ssd_delay("GSM WAIT..",0,500);
			}
#else
			print_ssd_delay("GSM",0,500);
#endif
			send_serial0_automate('\n');
			SET_PIN(DDRD, PD7);	/* configure GSM ON/OFF pin as output pin */
			SET_PIN(PORTD, PD7);
			_delay_ms(3000);		
			CLEAR_PIN(PORTD, PD7);
			CLEAR_PIN(DDRD, PD7);	/** once the GSM is powered ON, 
						  we are making the GSM trigger pin as input pin to avoid unwanted 
						  GSM shutdown **/
			/* after the GSM module is power up for the next 2 minutes do not send any AT commands */

			for(i=0;i<30;i++ )
			{
				CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
				send_serial0_automate('\r');
				print_ssd_delay("GSM0",0,500);
				SET_PIN(LED_CTRL_PORT,FAV_LED);
				print_ssd_delay("GSM1",0,500);
			}

			CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
			init_gsm_settings();	
		}
	}
	CLEAR_PIN(DDRD, PD7);	/** once the GSM is powered ON, 
				  we are making the GSM trigger pin as input pin to avoid unwanted GSM shutdown **/
}

	
unsigned char check_creg(void)
{
	wdt_reset();
	int i=0,j=0; 
	char creg[2]={0};


	print_ssd_delay("    ",0,50);
	recv_response(10); 
	print_ssd_delay("    ",0,50);
	send_to_gsm_string("AT+CREG?\0");    
	recv_response(100);
	print_ssd_delay("    ",0,50);

	i = 9; 	// to remove +creg: from the response 
	while(response[i] != '\0')
	{  
	       if(i >= MAX_RESP_LEN)
	       {
		       return 1; // as there is no data in buffer, dont decide the error case

	       }	       
		creg[j++]=response[i++];
	}   

	creg[j]='\0';
	print_ssd_delay("    ",0,50);
	/* sim card not registered or no network */
	if((strcmp(creg,"0") == 0) || (strcmp(creg,"2")==0) || (strcmp(creg,"3") == 0) || (strcmp(creg,"4")==0)) 
	{
		return 0;
	}

	wdt_reset();
	return 1;
} 


void check_sig_reg(void)
{
	//char buff[20]={0};
	//unsigned int signal =0;
	unsigned char low_signal=0,reg_fail=0,reboot=0,no_res=0,i =0,c=0;
	static unsigned int action =0,attempt=0;

	wdt_reset();

	switchon_gsm();

	if(byps == 1)
	{
		CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
		print_ssd_delay("BYPS",0,100);
		SET_PIN(LED_CTRL_PORT,AUTO_LED);
		print_ssd_delay("BYPS",0,100);
	}
	if(action == 0)
	{
		wdt_reset();
		action = 1;
		/*
		if((signal_strength(buff)) == 1) // 1 will be returned if function returns valid data
		{
			print_ssd_delay("    ",0,50);
			signal = atoi((const char *) buff);

			if((signal < signal_val) && ((signal != 0) ))
			{
				Delay_sec(2);
				print_ssd_delay("    ",0,50);
				recv_response(10);
				print_ssd_delay("    ",0,50);
				if((signal_strength(buff)) == 1) // 1 will be returned if function returns valid data
				{
					signal = atoi((const char *) buff);
					print_ssd_delay("    ",0,50);

					if((signal < signal_val) && ((signal != 0) ))
					{
						print_ssd_delay("LOWS",0,500);	
						low_signal=1; // confirmed that signal quality is less
					}
				}
			}

		}
		*/
	}
	else if(action == 1)
	{
		wdt_reset();
		action=2;
		if(0 == (check_creg()))
		{
			Delay_sec(2);
			print_ssd_delay("    ",0,50);
			recv_response(10);
			print_ssd_delay("    ",0,50);

			if( (0 == (check_creg())))
			{
				send_serial0_automate('\n');
				send_serial0_automate('\r');
				print_ssd_delay("REGF",0,500);	
				reg_fail=1; // confirmed that registration is failed
			}
		}
	}
	else if(action == 2)
	{
		action = 3;

		/*
		if(reboot_counter >= PER_DAY_REBOOT_TIME)
		{
			reboot_counter = 0; 	// default keep default reboot counter value to zero 
			my_eeprom_write_int((unsigned int)reboot_counter, (unsigned int *)EE_REBOOT_CNTR);
			reboot = 1;
		}
		*/
	}
	else if(action == 3)
	{

		wdt_reset();
		action = 0;
		no_res = 0;
		send_to_gsm_string("AT");
		recv_response(WAIT_GSM_RESPONSE);
		if(0 == strcmp((const char*)response,"OK"))
		{
			attempt=0;
		}
		else
		{
			attempt++;
		}

		if (attempt > 4) // confirm that module is not responding by checking 4 times.
		{
			no_res = 1;/* no response from GSM module ,may be GSM module hang,
					reboot the gsm module and controller*/
			print_ssd_delay("NORS",0,500);
		}

	}


	if((reg_fail==1) || (low_signal== 1) || (reboot == 1))
	{
		switch_off_gsm(); // switch off the GSM module when signal is low or registration is failed
	}
	else if((no_res == 1)) // this operation is performed when gsm is not giving any response.
	{
		// switch off the gsm module
		for(i=0;i<10;i++)
		{
			CLEAR_PIN(DDRD,PD6); //make GSM status pin as input
			SET_PIN(DDRD, PD7);	/* configure GSM ON/OFF pin as output pin */
			SET_PIN(PORTD, PD7);
			start_timer(20);//20 seconds
			while(timer_expired == 0) // loop untill timr is expired
			{
				if(GSM_STATUS_PIN == 0)
				{
					break; // break from while loop
				}
			}
			stop_timer();		
			CLEAR_PIN(PORTD, PD7);
			CLEAR_PIN(DDRD, PD7);	/** once the GSM is powered OFF, 
						  we are making the GSM trigger pin as input pin 
						  to avoid unwanted GSM shutdown **/

			Delay_sec(2);
			if(GSM_STATUS_PIN == 0)
			{
				break; // break from for loop
			}
		}

	}

	if((no_res == 1))/* reboot the controller also if 
			    it is not getting any response from GSM module,
			    it might be controller reciever problem */
	{
		
		c = 1;
		my_eeprom_write_byte((unsigned char)c,(unsigned char *)EE_PER_REBOOT);
		/* it is normal periodic reboot ,so dont inform the farmer.*/

		_delay_ms(5000);// controller will get rebooted

	}
	if(byps == 1)
	{
		CLEAR_PIN(LED_CTRL_PORT,AUTO_LED);
		print_ssd_delay("BYPS",0,100);
		SET_PIN(LED_CTRL_PORT,AUTO_LED);
		print_ssd_delay("BYPS",0,100);
	}
	wdt_reset();
}




void switch_off_gsm(void)
{
	char i=0;
#if AUTOMATE
	static char count =0;

	if((automation_code ==1) && (password_set ==1) && (count ==1))
	{
		while(1)
		{
			while_delay();
			print_ssd_delay("\rGSM-ERR",0,500);
			count =0;
		}
	}
	count++;
#endif
	wdt_reset();
	send_to_gsm_string("AT+CPOWD=1");       // power off the gsm.
	recv_response(100);
	Delay_sec(5);
	switchon_gsm();
	for(i=0;i<30;i++)
	{
		CLEAR_PIN(LED_CTRL_PORT,FAV_LED);
		print_ssd_delay("GOFF",0,100);		
		SET_PIN(LED_CTRL_PORT,FAV_LED);
		print_ssd_delay("GOFF",0,100);
		Delay_sec(1);	
		send_serial0_automate('\r');	
	}
	send_serial0_automate('\n');	
	send_serial0_automate('\r');	
}



	
unsigned char get_date(unsigned char *clk)
{
	int i=0,j=0;             
	send_to_gsm_string("AT+CCLK?\0");    
	recv_response(100);

	if(0 != strncmp((const char*)response,"+CCLK:",6))
	{
		return 1;
	}
	i = 8; 	/* to remove +cclk: from the response */ 
	while((response[i] != '\0') && (j <10))
	{   
		clk[j++]=response[i++];
		if(j%2 == 0)
			i++; 
	}   
	if((j < 10) && (response[i] == '\0')){
		return 1;
	}   

	clk[j]='\0';
	i=0;j=0;
	while((clk[j]!='\0') && (j<=5))
	{
		clk[i++]=clk[j++];
	}
	clk[i]='\0';
	return 0;
}  


	
unsigned char get_clock(unsigned char *clk)
{
	int i=0,j=0;
	send_to_gsm_string("AT+CCLK?\0");
	recv_response(WAIT_GSM_RESPONSE);
	while((response[i]!=',') && (response[i]!='\0'))
	{
		if(i >= MAX_RESP_LEN)
		{
			return 1;
		}
		i++;
	}
	if(response[i] == '\0'){
		return 1;
	}
	i++;
	clk[j++]=response[i++];
	clk[j++]=response[i++];
	i++;
	clk[j++]=response[i++];
	clk[j++]=response[i++];
	clk[j]='\0';
	return 0;
}

#if 0
	void 
set_clock(char *buff)
{
	unsigned char i = 0;
	char date[8]={0};

	get_date((unsigned char*)date);
	Delay_sec(1);

	send_serial_string("AT+CCLK=\"");

	while(i < 6)
	{
		if((i==2) || (i==4))
			send_serial('/');

		send_serial(date[i]);
		i++;
	}

	send_serial(',');
	i=0;
	while(i < 4)
	{
		if (i == 2)
			send_serial(':');

		send_serial(buff[i]);
		i++;
	}
	send_to_gsm_string(CLK_END);
	recv_response(100);
}
#endif

void set_clock(char *buff)
{
	unsigned char i = 0;
	send_serial_string(SET_CLK);
	while(i < 4)
	{
		if (i == 2)
			send_serial(':');
		send_serial(buff[i]);
		i++;
	}
	send_to_gsm_string(CLK_END);
	recv_response(100);
}


	 
void send_sms(unsigned char *num, unsigned char *msg)
{
	wdt_reset();
	send_serial_string("at+cmgs=\0");
	send_serial_string("\"+91");
	send_serial_string((char *)num);
	send_serial('\"');
	send_serial('\r');
	Delay_sec(1);
	send_serial_string((char *)msg);
	send_serial(26);
	send_serial('\r');
	send_serial('\n');
	recv_response(50);
}

//num should include semicolon (;) at the end
	 
void make_call(char num[])
{
	/* This function really makes the call by sending the AT command "ATD" */
	char command[20] = "ATD";

	call_status = 1;            /*This variable indicates that the call is made by the KISANRAJA*/

	strcat(command, (char*)num);
	Delay_sec(GSM_DELAY);
	send_to_gsm_string(command);
	print_ssd_delay("CALL",0,500);
}


